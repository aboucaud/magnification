from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
import numpy

ext_modules = [Extension('magnification.Cspectrum',
                         sources=['magnification/Cspectrum.pyx'],
                         include_dirs=['./nicaea/2.3/Demo',
                                       '/usr/local/include',
                                       numpy.get_include()],
                         library_dirs=['./nicaea/2.3/Demo'],
                         libraries=['nicaea'])]

setup(
  name='cosmo wrapper',
  cmdclass={'build_ext': build_ext},
  ext_modules=ext_modules
)
