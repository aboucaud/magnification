.PHONY: clean purge

CC = gcc

CUR_DIR = ${PWD}
NICAEA_DIR = ${CUR_DIR}/nicaea/2.3/Demo

cython: libnicaea.so
	CC=${CC} python setup.py build_ext -i

libnicaea.so:
	cd ${NICAEA_DIR} ; make libnicaea.so ; cd ${CUR_DIR}
	cp ${NICAEA_DIR}/libnicaea.so .

test: cython
	python magnification/matter.py

clean:
	rm -rf magnification/Cspectrum.so
	rm -rf magnification/Cspectrum.c
	find . -type f -name "*.pyc" -exec rm {} +

purge:
	make clean
	rm -rf libnicaea.so
	cd ${NICAEA_DIR} ; make clean ; cd ${CUR_DIR}

