#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
NAME
  Survey
DESCRIPTION
  Set of classes for defining LSST redshift distributions inspired
  from a code written by Ryan Scranton and Chris Morrisson.
CLASSES
  Base class
    dNdz
  Sub classes
    dNdzLSST - LSST redshift distibution
    dNdzGaussianLSST - Gaussian distribution inside the LSST one
CALLING METHOD
  main = dNdzLSST(z_min, z_max, mag_lim)
  bin = dNdzGaussianLSST(z_min, z_max, mag_lim, z0, sigma_z [,lsst_dndz])
OUPUT METHODS
  bin.dndz(z)
  bin.n_obj(obj_density) - number of objects in the distribution
AUTHOR
  Alexandre Boucaud
"""
from __future__ import print_function, division, absolute_import

import numpy as np
import scipy.integrate as Intg
import scipy.interpolate as Intp

import magnification.windowfunction as WF

RAD2DEG = 180 / np.pi
RAD2ARCMIN = 60 * RAD2DEG


class RedshiftBins(object):
    """Base class for creating and propagating in the code the chosen
    tomographic redshift distribution. The class included the
    redshift distribution, the overdensity and lensing
    window functions and the shot noise for each bins.

    Parameters
    ----------
    z_min : float, optional
        lower bound of the redshift distribution
    z_max : float, optional
        maximum bound of the redshift distribution
    survey : string
        name of the survey
    dictype : string, optional
        dictionary with cosmological parameters

    """
    def __init__(self, z_min=0.0, z_max=3.0,
                 survey='lsst', dictype='nicaea'):
        self.z_min = z_min
        self.z_max = z_max
        self.dictype = dictype

        if survey in ['lsst', 'Lsst', 'LSST']:
            self.delta_z = 0.025
            self.mag_lim = 27
            self.n_gal = 45.
            self.area = 20000
            self.dndz_survey = dNdzLSST(self.z_min, self.z_max,
                                        self.mag_lim)
        elif survey in ['euclid', 'Euclid', 'EUCLID']:
            self.delta_z = 0.025
            self.mag_lim = 24.5
            self.n_gal = 30.
            self.area = 15000
            self.dndz_survey = dNdzSmail(self.z_min, self.z_max, z_med=0.9)
        else:
            print('Survey not found')
        self.dndz_list = []
        self.delta_wf_list = []
        self.kappa_wf_list = []

        self._shotn = {}

        self._initialized_wf = False
        self._initialized_shotn = False

    def shot_noise(self):
        """Return shot noise dictionary
        The keys are the redshift bin numbers

        """
        if not self._initialized_shotn:
            self._init_shot_noise()

        return self._shotn

    def _init_shot_noise(self):
        """Create shot noise dictionary

        """
        for id_a in xrange(len(self.z_arr)):
            self._shotn[str(id_a)] = {}
            for id_b in xrange(id_a, len(self.z_arr)):
                self._shotn[str(id_a)][str(id_b)] = self._get_shotn(id_a, id_b)

        self._initialized_shotn = True

    def _get_shotn(self, id_a, id_b):
        """Compute the shot noise for a given redshift bin number

        """
        fact = self.dndz_survey.normalize()
        if id_a == id_b:
            density_rad = self.n_gal * RAD2ARCMIN ** 2
            area, _ = Intg.quad(self._embedded_dndz,
                                self.z_min, self.z_max,
                                args=(id_a,), limit=200)
            return 1.0 / (area * fact * density_rad)
        else:
            return 0.0

    def _embedded_dndz(self, redshift, id):
        """Redshift distribution normalized by the PDF of the survey

        """
        return -1.0

    def initialized(self):
        """Return bool for initialization of window functions

        """
        return self._initialized_wf


class RedshiftBinsBasic(RedshiftBins):
    """Sub-class of RedshiftBins producing hand-made
    Gaussian redshift bins, well separated so they do not overlap.

    Parameters
    ----------
    z_arr : array_like
        array of the mean redshift of the bins
    z_min : float, optional
        lower bound of the redshift distribution
    z_max : float, optional
        maximum bound of the redshift distribution
    delta_z : float, optional
        width of the Gaussian bins
    survey : string, optional
        name of the survey
    dictype : string, optional
        dictionary with cosmological parameters

    """
    def __init__(self, z_arr, z_min=0.0, z_max=3.0, delta_z=0.07,
                 survey='lsst', dictype='nicaea'):
        RedshiftBins.__init__(self, z_min, z_max, survey, dictype)
        self.z_arr = z_arr
        self.n_bins = len(z_arr)
        self.delta_z = delta_z

    def make_bins(self):
        self.dndz_list = [
            dNdzGaussian(self.z_min, self.z_max, redshift, self.delta_z)
            for redshift in self.z_arr]

    def init_window(self, cosmo_dict=None):
        """Initialize window functions for all redshift
        in the redshift range z_arr.

        """
        # Compute tomographic bins inside survey distribution
        self.make_bins()
        # Retrieve the average number density per bin to produce the shot noise
        self._init_shot_noise()
        # Initialize the galaxy and convergence window functions
        self.delta_wf_list = [
            WF.WindowFunctionGalaxy(dndz, self.z_min, self.z_max,
                                    cosmo_dict=cosmo_dict,
                                    dictype=self.dictype,
                                    isspline=False)
            for dndz in self.dndz_list]
        self.kappa_wf_list = [
            WF.WindowFunctionConvergence(dndz, self.z_min, self.z_max,
                                    cosmo_dict=cosmo_dict,
                                    dictype=self.dictype,
                                    isspline=False)
            for dndz in self.dndz_list]
        # for dndz in self.dndz_list:
        #     dndz.normalize()
        #     wfg = WF.WindowFunctionGalaxy(dndz, self.z_min, self.z_max,
        #                             cosmo_dict=cosmo_dict,
        #                             dictype=self.dictype,
        #                             isspline=False)
        #     wfc = WF.WindowFunctionConvergence(dndz, self.z_min, self.z_max,
        #                             cosmo_dict=cosmo_dict,
        #                             dictype=self.dictype,
        #                             isspline=False)
        #     self.delta_wf_list.append(wfg)
        #     self.kappa_wf_list.append(wfc)
        self._initialized = True
        # print('Window function initialized')

    def _embedded_dndz(self, redshift, id):
        """Redshift distribution normalized by the PDF of the survey"""
        return (self.dndz_survey.dndz(redshift) *
                self.dndz_list[id].dndz(redshift))


class RedshiftBinsSpectro(RedshiftBins):
    """Class for spectroscopic redshift distribution

    Sub-class of RedshiftBins producing tomographic redshift bins
    with equal number of galaxies in it. This assume a perfect redshift
    knowledge.

    Parameters
    ----------
    n_bins : integer
        the number of tomographic redshift bins to divide the survey
    z_min : float, optional
        lower bound of the redshift distribution
    z_max : float, optional
        maximum bound of the redshift distribution
    survey : string, optional
        name of the survey
    dictype : string, optional
        dictionary with cosmological parameters

    """
    def __init__(self, n_bins, z_min=0.0, z_max=5.0,
                 survey='lsst', dictype='ivan'):
        RedshiftBins.__init__(self, z_min, z_max, survey, dictype)
        self.n_bins = n_bins

    def make_bins(self):
        # Normalize main distribution
        # self.dndz_survey.normalize()
        # Create a dummy redshift table for normalized distribution
        _dz = 0.005
        _zr = np.arange(self.z_min, self.z_max + _dz, _dz)
        _pz = self.dndz_survey.dndz(_zr) * self.dndz_survey.normalize()
        # Divise distribution into equal number bins and get their redshift boundaries
        interval = (np.arange(self.n_bins - 1) + 1.) / self.n_bins
        z_bounds = np.interp(interval, _pz.cumsum() / _pz.sum(), _zr)
        zbin_min = [self.z_min] + [z for z in z_bounds]
        zbin_max = [z for z in z_bounds] + [self.z_max]
        # Compute bin distributions
        self.dndz_list = [
            dNdzFlat(self.z_min, self.z_max, self.dndz_survey, zbmin, zbmax)
            for zbmin, zbmax in zip(zbin_min, zbin_max)]
        # Get their median redshift
        self.z_arr = [self.get_med_redshift(dndz) for dndz in self.dndz_list]

    def get_med_redshift(self, dndzbin):
        """Compute and return median redshift of given bin

        Current method: get the first value after crossing the half the
                        bin population
        """
        _dz = 0.005
        _z = np.arange(self.z_min, self.z_max + _dz, _dz)
        pzbin = dndzbin.dndz(_z)
        frac = pzbin.cumsum() / pzbin.sum()

        return _z[np.where(frac >= 0.5)][0]

    def init_window(self, cosmo_dict=None):
        """Initialize window functions for all redshift
        in the redshift range z_arr.

        """
        # Compute tomographic bins inside survey distribution
        self.make_bins()
        # Retrieve the average number density per bin to produce the shot noise
        self._init_shot_noise()
        # Initialize the galaxy and convergence window functions
        self.delta_wf_list = [
            WF.WindowFunctionGalaxy(dndz, self.z_min, self.z_max,
                                    cosmo_dict=cosmo_dict,
                                    dictype=self.dictype,
                                    isspline=False)
            for dndz in self.dndz_list]
        self.kappa_wf_list = [
            WF.WindowFunctionConvergence(dndz, self.z_min, self.z_max,
                                    cosmo_dict=cosmo_dict,
                                    dictype=self.dictype,
                                    isspline=False)
            for dndz in self.dndz_list]

        self._initialized = True
        print('Window function initialized')

    def _get_shotn(self, id_a, id_b):
        """Compute the shot noise for a given redshift bin number

        """
        self.dndz_survey.normalize()
        density_rad = self.n_gal * RAD2ARCMIN ** 2
        area = 1.0 / self.n_bins
        if id_a == id_b:
            return 1.0 / (area * density_rad)
        else:
            return 0.0


class RedshiftBinsPhoto(RedshiftBins):
    """Class for photometric redshift distribution

    Sub-class of RedshiftBins producing tomographic redshift bins
    with equal number of galaxies in it. The error of the photometric
    redshifts is taken into account in the shape of the bins, creating
    an overlap.

    Parameters
    ----------
    n_bins : integer
        the number of tomographic redshift bins to divide the survey
    z_min : float, optional
        lower bound of the redshift distribution
    z_max : float, optional
        maximum bound of the redshift distribution
    delta_z : float, optional
        accuracy on the photometric redshift error
    survey : string, optional
        name of the survey
    dictype : string, optional
        dictionary with cosmological parameters
    nooverlap : boolean, optional
        decide whether the tomographic bins should overlap or not
        NOT VERY ACCURATE

    """
    def __init__(self, n_bins, z_min=0.0, z_max=5.0, delta_z=0.02,
                 survey='lsst', dictype='ivan', nooverlap=False):
        RedshiftBins.__init__(self, z_min, z_max, survey, dictype)
        self.n_bins = n_bins
        self.delta_z = delta_z
        self.nooverlap = nooverlap

    def make_bins(self):
        """Create bins with equal galaxy number in the survey
        distribution function

        """
        if self.nooverlap:
            n_bins = int(2 * self.n_bins - 1)
            idx = np.arange(n_bins)[::2]
        else:
            n_bins = self.n_bins
            idx = np.arange(n_bins)
        # Normalize main distribution
        self.dndz_survey.normalize()
        # Create a dummy redshift table for normalized distribution
        _dz = 0.005
        _zr = np.arange(self.z_min, self.z_max + _dz, _dz)
        _pz = self.dndz_survey.dndz(_zr)
        # Divise distribution into equal bins and get their redshift boundaries
        interval = (np.arange(self.n_bins - 1) + 1.) / self.n_bins
        z_bounds = np.interp(interval, _pz.cumsum() / _pz.sum(), _zr)
        zbin_min = [self.z_min] + [z for z in z_bounds]
        zbin_max = [z for z in z_bounds] + [self.z_max]
        # Compute bin distributions depending on the photo-z error sigma_z
        pzbins = [self.pzcut(_pz, _zr, self.delta_z, zbin_min[i], zbin_max[i])
                  for i in idx]
        self.dndz_list = [Intp.InterpolatedUnivariateSpline(_zr, pzb)
                          for pzb in pzbins]
        # Normalize the distributions
        pzbins_norm = [pzb / (pzb.sum() * _dz) for pzb in pzbins]
        # Create splines for the distributions
        self.dndz_norm_list = [Intp.InterpolatedUnivariateSpline(_zr, pzb)
                               for pzb in pzbins_norm]
        # Get their median redshift
        self.z_arr = [self.get_med_redshift(_zr, pzb) for pzb in pzbins]

    def pzcut(self, pz, z, zerr, zbin_min, zbin_max, size=10):
        """Convolution of the survey distribution function with a
        photometric redshift bin delimited by zbin_min and zbin_max

        """
        dz = z[1] - z[0]
        pzout = np.zeros_like(pz)
        jmin = int((zbin_min - (zerr*(1.+zbin_min)*size))/dz)
        if jmin < 0:
            jmin = 0
        jmax = int((zbin_max + (zerr*(1.+zbin_max)*size))/dz)
        if jmax > len(z):
            jmax = len(z)
        ztemp = np.linspace(zbin_min, zbin_max, 200)
        for j in range(jmin, jmax):
            sigz = zerr * (1 + z[j])
            pzerr = self.pz_err(ztemp, z[j], sigz)
            pzout[j] = pz[j] * pzerr.sum() * (ztemp[1] - ztemp[0])

        return pzout

    def pz_err(self, z, zg, sigz):
        """Probability distribution function taking photometric redshift
        errors into account

        """
        return (np.exp(-0.5 * np.power(z - zg, 2) / sigz**2) /
                (2.*np.pi)**0.5 / sigz)

    def get_med_redshift(self, z, pzbin):
        """Compute and return median redshift of given bin

        """
        frac = pzbin.cumsum() / pzbin.sum()
        zmed = z[np.where(frac >= 0.5)][0]

        return zmed

    def init_window(self, cosmo_dict=None):
        """Initialize window functions for all redshift
        in the redshift range z_arr.

        """
        self.make_bins()
        self.delta_wf_list = [
            WF.WindowFunctionGalaxy(dndz, self.z_min, self.z_max,
                                    cosmo_dict=cosmo_dict,
                                    dictype=self.dictype,
                                    isspline=True)
            for dndz in self.dndz_norm_list]
        self.kappa_wf_list = [
            WF.WindowFunctionConvergence(dndz, self.z_min, self.z_max,
                                         cosmo_dict=cosmo_dict,
                                         dictype=self.dictype,
                                         isspline=True)
            for dndz in self.dndz_norm_list]

        self._initialized = True
        print('Window function initialized')

    def _embedded_dndz(self, redshift, id):
        """Redshift distribution normalized by the PDF of the survey"""
        return self.dndz_list[id](redshift)


#------------------------------------------------------------------------------


class dNdz(object):
    """Base class for a simple redshift distribution.
    This class handles all of the details of normalization and
    interpolation. Derived classes should be used for specific redshift
    distributions.

    Parameters
    ----------
    z_min: float
        minimum redshift
    z_max: float
        maximum redshift

    """
    def __init__(self, z_min, z_max):
        self.z_min = z_min
        self.z_max = z_max
        self.norm = 1.0

    def normalize(self):
        """Compute the normalized PDF for the redshift distribution
        for the range z_min - z_max"""
        norm, _ = Intg.quad(self.dndz, self.z_min, self.z_max)
        # self.norm = 1.0 / norm
        return 1.0 / norm

    def raw_dndz(self, redshift):
        """Raw definition of the redshift distribution"""
        return 1.0

    def dndz(self, redshift):
        """Normalized dn/dz PDF"""
        return np.where((redshift <= self.z_max) & (redshift >= self.z_min),
                        self.norm * self.raw_dndz(redshift), 0.0)


class dNdzLSST(dNdz):
    """Class for the LSST redshift distribution

    Parameters
    ----------
    z_min: float
        minimum redshift
    z_max: float
        maximum redshift
    mag_lim, float
        limited magnitude in the i-band

    """
    def __init__(self, z_min, z_max, mag_lim):
        dNdz.__init__(self, z_min, z_max)
        self.z_min = z_min
        self.z_max = z_max
        self.mag_lim = mag_lim
        # Fitting function for z0 (extracted from LSSTScienceBook)
        self.z0 = 0.0417 * mag_lim - 0.744

    def raw_dndz(self, z):
        """LSST distribution

        p(z) = (z/z0)^2 * exp(-z/z0) / (2z0)

        """
        return (np.power(z / self.z0, 2.) *
                np.exp(-1.0 * z / self.z0) / (2 * self.z0))


class dNdzEuclid(dNdz):
    """Class for the Euclid redshift distribution

    DEPRECATED
    """
    def __init__(self, z_min, z_max):
        dNdz.__init__(self, z_min, z_max)
        self.z_min = z_min
        self.z_max = z_max

        #Extracted from redbook
        z_median = 0.9
        self.z0 = z_median / 1.412

    def raw_dndz(self, redshift):
        return 3. * (
            np.power(redshift / self.z0, 2) *
            np.exp(-1.0 * np.power(redshift / self.z0, 1.5)) /
            (2 * self.z0))


class dNdzSmail(dNdz):
    """Class for the Euclid redshift distribution

    Parameters
    ----------
    z_min: float
        minimum redshift
    z_max: float
        maximum redshift
    z_med: float, optional
        median redshift of the distribution

    """
    def __init__(self, z_min, z_max, z_med=0.9):
        dNdz.__init__(self, z_min, z_max)
        self.z_min = z_min
        self.z_max = z_max
        # z_med default value is for Euclid (extracted from redbook)
        self.z0 = z_med / 1.412

    def raw_dndz(self, redshift):
        return redshift**2 * np.exp(-1.0 * np.power(redshift / self.z0, 1.5))


class dNdzGaussian(dNdz):
    """Class for gaussian redshift distributions within the LSST
    galaxy redshift distribution"""
    def __init__(self, z_min, z_max, z_mean, sigma_z):
        dNdz.__init__(self, z_min, z_max)
        self.z_mean = z_mean
        self.sigma_z = sigma_z

    def raw_dndz(self, redshift):
        """Return a Gaussian distribution"""
        return np.exp(-0.5 * np.power(
            (redshift - self.z_mean) / self.sigma_z, 2))


class dNdzFlat(dNdz):
    """Class for flat redshift distributions within the LSST
    galaxy redshift distribution"""
    def __init__(self, z_min, z_max, dndz_survey, zbin_min, zbin_max):
        dNdz.__init__(self, z_min, z_max)
        self.survey = dndz_survey
        self.zbin_min = zbin_min
        self.zbin_max = zbin_max

    def raw_dndz(self, redshift):
        """Return LSST normalized distribution for a given bin"""
        return np.where((redshift >= self.zbin_min) &
                        (redshift <= self.zbin_max),
                        self.survey.dndz(redshift), 0.0)


if __name__ == '__main__':
    pass
