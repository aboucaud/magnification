from libc.stdio cimport FILE, stdout, stderr, fopen, fclose, fflush

cimport numpy as np
import numpy as np

import warnings

class CosmoWarning(Warning):
    pass

warnings.filterwarnings("always", category = CosmoWarning, module = __name__)

cdef extern from *:
    ctypedef char* const_char_ptr "const char*"
cdef extern from "errorlist.h":
    ctypedef struct error:
        pass

    int _isError(error *err)
    void printError(FILE* flog,error* err)
    void purgeError(error **err)


cdef extern from "cosmo.h":
    ctypedef enum nonlinear_t:
        linear
        pd96
        smith03
        smith03_de
        halodm
    ctypedef enum transfer_t:
        bbks
        eisenhu
        eisenhu_osc
        camb_vinschter
        camb
        be84
    ctypedef enum growth_t:
        heath
        growth_de
        camb_vinschter_gr
    ctypedef enum de_param_t:
        jassal
        linder
        earlyDE
    ctypedef enum norm_t:
        norm_s8
        norm_as
    ctypedef struct cosmo:
        pass

    cosmo* init_parameters(double OMEGAM, double OMEGAV, double W0_DE, double W1_DE, 
                           double H100, double OMEGAB, double OMEGANUMASS,
                           double NEFFNUMASS, double NORM, double NSPEC,
                           nonlinear_t NONLINEAR, transfer_t TRANSFER, growth_t GROWTH,
                           de_param_t DEPARAM, norm_t normmode, double AMIN, error **err)
    void free_parameters(cosmo** self)

    void updateParameters(cosmo* model, double OMEGAM, double OMEGAV, double W0_DE, double W1_DE, 
                          double H100, double OMEGAB, double OMEGANUMASS, double NEFFNUMASS,
                          double NORM, double NSPEC, nonlinear_t NONLINEAR, transfer_t TRANSFER,
                          growth_t GROWTH, de_param_t DEPARAM, norm_t normmode, double AMIN,
                          error **err)

    double P_L(cosmo* ,double a, double k, error **err)
    double P_NL(cosmo*, double a, double k, error **err)

cdef class Cosmo(object):

    cdef error* _error
    cdef cosmo* _cosmo
    cdef public double Omega_m, Omega_de, w0_de, w1_de, h_100, Omega_b, Omega_nu_mass
    cdef public double Neff_nu_mass, normalization, n_spec
    cdef public nonlinear_t nonlinear
    cdef public transfer_t transfer
    cdef public growth_t growth
    cdef public de_param_t de_params
    cdef public norm_t normmode
    cdef public double a_min

    def __cinit__(self, cosmoDict, a_min = 0.1, Omega_nu_mass = 0.0, Neff_nu_mass = 0.0):
        self.Omega_m = cosmoDict.omega_m
        self.Omega_de = cosmoDict.omega_l
        self.w0_de = cosmoDict.w0
        self.w1_de = cosmoDict.wa
        self.h_100 = cosmoDict.h
        self.Omega_b = cosmoDict.omega_b
        self.Omega_nu_mass = Omega_nu_mass
        self.Neff_nu_mass = Neff_nu_mass
        self.normalization = cosmoDict.norm
        self.n_spec = cosmoDict.n_s
        self.nonlinear = smith03_de   #smith03_de
        self.transfer = eisenhu_osc    #eisenhu_osc
        self.growth = growth_de      #growth_de
        self.de_params = linder   #linder
        self.normmode = cosmoDict.normmode
        self.a_min = a_min

        self._error = NULL
        # Creating cosmo lib in C
        self._cosmo = init_parameters(self.Omega_m,
                                      self.Omega_de,
                                      self.w0_de,
                                      self.w1_de,
                                      self.h_100,
                                      self.Omega_b,
                                      self.Omega_nu_mass,
                                      self.Neff_nu_mass,
                                      self.normalization,
                                      self.n_spec,
                                      self.nonlinear,
                                      self.transfer,
                                      self.growth,
                                      self.de_params,
                                      self.normmode,
                                      self.a_min,
                                      &self._error)

    def updateParameters(self):
        updateParameters(self._cosmo, self.Omega_m, self.Omega_de,
                         self.w0_de, self.w1_de, self.h_100, self.Omega_b,
                         self.Omega_nu_mass, self.Neff_nu_mass,
                         self.normalization, self.n_spec, self.nonlinear,
                         self.transfer, self.growth, self.de_params,
                         self.normmode, self.a_min, &self._error)        
    
    def printError(self):
        printError(stderr, self._error)
        fflush(stdout)
    
    def purgeError(self):
        purgeError(&self._error)

    def __dealloc__(self):
        free_parameters(&self._cosmo)

    def P_L(self, av, kv):
        b = np.broadcast(av, kv)
        res = np.empty(b.shape, np.float64)
        for i, (a, k) in enumerate(b):
            res.flat[i] = P_L(self._cosmo, a, k, &self._error)
        if _isError(self._error):
            warnings.warn("An error occured, see printError.", category = CosmoWarning)
        return res

    def P_NL(self, av, kv):
        b = np.broadcast(av, kv)
        res = np.empty(b.shape, np.float64)
        for i, (a, k) in enumerate(b):
            res.flat[i] = P_NL(self._cosmo, a, k, &self._error)
        if _isError(self._error):
            warnings.warn("An error occured, see printError.", category = CosmoWarning)
        return res

