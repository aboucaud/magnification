"""
Dictionaries
"""
from __future__ import print_function, division, absolute_import


class Cosmodict(dict):
    #WMAP7
    _defaults = {'h': 0.714,
                 'omc': 0.1107,
                 'omb': 0.0227,
                 'Omk': 0.0,
                 'w0': -1.,
                 'wa': 0.,
                 'A_s': 2.38e-9,
                 'n_s': 0.969
                 }

    _authorized = frozenset(_defaults.keys())

    def __init__(self, *args, **kwds):
        # standard dict initialisation
        super(Cosmodict, self).__init__(*args, **kwds)

        # set default values
        for key, value in self._defaults.items():
            self.setdefault(key, value)

        # keep only authorized keys
        unauthorized = set(self.keys()) - self._authorized
        for k in unauthorized:
            del self[k]

    # get the value with self.name instead of self[name]
    def __getattr__(self, name):
        if name in self:
            return self[name]
        else:
            raise AttributeError('unknown attribute')

    # set the new value when calling wih name = newvalue
    def __setattr__(self, name, value):
        if name in self._authorized:
            self[name] = value
        else:
            raise AttributeError('unauthorized attribute')

        #if name == (omega_m or omega_l):
       #    print('You should verify if omega_m + omega_l = 1 !')

    def __dir__(self):
        return sorted(self.__dict__.keys() + self.keys())


class Localdict(dict):

    _defaults = {'h': 0.714,
                 'n_s': 0.969,
                 'omega_m': 0.262,
                 'omega_b': 0.045,
                 'omega_l0': 0.738,
                 'omega_k': 0.0,
                 'w0': -1.,
                 'wa': 0.}

    _authorized = frozenset(_defaults.keys())

    def __init__(self, *args, **kwds):
        # standard dict initialisation
        super(Localdict, self).__init__(*args, **kwds)

        # set default values
        for key, value in self._defaults.items():
            self.setdefault(key, value)

        # keep only authorized keys
        unauthorized = set(self.keys()) - self._authorized
        for k in unauthorized:
            del self[k]

    # get the value with self.name instead of self[name]
    def __getattr__(self, name):
        if name in self:
            return self[name]
        else:
            raise AttributeError('unknown attribute')

    # set the new value when calling wih name = newvalue
    def __setattr__(self, name, value):
        if name in self._authorized:
            self[name] = value
        else:
            raise AttributeError('unauthorized attribute')

        #if name == (omega_m or omega_l):
       #    print('You should verify if omega_m + omega_l = 1 !')

    def __dir__(self):
        return sorted(self.__dict__.keys() + self.keys())


class NicaeaCosmodict(dict):
    _defaults = {'h': 0.714,
                 'norm': 0.803,
                 'n_s': 0.969,
                 'omega_m': 0.262,
                 'omega_b': 0.045,
                 'omega_l': 0.738,
                 'omega_k': 0.0,
                 'w0': -1.,
                 'wa': 0.,
                 'normmode': 0}
                 #0 for sigma8 and 1 for As
    _authorized = frozenset(_defaults.keys())

    def __init__(self, *args, **kwds):
        # standard dict initialisation
        super(NicaeaCosmodict, self).__init__(*args, **kwds)

        # set default values
        for key, value in self._defaults.items():
            self.setdefault(key, value)

        # keep only authorized keys
        unauthorized = set(self.keys()) - self._authorized
        for k in unauthorized:
            del self[k]

    # get the value with self.name instead of self[name]
    def __getattr__(self, name):
        if name in self:
            return self[name]
        else:
            raise AttributeError('unknown attribute')

    # set the new value when calling wih name = newvalue
    def __setattr__(self, name, value):
        if name in self._authorized:
            self[name] = value
        else:
            raise AttributeError('unauthorized attribute')

        #if name == (omega_m or omega_l):
       #    print('You should verify if omega_m + omega_l = 1 !')

    def __dir__(self):
        return sorted(self.__dict__.keys() + self.keys())


class IvanCosmodict(dict):
    _defaults = {'h': 0.67,
                 'norm': 0.82,
                 'n_s': 1.,
                 'omega_m': 0.31,
                 'omega_b': 0.048,
                 'omega_l': 0.69,
                 'omega_k': 0.0,
                 'w0': -1.0,
                 'wa': 0.,
                 'normmode': 0}
    _authorized = frozenset(_defaults.keys())

    def __init__(self, *args, **kwds):
        super(IvanCosmodict, self).__init__(*args, **kwds)
        for key, value in self._defaults.items():
            self.setdefault(key, value)
        unauthorized = set(self.keys()) - self._authorized
        for k in unauthorized:
            del self[k]

    def __getattr__(self, name):
        if name in self:
            return self[name]
        else:
            raise AttributeError('unknown attribute')

    def __setattr__(self, name, value):
        if name in self._authorized:
            self[name] = value
        else:
            raise AttributeError('unauthorized attribute')

    def __dir__(self):
        return sorted(self.__dict__.keys() + self.keys())
