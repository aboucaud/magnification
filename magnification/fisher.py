#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
NAME
    fisher
DESCRIPTION
    module containing the class MagnificationFisher

Created on Thu Jan 24 23:58:20 2013

@author: alexandreboucaud
"""
from __future__ import print_function, division, absolute_import

import numpy as np
import os
import pickle
import time as Time
import scipy.interpolate as Intp
from scipy.stats import linregress
import numpy.linalg.linalg as Alg

from itertools import combinations

import magnification.dictionaries as Dict
import magnification.covariance as Cov
import magnification.correlation as Corr

# macalex
WORKDIR = '/Users/alexandreboucaud/work/svn/lsst/'
PROJECTDIR = os.path.join(WORKDIR, 'Amplification/trunk/magnification')
DATADIR = '/Users/alexandreboucaud/work/LSST/magnif'
# MPATH = os.path.join(DATADIR, 'covariance_matrices')
MPATH = os.path.join(PROJECTDIR, 'matrix2')
MATRIXFILENAME = 'matrix_{}.txt'
OUTPUTPATH = os.path.join(DATADIR, 'outputs')
# macboss
# WORKDIR = '/Users/boucaud/gitrepo'
# PROJECTDIR = os.path.join(WORKDIR, 'magnification')
# MPATH = os.path.join(WORKDIR, 'cmatrix')
# OUTPUTPATH = os.path.join(WORKDIR, 'outputs')


class MagnificationFisher(object):
    """Class computing the Fisher matrix of cosmological parameters
    for the magnification cross-power spectrum, including tomography

    Calling sequence:
        fisher = MagnificationFisher(redshift_bins, **kwargs)
        fisher.fisher()

    """
    def __init__(self, redshift_bins, covariance='code', nval=10, linear=False):
        """Constructor for the Fisher class.

        param | redshift_bins : object containing the redshift distribution
                                and delta/kappa window functions associated
        param | covariance : switch between 'file' to read from file
                                            'code' to compute from scratch
        param | dictype : choose between wmap / camb and nicaea

        """
        # local date and time
        self.datetime = Time.strftime('%Y-%m-%d_%H%M%S')
        # Window functions
        self.redshift_bins = redshift_bins
        # Redshift range
        self.redshifts = redshift_bins.z_arr
        # Area in deg^2
        self.area = redshift_bins.area
        # Dictionary type: wmap or nicaea
        self.dictype = redshift_bins.dictype
        print('Cosmological parameters dictionary type: %s' % self.dictype)
        # Covariance matrix of error:
        #   'file' to read from file
        #   'code' to compute the matrix
        self.covtype = covariance
        # log array of 2D modes
        # self.ln_ell_arr = np.log(np.logspace(1, 4, 30))
        self.ln_ell_arr = np.log(np.logspace(0.7, 3.7, 200))
        #
        self.nval = nval
        self.linear = linear
        ###
        # Initialize other attributes
        ###
        # Fiducial cosmology
        self.cosmofid = {}
        # Sorted cosmological parameters
        self.sorted_parms = []
        # 2D-modes
        self.ell = np.array([])
        # Derivatives
        self.derivatives = {}
        # Covariance Matrix
        self.covariance = np.array([])
        # Fisher matrix at a given ell (2D mode)
        self.fisher_ell = np.array([])
        # Fisher Matrix
        self.fisher_matrix = np.array([])
        ###
        # Private variables
        ###
        self._initialized_parameters = False
        if self.covtype in ['file', 'File', 'FILE']:
            self._initialized_covariance = True
        else:
            self._initialized_covariance = False
        self._initialized_derivatives = False

    def _init_parameters(self):
        """Initialize cosmological parameters for the Fisher matrix.

            - a sorted parameter list
            - a fiducial cosmology dictionary

        """
        if self.dictype in ['nicaea', 'NICAEA', 'Nicaea']:
            self.cosmofid = Dict.NicaeaCosmodict()
        elif self.dictype in ['ivan', 'Ivan', 'IVAN']:
            self.cosmofid = Dict.IvanCosmodict()
        else:
            self.cosmofid = Dict.Cosmodict()

        self.sorted_parms = ['w0', 'wa', 'norm', 'omega_m',
                             'omega_l', 'omega_b', 'h', 'n_s']

        self.biaslist = ['b0', 'b1', 'b2', 'b3', 'b4', 'b5', 'b6', 'b7', 'b8', 'b9']
        if self.biaslist:
            self.sorted_parms += self.biaslist[:max(1, len(self.redshifts)-1)]

        self._initialized_parameters = True
        print("Parameters initialized")

    def get_dev(self):
        """Set up the steps length for the parameter ranges.

        Empirical value that corresponds to the minimum step
        for the numerical derivation to converge.

        """
        dev = {
            'w0': 1.e-2,
            'wa': 5.e-2,
            'norm': 1.e-2,
            'omega_b': 2.e-4,
            'omega_m': 2.e-3,
            'omega_l': 5.e-3,
            'h': 5.e-3,
            'n_s': 1.e-2
            }

        return dev

    def get_covariance_errors(self):
        """Compute the error covariance matrix of cosmic magnification."""
        self.covariance = Cov.MagnificationCovariance(self.redshift_bins)

        self._initialized_covariance = True
        print("Covariance matrix initialized")

    def _init_derivatives(self):
        """Store the derivatives of the cross power spectrum in a dictionary."""
        for param in self.sorted_parms:
            if param in self.biaslist:
                self.derivatives[param] = self.get_derivative_bias(param)
            else:
                self.derivatives[param] = self.get_derivatives(param)

        pfilename = os.path.join(OUTPUTPATH, '{}_derivatives_file.pck'.format(self.datetime))
        with open(pfilename, 'w') as pfile:
            pickle.dump(self.derivatives, pfile)
        print('Temporary copy saved in %s' % (pfilename))

        self._initialized_derivatives = True

    def get_derivative_bias(self, param):
        """Compute the derivative of cross power spectrum with respect
        to the bias

        """
        idpar = self.biaslist.index(param)
        derivative = {}
        for id_a in xrange(len(self.redshifts)):
            derivative[str(id_a)] = {}
            for id_b in xrange(id_a, len(self.redshifts)):
                status = 'Param %s\tID A = %d\tID B = %d..'
                if (id_a != idpar):
                    # derivatives_ell = np.zeros(len(self.ln_ell_arr))
                    logderivatives_ell = np.zeros(len(self.ln_ell_arr))
                else:
                    print(status % (param, id_a, id_b))
                    pgg = self.get_spectrum(id_a, id_b, self.cosmofid,
                                            corrtype='auto')
                    pgm = self.get_spectrum(id_a, id_b, self.cosmofid,
                                            corrtype='cross')
                    pmg = self.get_spectrum(id_a, id_b, self.cosmofid,
                                            corrtype='swap')
                    # derivatives_ell = (2 * pgg(self.ln_ell_arr) +
                    logderivatives_ell = np.log(2 * pgg(self.ln_ell_arr) +
                                       pmg(self.ln_ell_arr) +
                                       pgm(self.ln_ell_arr))
                # derivatives_spl = Intp.InterpolatedUnivariateSpline(
                    # self.ln_ell_arr, derivatives_ell)
                logderivatives_spl = Intp.InterpolatedUnivariateSpline(
                    self.ln_ell_arr, logderivatives_ell)
                # derivative[str(id_a)][str(id_b)] = derivatives_spl
                derivative[str(id_a)][str(id_b)] = logderivatives_spl
        done = '%s derivative splines  have been edited\n'
        print(done % (param))

        return derivative

    def get_derivatives(self, param):
        """Compute the derivatives of the cross power spectrum."""
        if not self._initialized_parameters:
            self._init_parameters()
        vrange, cosmo_list = self.get_range(param)
        derivatives = {}
        debug = {}
        for id_a in xrange(len(self.redshifts)):
            derivatives[str(id_a)] = {}
            debug[str(id_a)] = {}
            for id_b in xrange(id_a, len(self.redshifts)):
                status = 'Param %s\tID A = %d\tID B = %d'
                print(status % (param, id_a, id_b))
                if id_b < id_a:
                    # derivatives_ell = np.zeros(len(self.ln_ell_arr))
                    logderivatives_ell = np.zeros(len(self.ln_ell_arr))
                else:
                    debug[str(id_a)][str(id_b)] = {}
                    spectra_spline_list = [
                        self.get_spectrum(id_a, id_b, cosmoD)
                        for cosmoD in cosmo_list]
                    debug[str(id_a)][str(id_b)]['splist'] = spectra_spline_list
                    # spectra_array = self.make_ell_array(
                    #     spectra_spline_list, len(vrange))
                    logspectra_array = self.make_ell_array(
                        spectra_spline_list, len(vrange))
                    debug[str(id_a)][str(id_b)]['lsparr'] = logspectra_array
                    # derivatives_ell = self.make_derivatives_array(
                    #     spectra_array, vrange, param)
                    logderivatives_ell = self.make_derivatives_array(
                        logspectra_array, vrange, param)
                    debug[str(id_a)][str(id_b)]['lders'] = logderivatives_ell
                    # print(derivatives_ell)
                # derivatives_spl = Intp.InterpolatedUnivariateSpline(
                #     self.ln_ell_arr, derivatives_ell)
                logderivatives_spl = Intp.InterpolatedUnivariateSpline(
                    self.ln_ell_arr, logderivatives_ell)
                # derivatives[str(id_a)][str(id_b)] = derivatives_spl
                derivatives[str(id_a)][str(id_b)] = logderivatives_spl
        done = '%s derivative splines  have been edited\n'
        print(done % (param))
        pfilename = os.path.join(
            OUTPUTPATH, '{}_{}_debugfile.pck'.format(self.datetime, param))
        with open(pfilename, 'w') as pfile:
            pickle.dump(debug, pfile)
        print('Debug file saved in %s' % (pfilename))

        return derivatives

    def get_range(self, param):
        """Create a range of values around the cosmo parameters."""
        dev = self.get_dev()
        x0 = self.cosmofid[param]
        vrange = np.linspace(x0 - dev[param] * (self.nval-1) / 2.,
                             x0 + dev[param] * (self.nval-1) / 2.,
                             self.nval)
        # vrange = np.linspace(
        #     self.cosmofid[param] - dev[param],
        #     self.cosmofid[param] + dev[param],
        #     nval)
        # Dictionary selection
        if self.dictype in ['nicaea', 'NICAEA', 'Nicaea']:
            currentdict = Dict.NicaeaCosmodict
        elif self.dictype in ['ivan', 'IVAN', 'Ivan']:
            currentdict = Dict.IvanCosmodict
        # Cosmological parameter excursions
        # WARNING: doesnt require Om + Ol = 1 here
        if param == 'w0':
            cosmo_list = [
                currentdict(w0=val) for val in vrange]
        elif param == 'wa':
            cosmo_list = [
                currentdict(wa=val) for val in vrange]
        elif param == 'norm':
            cosmo_list = [
                currentdict(norm=val) for val in vrange]
        elif param == 'omega_m':
            cosmo_list = [
                currentdict(omega_m=val) for val in vrange]
        elif param == 'omega_b':
            cosmo_list = [
                currentdict(omega_b=val) for val in vrange]
        elif param == 'omega_l':
            cosmo_list = [
                currentdict(omega_l=val) for val in vrange]
        elif param == 'h':
            cosmo_list = [
                currentdict(h=val) for val in vrange]
        elif param == 'n_s':
            cosmo_list = [
                currentdict(n_s=val) for val in vrange]
        else:
            pass
        print("=> initializing %s derivatives" % (param))

        return (vrange, cosmo_list)

    def get_spectrum(self, id_a, id_b, cosmo_dict, corrtype='all'):
        """Compute the magnification spectra

        """
        spectrum = Corr.CorrelationFull(
            self.redshift_bins, id_a, id_b,
            cosmo_dict=cosmo_dict, dictype=self.dictype,
            corrtype=corrtype, linear=self.linear)

        return spectrum.get_kernel_spline()

    def make_ell_array(self, spectra_list, nval):
        """Compute the spline values of the logarithm of the power spectra
        with respect to ell

        """
        # ps_arr = np.zeros((nval, len(self.ln_ell_arr)))
        ln_ps_arr = np.zeros((nval, len(self.ln_ell_arr)))
        for idx, spectrum in enumerate(spectra_list):
            # ps_arr[idx, :] = spectrum(self.ln_ell_arr)
            ln_ps_arr[idx, :] = np.log(spectrum(self.ln_ell_arr))

        # return ps_arr
        return ln_ps_arr

    def make_derivatives_array(self, spectra_array, vrange, param):
        """Compute the log derivatives around the fiducial values

        """
        derivatives_ell = np.zeros(len(self.ln_ell_arr))
        for i in xrange(len(self.ln_ell_arr)):
            # spl = InterpolatedUnivariateSpline(
            #     vrange, spectra_array[:, i])
            # Return first derivative
            # derivatives_ell[i] = spl.derivatives(self.cosmofid[param])[1]
            lreg = linregress(vrange, spectra_array[:, i])
            derivatives_ell[i] = lreg[0]

        return derivatives_ell

    def get_fisher_ell(self, covmatrix, ell):
        """Compute the Fisher matrix for a given ell value

        """
        fisher_ell = np.zeros((len(self.sorted_parms), len(self.sorted_parms)))
        if len(self.redshifts) == 1 and self.covtype == 'file':
            cov = 1 / float(covmatrix)
        else:
            cov = Alg.inv(covmatrix)
        #print('Covariance matrix : \t', covmatrix, '\n')
        #print('Inverse Covariance matrix : \t', C, '\n')
        for idx, xparm in enumerate(self.sorted_parms):
            for idy, yparm in enumerate(self.sorted_parms):
                if idy < idx:
                    continue
                dpx = self.vectorize_derivatives(xparm, ell)
                dpy = self.vectorize_derivatives(yparm, ell)
                fisher_ell[idx, idy] = multiply_matrix(dpx, dpy, cov)
                if idy != idx:
                    fisher_ell[idy, idx] = fisher_ell[idx, idy]

        return fisher_ell

    def vectorize_derivatives(self, parm, ell):
        """Cast the derivatives in an array

        """
        vector = []
        for id_a in xrange(len(self.redshifts)):
            for id_b in xrange(id_a, len(self.redshifts)):
                pm = self.pmag[str(id_a)][str(id_b)](np.log(ell))
                der = np.exp(
                    self.derivatives[parm][str(id_a)][str(id_b)](np.log(ell)))
                if der == 1:
                    vector.append(0)
                else:
                    if parm in self.biaslist:
                        vector.append(der)
                    else:
                        vector.append(der * pm)

        return np.array(vector)

    def load_derivatives(self, external=''):
        if external:
            pfilename = external
        else:
            pfilename = os.path.join(OUTPUTPATH, '{}_derivatives_file.pck'.format(self.datetime))
        with open(pfilename, 'r') as pfile:
            self.derivatives = pickle.load(pfile)

        self._initialized_derivatives = True

    def keep_cross_terms(self):
        idx_list = [str(idx) for idx in xrange(len(self.redshifts))]
        for par in self.sorted_parms:
            for idx in idx_list:
                self.derivatives[par][idx][idx] = \
                    Intp.InterpolatedUnivariateSpline(self.ln_ell_arr,
                        np.zeros(len(self.ln_ell_arr)))

    def keep_auto_terms(self):
        idx_list = combinations(range(len(self.redshifts)), 2)
        for par in self.sorted_parms:
            for a, b in idx_list:
                self.derivatives[par][str(a)][str(b)] = \
                    Intp.InterpolatedUnivariateSpline(self.ln_ell_arr,
                        np.zeros(len(self.ln_ell_arr)))

    def run_fisher(self, ell_min, ell_max, n_ell, fromfile='', cross_only=None, auto_only=None):
        """Compute the Fisher matrix

        """
        print('Starting fisher computation..')
        # IN CASE OF AN ISSUE DURING THE RUN
        if fromfile:
            self.load_derivatives(fromfile)

        if not self._initialized_parameters:
            self._init_parameters()
        if not self._initialized_covariance:
            self.get_covariance_errors()
        if not self._initialized_derivatives:
            self._init_derivatives()

        if cross_only:
            self.load_derivatives(fromfile)
            self.keep_cross_terms()
            self.ext = '_cross'
        elif auto_only:
            self.load_derivatives(fromfile)
            self.keep_auto_terms()
            self.ext = '_auto'
        else:
            self.ext = '_full'


        big_ell = np.logspace(np.log10(ell_min), np.log10(ell_max), num=n_ell)
        self.ell = 0.5 * (big_ell[1:] + big_ell[:-1])
        print('\nell range\n', self.ell, '\n')
        dell_range = np.diff(big_ell)

        with open('pmag.pck', 'r') as fil:
            self.pmag = pickle.load(fil)

        self.fisher_ell = np.zeros(
            (n_ell - 1, len(self.sorted_parms), len(self.sorted_parms)))
        for i in xrange(n_ell - 1):
            if self.covtype in ['file', 'File', 'FILE']:
                cmatrix = np.loadtxt(
                    os.path.join(MPATH, MATRIXFILENAME.format(i)))
            else:
                cmatrix = self.covariance.get_matrix(
                            self.ell[i], dell_range[i], shotnoiseonly=False)
            self.fisher_ell[i] = self.get_fisher_ell(cmatrix, self.ell[i])

        self.fisher_matrix = self.fisher_ell.sum(axis=0)

    def get_fisher_matrix(self):
        """Return the Fisher matrix

        """
        return self.fisher_matrix

    def get_covariance_matrix(self):
        """Returns the covariance matrix (inverse of Fisher matrix)

        """
        if len(self.sorted_parms) == 1:
            final_cov = 1. / self.fisher_matrix
        else:
            final_cov = Alg.inv(self.fisher_matrix)

        return final_cov

    def write(self, time=None):
        """Write the output into files

        """
        self.write_fisher()
        self.write_covariance()
        filename = '%s.log' % (self.datetime + self.ext)
        logfile = os.path.join(OUTPUTPATH, filename)
        with open(logfile, 'w') as logf:
            header = '# Magnification Fisher matrix log\n#---\n#Name\tValue\n'
            logf.write(header)
            # for parm, val in zip(self.cosmofid.keys(), self.cosmofid.values()):
            deviat = self.get_dev()
            for parm in self.sorted_parms:
                if parm in self.biaslist:
                    line = '%s\t%.3f\n' % (parm, 1)
                else:
                    line = '%s\t%.3f\t%0.1e\n' % (parm, self.cosmofid[parm], deviat[parm])
                logf.write(line)
            logf.write('# Average redshift of bins:\n')
            red = '\t'.join(['%.2f' % z for z in self.redshifts])
            logf.write('%s\n' % red)
            logf.write('# Ell range:\n')
            ellr = '\t'.join(['%.1f' % el for el in self.ell])
            logf.write('%s\n' % ellr)
            if time:
                logf.write('# Computational (real) time in sec / min / hrs\n')
                timlis = [time, time / 24., time / 3600.]
                timstr = ' / '.join('%.2f' % t for t in timlis)
                logf.write('%s\n' % timstr)
        print('\nLog saved in: %s' % (logfile))

    def write_fisher(self):
        """Write the fisher matrix into a file

        """
        fisherm = self.get_fisher_matrix()
        filename = '%s_fmat.txt' % (self.datetime + self.ext)
        outputfile = os.path.join(OUTPUTPATH, filename)
        np.savetxt(outputfile, fisherm)
        print('Fisher matrix saved in: %s' % (outputfile))

    def write_covariance(self):
        """Write the covariance matrix into a file

        """
        covm = self.get_covariance_matrix()
        filename = '%s_cmat.txt' % (self.datetime + self.ext)
        outputfile = os.path.join(OUTPUTPATH, filename)
        np.savetxt(outputfile, covm)
        print('Covariance matrix saved in: %s' % (outputfile))


def inverse_matrix(matrix_list):
    """Return the inverse matrix for all the mode values."""
    return [Alg.inv(M) for M in matrix_list]


def multiply_matrix(vect1, vect2, matrix):
    """Return the scalar product of a matrix and two vectors."""
    return Alg.dot(Alg.dot(Alg.transpose(vect1), matrix), vect2)
