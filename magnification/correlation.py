#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
NAME
    Correlation
DESCRIPTION
    Correlation functions between matter overdensities along the
    line-of-sight
AUTHOR
    Alexandre Boucaud
"""
from __future__ import print_function, division, absolute_import

import numpy as np
import scipy.integrate as Intg
import scipy.interpolate as Intpl

import magnification.cosmology as Cosmo
import magnification.matter as Matter
#import magnification.camb as Camb


class Correlation(object):
    """Class ...
    """
    def __init__(self, window_function_a, window_function_b,
                 ell_min=5, ell_max=5000,
                 cosmo_dict=None, dictype='nicaea'):
        """Initialize Correlation class attributes."""
        self.ln_ell_min = np.log(ell_min)
        self.ln_ell_max = np.log(ell_max)

        self.wf_a = window_function_a
        self.wf_b = window_function_b

        self.dictype = dictype
        # Init cosmological dictionary
        self.cosmo = {}
        # Init redshift boundaries
        self.z_min = 0.0
        self.z_max = 5.0
        # Init integral boundaries
        self.chi_min = 0.0
        self.chi_max = 10000.0

        self.matterspec = None

        if not cosmo_dict:
            import magnification.dictionaries as Dict
            if self.dictype in ['nicaea', 'Nicaea', 'NICAEA']:
                cosmo_dict = Dict.NicaeaCosmodict()
            elif self.dictype in ['Ivan', 'ivan', 'IVAN']:
                cosmo_dict = Dict.IvanCosmodict()
            else:
                cosmo_dict = Dict.Cosmodict()
        self.set_cosmology(cosmo_dict)

        dlnell = (self.ln_ell_max - self.ln_ell_min) / 40
        self._ln_ell_array = np.arange(
            self.ln_ell_min, self.ln_ell_max + dlnell, dlnell)
        self._kernel_array = np.zeros_like(self._ln_ell_array)

        self._kernel_spline = None
        self._initialized_spline = False

    def init_spline(self):
        """Initialize kernel spline."""
        for idx, ln_ell in enumerate(self._ln_ell_array):
            kernel = self.raw_kernel(ln_ell)
            self._kernel_array[idx] = kernel

        self._kernel_spline = Intpl.InterpolatedUnivariateSpline(
            self._ln_ell_array, self._kernel_array)

        self._initialized_spline = True
        #print('==> Correlation spline initialized')

    def get_kernel_spline(self):
        """Return kernel spline."""
        if not self._initialized_spline:
            self._init_spline()

        return self._kernel_spline

    def set_cosmology(self, cosmo_dict):
        """Initialize the cosmology given cosmological parameters
        and calls a power spectrum routine."""
        self.wf_a.set_cosmology(cosmo_dict)
        self.wf_b.set_cosmology(cosmo_dict)

        self.chi_min = self.wf_a.chi_min
        self.z_min = self.wf_a.z_min
        if self.wf_b.chi_min < self.chi_min:
            self.chi_min = self.wf_b.chi_min
            self.z_min = self.wf_b.z_min

        self.chi_max = self.wf_a.chi_max
        self.z_max = self.wf_a.z_max
        if self.wf_b.chi_max > self.chi_max:
            self.chi_max = self.wf_b.chi_max
            self.z_max = self.wf_b.z_max

        #self.cosmo = Cosmology.CosmoStructure(cosmo_dict)
        self.cosmo = Cosmo.CosmoStructure(cosmo_dict, dictype=self.dictype)
        #self.matterspec = Matter.MatterPowerSpectrum(self.cosmo)
#        if self.dictype == 'camb':
#            self.matterspec = Camb.CambWrapper(cosmo_dict=cosmo_dict)
#        else:
        self.matterspec = Matter.MatterPowerSpectrum(
            self.cosmo, dictype=self.dictype)

        self._initialized_spline = False

    def raw_kernel(self, ln_ell):
        """Returns the integral over chi of the right kernel integrand,
        for a given ln_ell."""
        ell = np.exp(ln_ell)
#        if self.dictype == 'camb':
#            kernel, err = Intg.quad(self._kernel_integrand_camb,
#                                    self.chi_min, self.chi_max,
#                args=(ell,), limit=200)
#        else:
        kernel = Intg.quad(
            self._kernel_integrand_nicaea, self.chi_min, self.chi_max,
            args=(ell,), limit=200)[0]
        return kernel

#    def _kernel_integrand_camb(self, chi, ell):
#        """Kernel integrand used with CAMB wrapper
#        """
#        self.matterspec.set_redshift(self.cosmo.redshift(chi))
#        return (self.wf_a.window_function(chi) *
#                self.wf_b.window_function(chi) *
#            self.matterspec.linear_power(ell / chi) /
#            self.cosmo.cosmopar.h**3 / chi / chi)

    def _kernel_integrand_nicaea(self, chi, ell):
        """Kernel integrand used with Nicaea wrapper."""
        return (self.wf_a.window_function(chi) *
                self.wf_b.window_function(chi) *
                self.matterspec.power_spectrum(chi, ell) / chi / chi)

    def kernel(self, ln_ell):
        """Returns the kernel value evaluated at ln_ell (value/array)."""
        if not self._initialized_spline:
            self.init_spline()

        return np.where(
            (ln_ell <= self.ln_ell_max) & (ln_ell >= self.ln_ell_min),
            self._kernel_spline(ln_ell), 0.0)


class CorrelationFull(object):
    """Class ...
    """
    def __init__(self, redshift_bins, id_a, id_b,
                 ell_min=5, ell_max=5000,
                 cosmo_dict=None, dictype='ivan',
                 corrtype='all', linear=None):
        """Initialize CorrelationFull class attributes

        """
        self.redshift_bins = redshift_bins
        self.ida = id_a
        self.idb = id_b

        self.ln_ell_min = np.log(ell_min)
        self.ln_ell_max = np.log(ell_max)

        self.dictype = dictype
        self.corrtype = corrtype
        self.linear = linear

        # Init cosmology
        self.cosmo = {}
        # Init redshift boundaries
        self.z_min = 0.0
        self.z_max = 5.0
        # Init integral boundaries
        self.chi_min = 0.0
        self.chi_max = 10000.0

        self.matterspec = None

        if not cosmo_dict:
            import magnification.dictionaries as Dict
            if self.dictype in ['nicaea', 'Nicaea', 'NICAEA']:
                cosmo_dict = Dict.NicaeaCosmodict()
            elif self.dictype in ['Ivan', 'ivan', 'IVAN']:
                cosmo_dict = Dict.IvanCosmodict()
            else:
                cosmo_dict = Dict.Cosmodict()
        self.assign_wf()
        self.set_cosmology(cosmo_dict)

        # dlnell = (self.ln_ell_max - self.ln_ell_min) / 40
        dlnell = (self.ln_ell_max - self.ln_ell_min) / 200
        self._ln_ell_array = np.arange(
            self.ln_ell_min, self.ln_ell_max + dlnell, dlnell)
        self._kernel_array = np.zeros_like(self._ln_ell_array)

        self._kernel_spline = None
        self._initialized_spline = False

    def _init_spline(self):
        """Initialize kernel spline

        """
        for idx, ln_ell in enumerate(self._ln_ell_array):
            kernel = self.raw_kernel(ln_ell)
            self._kernel_array[idx] = kernel

        self._kernel_spline = Intpl.InterpolatedUnivariateSpline(
            self._ln_ell_array, self._kernel_array)

        self._initialized_spline = True

    def get_kernel_spline(self):
        """Return kernel spline

        """
        if not self._initialized_spline:
            self._init_spline()

        return self._kernel_spline

    def assign_wf(self):
        """Assign window functions (delta/kappa)

        """
        if not self.redshift_bins.initialized():
            self.redshift_bins.init_window()

        self.d_a = self.redshift_bins.delta_wf_list[self.ida]
        self.d_b = self.redshift_bins.delta_wf_list[self.idb]
        self.k_a = self.redshift_bins.kappa_wf_list[self.ida]
        self.k_b = self.redshift_bins.kappa_wf_list[self.idb]

    def set_cosmology(self, cosmo_dict):
        """Initialize the cosmology given cosmological parameters
        and calls a power spectrum routine

        """
        self.d_a.set_cosmology(cosmo_dict)
        self.d_b.set_cosmology(cosmo_dict)
        self.k_a.set_cosmology(cosmo_dict)
        self.k_b.set_cosmology(cosmo_dict)

        self.chi_min = self.d_a.chi_min
        self.z_min = self.d_a.z_min
        if self.d_b.chi_min < self.chi_min:
            self.chi_min = self.d_b.chi_min
            self.z_min = self.d_b.z_min

        self.chi_max = self.d_a.chi_max
        self.z_max = self.d_a.z_max
        if self.d_b.chi_max > self.chi_max:
            self.chi_max = self.d_b.chi_max
            self.z_max = self.d_b.z_max

        self.cosmo = Cosmo.CosmoStructure(cosmo_dict, dictype=self.dictype)
        # self.matterspec = Matter.MatterPowerSpectrum(self.cosmo)
        # if self.dictype == 'camb':
        #     self.matterspec = Camb.CambWrapper(cosmo_dict=cosmo_dict)
        # else:
        self.matterspec = Matter.MatterPowerSpectrum(self.cosmo,
                                                     dictype=self.dictype)

        self._initialized_spline = False

    def raw_kernel(self, ln_ell):
        """Returns the integral over chi of the right kernel integrand,
        for a given ln_ell

        """
        ell = np.exp(ln_ell)
        if self.corrtype == 'all':
#            if self.dictype == 'camb':
#                #print('Using camb')
#                #self.matterspec.set_redshift(self.cosmo.redshift(chi))
#                kernel = 0
#                for name in ['auto', 'cross', 'swap', 'lens']:
#                    kernel_type, err = Intg.quad(self._kernel_integrand_camb,
#                                                 self.chi_min, self.chi_max,
#                        args=(ell,name,), limit=200)
#                    kernel += kernel_type
#            else:
#                #print('Using Nicaea')
            kernel = 0
            for name in ['auto', 'cross', 'swap', 'lens']:
                kernel_type, _ = Intg.quad(self._kernel_integrand_nicaea,
                                           self.chi_min, self.chi_max,
                                           args=(ell, name,), limit=200)
                kernel += kernel_type
        else:
#            if self.dictype == 'camb':
#                #self.matterspec.set_redshift(self.cosmo.redshift(chi))
#                kernel, err = Intg.quad(self._kernel_integrand_camb,
#                                        self.chi_min, self.chi_max,
#                    args=(ell, self.corrtype,), limit=200)
#            else:
            kernel, _ = Intg.quad(self._kernel_integrand_nicaea,
                                  self.chi_min, self.chi_max,
                                  args=(ell, self.corrtype,), limit=200)

        return kernel

    def _kernel_integrand_nicaea(self, chi, ell, name):
        """Kernel integrand used with Nicaea wrapper

        """
        if name == 'auto':
            wfs = (self.d_a.window_function(chi) *
                   self.d_b.window_function(chi))
        elif name == 'cross':
            wfs = (self.d_a.window_function(chi) *
                   self.k_b.window_function(chi))
        elif name == 'swap':
            wfs = (self.k_a.window_function(chi) *
                   self.d_b.window_function(chi))
        elif name == 'lens':
            wfs = (self.k_a.window_function(chi) *
                   self.k_b.window_function(chi))
        else:
            wfs = 0

        return (wfs / chi / chi *
                self.matterspec.power_spectrum(chi, ell, self.linear))

#    def _kernel_integrand_camb(self, chi, ell, name):
#        """Kernel integrand used with CAMB wrapper
#        """
#        self.matterspec.set_redshift(self.cosmo.redshift(chi))
#        if name == 'auto':
#            wfs = (self.d_a.window_function(chi) *
#                   self.d_b.window_function(chi))
#        elif name == 'cross':
#            wfs = (self.d_a.window_function(chi) *
#                   self.k_b.window_function(chi))
#        elif name == 'swap':
#            wfs = (self.k_a.window_function(chi) *
#                   self.d_b.window_function(chi))
#        elif name == 'lens':
#            wfs = (self.k_a.window_function(chi) *
#                   self.k_b.window_function(chi))
#        else:
#            wfs = 0
#        return (wfs * self.matterspec.linear_power(ell / chi) /
#            self.cosmo.cosmopar.h**3 / chi / chi)

    def kernel(self, ln_ell):
        """Returns the kernel value evaluated at ln_ell (value/array)."""
        if not self._initialized_spline:
            self._init_spline()
        return np.where(
            (ln_ell <= self.ln_ell_max) & (ln_ell >= self.ln_ell_min),
            self._kernel_spline(ln_ell), 0.0)


def main():
    import pickle
    import magnification.survey as S
    ZTAB = [0.3, 0.7, 1.1, 1.5, 1.9]
    zb = S.RedshiftBinsBasic(ZTAB)
    pmag = {}
    for ifg in xrange(len(ZTAB)):
        pmag[str(ifg)] = {}
        for ibg in xrange(ifg, len(ZTAB)):
            spectr = CorrelationFull(zb, ifg, ibg)
            pmag[str(ifg)][str(ibg)] = spectr.get_kernel_spline()
    with open('pmag.pck', 'w') as fil:
        pickle.dump(pmag, fil)
    print('DONE\a')

if __name__ == '__main__':
    main()
