#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
NAME
  Covariance
DESCRIPTION
  The class MagnificationCovariance computes the covariance matrix of
  galaxy clustering, given a survey and a number of redshift bins.
  The only input is a RedshiftBins object that already contains the
  relevant information.
CLASSES
  MagnificationCovariance
CALLING METHOD
  z_range = [z0, z1, z2, ...] - central position of the bins
  x = MagnificationCovariance(
           z_range
           [, z_min, z_max, delta_z, mag_lim, area, n_gal])
OUTPUT
  m = x.matrix(ell, delta_ell)
  List of the covariance matrices of the magnification
  cross power spectrum between bins for a given ell_mode.
AUTHOR
  Alexandre Boucaud
"""
from __future__ import print_function, division, absolute_import

import os
import copy
from itertools import combinations_with_replacement

import numpy as np

import magnification.correlation as Corr

DEG2RAD = np.pi / 180.

PLTPATH = "/Users/aboucaud/work/LSST/devel/magnification/matrix"

class MagnificationCovariance(object):
    """Class ..
    """
    def __init__(self, redshift_bins, linear=None):
        """Initialize MagnificationCovariance attributes

        """
        self.redshift_bins = redshift_bins
        self.n_bins = redshift_bins.n_bins
        self.dictype = redshift_bins.dictype
        # Area of the survey converted from degrees to radians
        self.area = redshift_bins.area * DEG2RAD ** 2
        self.linear = linear

        self.ell_arr = np.logspace(1, 4, 50)

        self.delta_wf_list = []
        self.kappa_wf_list = []

        self.Pmag = {}
        self.matrix = []

        self._matrix_shape = []
        self._initialized_matrix = False
        self._initialized_spectra = False

    def _init_matrix(self):
        """Initialize the list of matrices that represent the covariance
        in the cross-correlation between backgrounds

        """
        mat_dim = int(0.5 * self.n_bins * (self.n_bins + 1))
        self._matrix_shape = np.zeros((mat_dim, mat_dim))

        self._initialized_matrix = True
        print('=> Matrix list initialized')

    def _init_spectra(self):
        """Initialize delta-delta, delta-kappa and kappa-kappa power
        spectra for the whole redshift range

        """
        if not self.redshift_bins.initialized():
            self.redshift_bins.init_window()

        print('Initializing spectra..')

        # for id_a, id_b in combinations_with_replacement(range(self.n_bins), 2):
        for id_a in xrange(self.n_bins):
            self.Pmag[str(id_a)] = {}
            for id_b in xrange(id_a, self.n_bins):
                pmag_spl = self.__get_correl_spline(id_a, id_b)
                self.Pmag[str(id_a)][str(id_b)] = pmag_spl

        self._initialized_spectra = True
        print('Spectra initialized')

    def __get_correl_spline(self, id_a, id_b):
        """Return a spline of the input correlation function

        """
        mag = Corr.CorrelationFull(self.redshift_bins, id_a, id_b,
                                   dictype=self.dictype, linear=self.linear)
        spline = mag.get_kernel_spline()

        return spline

    def get_covariance(self, ln_ell, lin, col, shotnoiseonly):
        # list all the pairs with repetition in the range(self.n_bins)
        bin_idx = list(combinations_with_replacement(range(self.n_bins), 2))
        idi, idj = bin_idx[lin]
        idk, idl = bin_idx[col]
        if shotnoiseonly:
            cov = (
                self.shot_noise(idi, idk) *
                self.shot_noise(idj, idl)
                +
                self.shot_noise(idi, idl) *
                self.shot_noise(idj, idk))
        else:
            cov = (
                self.powerspectra_full(ln_ell, idi, idk) *
                self.powerspectra_full(ln_ell, idj, idl)
                +
                self.powerspectra_full(ln_ell, idi, idl) *
                self.powerspectra_full(ln_ell, idj, idk))
        return cov

    def powerspectra_sum(self, ln_ell, id_a, id_b):
        """Return all magnification terms with shot noise

        """
        if id_a > id_b:
            return self.Pmag[str(id_b)][str(id_a)](ln_ell)
        else:
            return self.Pmag[str(id_a)][str(id_b)](ln_ell)

    def shot_noise(self, id_a, id_b):
        """Shot noise between bin a and b

        """
        shotn = self.redshift_bins.shot_noise()
        if id_a > id_b:
            return shotn[str(id_b)][str(id_a)]
        else:
            return shotn[str(id_a)][str(id_b)]

    def powerspectra_full(self, ln_ell, id_a, id_b):
        """Cross-correlation power spectrum with shot noise

        """
        return (self.powerspectra_sum(ln_ell, id_a, id_b) +
                self.shot_noise(id_a, id_b))

    def __fill_matrix(self, ell, sno):
        """Compute for a given l-mode value the diagonal terms
        of the covariance matrices

        """
        ln_ell = np.log(ell)
        # for lin, col in combinations_with_replacement(range(len(self.matrix)), 2):
        for lin in xrange(len(self.matrix)):
            for col in xrange(lin, len(self.matrix)):
                self.matrix[lin, col] = self.get_covariance(
                    ln_ell, lin, col, sno)
                if lin != col:
                    self.matrix[col, lin] = self.matrix[lin, col]

    def __make_head(self, ell, delta_ell):
        """Compute the survey dependend factor in front of
        the covariance matrix

        """
        head = 2 * np.pi / (ell * delta_ell) / self.area
        self.matrix *= head

    def get_matrix(self, ell, delta_ell, shotnoiseonly=None):
        """Calling method to retrieve the covariance matrices
        for a given set of (l, delta_l)

        """
        if not self._initialized_matrix:
            self._init_matrix()

        self.matrix = copy.copy(self._matrix_shape)
        if shotnoiseonly:
            self.__fill_matrix(ell, 1)
        else:
            if not self._initialized_spectra:
                self._init_spectra()
            self.__fill_matrix(ell, 0)
        # self.__add_shot_noise()
        self.__make_head(ell, delta_ell)

        return self.matrix



class ShearCovariance(object):
    """Class ..
    """
    def __init__(self, redshift_bins, linear=None):
        """Initialize MagnificationCovariance attributes

        """
        self.redshift_bins = redshift_bins
        self.n_bins = redshift_bins.n_bins
        self.dictype = redshift_bins.dictype
        # Area of the survey converted from degrees to radians
        self.area = redshift_bins.area * DEG2RAD ** 2
        self.linear = linear

        self.ell_arr = np.logspace(1, 4, 50)

        self.delta_wf_list = []
        self.kappa_wf_list = []

        self.sigma_ellipticite = 0.35

        self.Pshear = {}
        self.matrix = []

        self._matrix_shape = []
        self._initialized_matrix = False
        self._initialized_spectra = False

    def _init_matrix(self):
        """Initialize the list of matrices that represent the covariance
        in the cross-correlation between backgrounds

        """
        mat_dim = int(0.5 * self.n_bins * (self.n_bins + 1))
        self._matrix_shape = np.zeros((mat_dim, mat_dim))

        self._initialized_matrix = True
        print('=> Matrix list initialized')

    def _init_spectra(self):
        """Initialize delta-delta, delta-kappa and kappa-kappa power
        spectra for the whole redshift range

        """
        if not self.redshift_bins.initialized():
            self.redshift_bins.init_window()

        print('Initializing spectra..')

        # for id_a, id_b in combinations_with_replacement(range(self.n_bins), 2):
        for id_a in xrange(self.n_bins):
            self.Pshear[str(id_a)] = {}
            for id_b in xrange(id_a, self.n_bins):
                spectrum_spl = self.__get_correl_spline(id_a, id_b)
                self.Pshear[str(id_a)][str(id_b)] = spectrum_spl

        self._initialized_spectra = True
        print('Spectra initialized')

    def __get_correl_spline(self, id_a, id_b):
        """Return a spline of the input correlation function

        """
        mag = Corr.CorrelationFull(self.redshift_bins, id_a, id_b,
                                   dictype=self.dictype, linear=self.linear,
                                   corrtype='lens')
        spline = mag.get_kernel_spline()

        return spline

    def get_covariance(self, ln_ell, lin, col, shotnoiseonly):
        # list all the pairs with repetition in the range(self.n_bins)
        bin_idx = list(combinations_with_replacement(range(self.n_bins), 2))
        idi, idj = bin_idx[lin]
        idk, idl = bin_idx[col]
        if shotnoiseonly:
            cov = (
                self.shot_noise(idi, idk) *
                self.shot_noise(idj, idl)
                +
                self.shot_noise(idi, idl) *
                self.shot_noise(idj, idk))
        else:
            cov = (
                self.powerspectra_full(ln_ell, idi, idk) *
                self.powerspectra_full(ln_ell, idj, idl)
                +
                self.powerspectra_full(ln_ell, idi, idl) *
                self.powerspectra_full(ln_ell, idj, idk))
        return cov

    def powerspectra_sum(self, ln_ell, id_a, id_b):
        """Return all magnification terms with shot noise

        """
        if id_a > id_b:
            return self.Pshear[str(id_b)][str(id_a)](ln_ell)
        else:
            return self.Pshear[str(id_a)][str(id_b)](ln_ell)

    def shot_noise(self, id_a, id_b):
        """Shot noise between bin a and b

        """
        shotn = self.redshift_bins.shot_noise()
        if id_a > id_b:
            return self.sigma_ellipticite * shotn[str(id_b)][str(id_a)]
        else:
            return self.sigma_ellipticite * shotn[str(id_a)][str(id_b)]

    def powerspectra_full(self, ln_ell, id_a, id_b):
        """Cross-correlation power spectrum with shot noise

        """
        return (self.powerspectra_sum(ln_ell, id_a, id_b) +
                self.shot_noise(id_a, id_b))

    def __fill_matrix(self, ell, sno):
        """Compute for a given l-mode value the diagonal terms
        of the covariance matrices

        """
        ln_ell = np.log(ell)
        # for lin, col in combinations_with_replacement(range(len(self.matrix)), 2):
        for lin in xrange(len(self.matrix)):
            for col in xrange(lin, len(self.matrix)):
                self.matrix[lin, col] = self.get_covariance(
                    ln_ell, lin, col, sno)
                if lin != col:
                    self.matrix[col, lin] = self.matrix[lin, col]

    def __make_head(self, ell, delta_ell):
        """Compute the survey dependend factor in front of
        the covariance matrix

        """
        head = 2 * np.pi / (ell * delta_ell) / self.area
        self.matrix *= head

    def get_matrix(self, ell, delta_ell, shotnoiseonly=None):
        """Calling method to retrieve the covariance matrices
        for a given set of (l, delta_l)

        """
        if not self._initialized_matrix:
            self._init_matrix()

        self.matrix = copy.copy(self._matrix_shape)
        if shotnoiseonly:
            self.__fill_matrix(ell, 1)
        else:
            if not self._initialized_spectra:
                self._init_spectra()
            self.__fill_matrix(ell, 0)
        # self.__add_shot_noise()
        self.__make_head(ell, delta_ell)

        return self.matrix


def plot_matrix(mcov, lmin, lmax, nval=10):
    """Plots the covariance matrix with colorbar

    """
    import matplotlib.pyplot as plt
    pltpath = '/Users/alexandreboucaud/work/svn/lsst/Amplification/trunk/magnification/matrix2/'
    ell_base = np.logspace(np.log10(lmin), np.log10(lmax), nval)
    dell = np.diff(ell_base)
    ell = ell_base[:-1] + 0.5 * dell
    ticklabel = list(combinations_with_replacement(range(mcov.n_bins), 2))
    # ticklabel = [(0, 0), (0, 1), (0, 2), (1, 1), (1, 2), (2, 2)]
    for i in xrange(nval - 1):
        mat = mcov.get_matrix(ell[i], dell[i])
        np.savetxt(pltpath + 'matrix_{}.txt'.format(i), mat)
        normmat = np.zeros_like(mat)
        for u in xrange(len(mat)):
            for v in xrange(len(mat)):
                normmat[u,v] = mat[u,v] / np.sqrt(mat[u,u] * mat[v,v])
        plt.figure(i)
        plt.title(r'$\ell = %.1f$' % (ell[i]))
        plt.imshow(np.log10(normmat), interpolation='nearest', origin='lower', alpha=0.8)
        plt.colorbar()
        plt.xticks(np.arange(len(mat)), ticklabel, rotation=45)
        plt.yticks(np.arange(len(mat)), ticklabel)
        plt.savefig(pltpath + 'plotmat_{}.png'.format(i), dpi=144)


def save_matrix():
    ZTAB = [0.3, 0.7, 1.1, 1.5, 1.9]
    ELLMIN, ELLMAX, NELL = 10, 3000, 30
    ell_base = np.logspace(np.log10(ELLMIN), np.log10(ELLMAX), NELL)
    dell = np.diff(ell_base)
    ell = ell_base[:-1] + 0.5 * dell

    from magnification.survey import RedshiftBinsBasic
    rb = RedshiftBinsBasic(ZTAB, survey='LSST', dictype="ivan")
    mc = MagnificationCovariance(rb)
    sc = ShearCovariance(rb)

    filepattern = "covmatrix_{}_{:02d}.dat"
    filepath = os.path.join(PLTPATH, filepattern)
    for imat in xrange(len(ell)):
        magmat = mc.get_matrix(ell[imat], dell[imat])
        shmat = sc.get_matrix(ell[imat], dell[imat])
        np.savetxt(filepattern.format('mag', imat), magmat)
        np.savetxt(filepattern.format('shear', imat), shmat)


def main():
    save_matrix()
    # plot_matrix(mc, ELLMIN, ELLMAX, NELL)
    # import matplotlib.pyplot as plt
    # plt.figure(1)
    # plt.imshow(mc.get_matrix(ell[0], dell[0], 1) / mc.get_matrix(ell[0], dell[0]),
    #     interpolation='nearest', alpha=0.7)
    # plt.figure(2)
    # plt.imshow(mc.get_matrix(ell[5], dell[5], 1) / mc.get_matrix(ell[5], dell[5]),
    #     interpolation='nearest', alpha=0.7)
    # plt.figure(3)
    # plt.imshow(mc.get_matrix(ell[9], dell[9], 1) / mc.get_matrix(ell[9], dell[9]),
    #     interpolation='nearest', alpha=0.7)

if __name__ == '__main__':
    main()
