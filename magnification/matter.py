"""
NAME
    Matter
SUMMARY
    This contains several classes aimed at creating power spectra.
DESCRIPTION
    The matter power spectrum is computed with a wrapper of the C-code NICAEA
    written by M.Kilbinger. The non-linear transfer function comes from
    Smith et al. (2003) and the fitting function for BAO and wiggles features
    from Eisenstein & Hu (1998)
    CovariancePS is a class that can compute autocorrelation spectra
    (P_delta_delta) and lensing cross power spectra (P_kappa_delta)
CALLING METHODS
    - MatterPowerspectrum (redshift=0. [,cosmodict]) - main class
    - CrossPS (Input: redshifts, cosmo dictionary, survey dictionary
      [, dz, alpha1, alpha2, b, linear])
    - CovariancePS (Input: z_val, survey dictionary
      [, dz, cosmo dictionary, alpha1, alpha2, b, linear])
AUTHOR
    Alexandre Boucaud
"""
from __future__ import print_function, division, absolute_import

import numpy as np
import matplotlib.pyplot as plt

import scipy.integrate as Intg

import magnification.dictionaries as Dict
import magnification.cosmology as Cosmo
import magnification.Cspectrum as Cspectrum

blue = "#348ABD"
red = "#A60628"
violet = "#7A68A6"
green = "#467821"
orange = "#E24A33"
pink = '#F53374'

class MatterPowerSpectrum(object):
    """Wrapper class for Martin Kilbinger's C code Nicea.

    It produce a matter power spectrum in the linear
    and non-linear regime.
    INPUT: cosmology (WMAP7 by default)
    Main function: P(chi, ell, *linear)

    """
    def __init__(self, cosmo_str=None, dictype='nicaea'):
        """Initialize MatterPowerSpectrum attributes.

        param | cosmo_str: cosmological structure
        kwd   | dictype: cosmological parameters dictionary type

        """
        self.cosmo = cosmo_str
        self.dictype = dictype
        if not cosmo_str:
            if self.dictype in ['wmap', 'Wmap', 'WMAP']:
                cosmo_dict = Dict.Cosmodict()
            elif self.dictype in ['ivan', 'Ivan', 'IVAN']:
                cosmo_dict = Dict.IvanCosmodict()
            else:
                cosmo_dict = Dict.NicaeaCosmodict()
            self.cosmo = Cosmo.CosmoStructure(
                cosmo_dict, dictype=self.dictype)

        self._kmin = 0.001
        self._kmax = 10.0

        self._fiducial_nicaea = Cspectrum.Cosmo(
            Dict.NicaeaCosmodict())

        self.fig = None

        self.nicaea = None
        self._initialized_nicaea = False

    def set_cosmo(self, cosmo_dict):
        """Set the cosmological parameters."""
        self.cosmo.set_cosmology(cosmo_dict)

        self._initialized_nicaea = False

    def get_nicaea_dict(self):
        """Nicaea input parameters."""
        return self.cosmo.cosmopar

    def _init_nicaea(self):
        """Calls the C-code to compute the power spectrum"""
        if self.dictype == 'wmap':
            self.nicaea = Cspectrum.Cosmo(self.get_nicaea_dict())
        else:
            self.nicaea = Cspectrum.Cosmo(self.cosmo.cosmopar)

        self._initialized_nicaea = True

    def power_spectrum(self, chi, ell, linear=None):
        """Main call for the matter power spectrum

        INPUT:
         - chi = the comoving distance
         - ell = the 2-mode on the sphere
         - *linear = keyword to select linear or not
         (non-linear by default)
        OUTPUT: P(l/chi,chi)

        """
        if not self._initialized_nicaea:
            self._init_nicaea()

        scalef = self.cosmo.scale_factor(self.cosmo.redshift(chi))
        if linear:
            return (self.nicaea.P_L(scalef, ell / chi) /
                    self.cosmo.cosmopar.h ** 3)
        else:
            return (self.nicaea.P_NL(scalef, ell / chi) /
                    self.cosmo.cosmopar.h ** 3)

    def plot(self, redshift=0.):
        """Plot the 3D matter power spectrum"""
        if not self._initialized_nicaea:
            self._init_nicaea()

        a_z = 1. / (1. + redshift)
        k_arr = np.logspace(-3, 2, 100)
        psl = np.array([self.nicaea.P_L(a_z, k) for k in k_arr])
        psnl = np.array([self.nicaea.P_NL(a_z, k) for k in k_arr])
        self.fig = plt.figure(figsize=(6, 6))
        ax = self.fig.add_subplot(111)
        ax.loglog(k_arr, psl, color=red, lw=2., label="Linear spectrum")
        ax.loglog(k_arr, psnl, color=blue, lw=2., label="Non linear spectrum")
        ax.set_xlabel(r'$k\ [h\,{\rm Mpc}^{-1}]$')
        ax.set_ylabel(r'$P(k)\ [(h^{-1}\,{\rm Mpc})^3]$')
        ax.set_xlim(1e-3, 1e1)
        ax.set_ylim(1, 1e5)
        plt.text(0.05, 0.05, "3D matter power spectrum",
                 fontsize=16, transform=ax.transAxes)
        plt.legend(loc=0, frameon=False)
        plt.show()

    def close_fig(self):
        self.fig.close()

    def as2sigma(self, scalar_amp, radius, chi):
        """Conversion from scalar amplitude A_s to sigma8 normalization."""
        k0 = 0.002 * self.cosmo.cosmopar.h
        if chi <= 1.:
            a = 1
        else:
            a = self.cosmo.scale_factor(self.cosmo.redshift(chi))
        Pk0 = self._fiducial_nicaea.P_L(a, k0) / self.cosmo.cosmopar.h ** 3
        sigma8 = self.sigma_8()
        norm = k0 ** 3 / (2 * np.pi ** 2)
        scalar_amp_true = scalar_amp / norm
        return np.sqrt(scalar_amp_true / Pk0) * sigma8

    def sigma8(self):
        return self.sigma_r(8.0)

    def sigma_r(self, radius, a=1.):
        """Return the integral of the density fluctuation in
        a sphere of radius R at the age a"""
        k_arr = np.logspace(
            np.log10(self._kmin), np.log10(self._kmax), 1000)
        int_arr = self._sigma_integrand(k_arr, radius, a)
        val = Intg.simps(int_arr, k_arr)
        return np.sqrt(val / (2 * np.pi ** 2))

    def findRoots(self):
        import scipy.optimize as Op
        km = self._kmax
        karr = np.arange(km) + 1.  # np.arange(self._kmax) / float(self._kmax)
        # karr = np.append(karr, np.arange(self._kmax)+1.)
        rootlist = [self._kmin]
        for k in karr:
            root = Op.newton(self._sigma_wf, k)
            if root > 0:
                rootlist.append(root)
        if rootlist[-1] < km:
            rootlist.append(km)
        return np.unique(np.round(rootlist, 5))

    def _sigma_integrand(self, k, radius, a):
        """Integrand for sigma_r() method."""
        if not self._initialized_nicaea:
            self._init_nicaea()
        kR = radius * k
        WF = self._sigma_wf(kR)
        Pk = self._fiducial_nicaea.P_L(a, k)
        return k ** 2 * Pk * WF ** 2

    def _sigma_wf(self, x):
        """Sigma integrand window function."""
        sph_bessel = (np.sin(x) / x ** 2 - np.cos(x) / x)
        return 3 * sph_bessel / x


def main():
    mps = MatterPowerSpectrum()
    try:
        mps.plot()
    except:
        print("Issue during the run, try reinstalling")
    else:
        print("Installation successful")


if __name__ == '__main__':
    main()
