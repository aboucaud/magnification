"""
NAME
  WindowFunction
DESCRIPTION

CALLING METHOD

OUTPUT METHOD

AUTHOR
    Alexandre Boucaud
"""
from __future__ import print_function, division, absolute_import

import numpy as np
import scipy.interpolate as Intp
import scipy.integrate as Intg

import magnification.cosmology as Cosmo
import magnification.dictionaries as Dict


class WindowFunction(object):
    """Base class for an angular correlation window function"""
    def __init__(self, z_min, z_max, cosmo_dict=None, dictype='normal'):
        """Initialize WindowFunction attributes

        """
        self.z_min = z_min
        self.z_max = z_max
        self.dictype = dictype
        if not cosmo_dict:
            if self.dictype in ['wmap', 'Wmap', 'WMAP']:
                cosmo_dict = Dict.Cosmodict()
            elif self.dictype in ['ivan', 'Ivan', 'IVAN']:
                cosmo_dict = Dict.IvanCosmodict()
            else:
                cosmo_dict = Dict.NicaeaCosmodict()
        self.cosmo = None
        self.set_cosmology(cosmo_dict)

        self.chi_min = self.cosmo.comoving_distance(0.0, self.z_min)
        self.chi_max = self.cosmo.comoving_distance(0.0, self.z_max)

        self.n_chi = 200  # empirical number
        self._window_spline = None
        self._initialized_spline = False

    def set_cosmology(self, cosmo_dict):
        """Set the cosmological parameters

        """
        self.cosmo = Cosmo.CosmoStructure(
            cosmo_dict, dictype=self.dictype)

        self._initialized_spline = False

    def _init_spline(self):
        """Initialize window function spline

        """
        self._chi_array = np.linspace(self.chi_min, self.chi_max, self.n_chi)
        self._window_array = np.zeros_like(self._chi_array)

        for idx, chi in enumerate(self._chi_array):
            self._window_array[idx] = self.raw_window_function(chi)
        self._window_spline = Intp.InterpolatedUnivariateSpline(
            self._chi_array, self._window_array)

        self._initialized_spline = True

    def raw_window_function(self, chi):
        """Useful for init purpose

        """
        return 1.0

    def window_function(self, chi):
        """Return the window value for a given comoving distance

        """
        if not self._initialized_spline:
            self._init_spline()

        return np.where((chi > self.chi_min) & (chi < self.chi_max),
                        self._window_spline(chi), 0.0)

    def write(self, output_file_name):
        """Write the window function in an output file

        """
        outputfile = open(output_file_name, "w")
        for chi, wfunc in zip(self._chi_array, self._window_array):
            outputfile.write("%1.10f %1.10f\n" % (chi, wfunc))
        outputfile.close()
        print('Window function output to {}'.format(output_file_name))


class WindowFunctionGalaxy(WindowFunction):
    """WindowFunction subclass for a galaxy distribution:

    W(chi) = dN/dz dz/dchi

    """
    def __init__(self, dndz, z_min, z_max,
                 cosmo_dict=None, dictype='normal',
                 isspline=False):
        """Initialize class attributes

        """
        WindowFunction.__init__(self, z_min, z_max, cosmo_dict, dictype)
        self.dndz_bin = dndz
        self.isspline = isspline
        if not isspline:
            self.norm = self.dndz_bin.normalize()

    def raw_window_function(self, chi):
        """Return galaxy raw window function

        """
        redshift = self.cosmo.redshift(chi)
        dzdchi = self.cosmo.dzdchi(redshift)
        if self.isspline:
            wf = dzdchi * self.dndz_bin(redshift)
        else:
            wf = dzdchi * self.norm * self.dndz_bin.dndz(redshift)

        return wf


class WindowFunctionConvergence(WindowFunction):
    """WindowFunction subclass for magnification of a background sample.

    W(chi) = 3*omega_m*g(chi)/a
    where we have omitted the (2.5*s - 1) factor that is due to the number count
    slope of the sample and where
    g(chi) = chi*int(chi, inf, dN/dz dz/dchi' (1.0 - chi/chi'))

    """
    def __init__(self, dndz, z_min, z_max,
                 cosmo_dict=None, dictype='normal',
                 isspline=False):
        """Class constructor

        """
        # Even though the input distribution may only extend between some bounds
        # in redshift, the lensing kernel will extend across z = [0, z_max]
        WindowFunction.__init__(self, 0.0, z_max, cosmo_dict, dictype)

        self.n_chi = 120  # Value tuned to a good balance between accuracy and
                          # CPU time
        self.isspline = isspline
        if not isspline:
            self.norm = dndz.normalize()

        self.dndz_bin = dndz

        self._g_chi_min = self.cosmo.comoving_distance(0.0, z_min)

    def raw_window_function(self, chi):
        """Return magnification raw window function

        """
        scale_factor = 1.0 / (1.0 + self.cosmo.redshift(chi))
        H0c = 1.0 / self.cosmo.hubble_distance()

        chi_min = chi
        if chi_min < self._g_chi_min:
            chi_min = self._g_chi_min

        g_chi, _ = Intg.quad(self._lensing_integrand,
                             chi_min, self.chi_max,
                             args=(chi,), limit=200)
        g_chi *= 3.0 / 2.0 * self.cosmo.cosmopar.omega_m  * H0c ** 2

        return g_chi * chi / scale_factor

    def _lensing_integrand(self, chi, chi0):
        """Return lensing kernel

        """
        redshift = self.cosmo.redshift(chi)
        dzdchi = self.cosmo.dzdchi(redshift)
        if self.isspline:
            dndz = self.dndz_bin(redshift)
        else:
            # self.dndz_bin.normalize()
            dndz = self.norm * self.dndz_bin.dndz(redshift)

        return dzdchi * dndz * (chi - chi0) / chi
