#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
NAME
  Cosmology
DESCRITPION
  Class contains functions that derive cosmological values
  for a given set of cosmological parameters (dictionary)
CLASS
  CosmoStructure
CALLING METHOD
  cosmo = CosmoStructure(
             [cosmo_dictionary])
OUTPUT
  cosmo.scale_factor(z) - scale factor
  cosmo.w(z) - dark energy equation-of-state parameter w(z)
  cosmo.hubble_distance(z) - hubble distance
  cosmo.hubble_rate(z) - hubble parameter
  cosmo.Dc(z, cosmo) = comoving distance
  ...
AUTHOR
  Alexandre Boucaud
"""
from __future__ import print_function, division, absolute_import

import numpy as np
import scipy.interpolate as Intp
import scipy.integrate as Intg
import scipy.special as Sp

import magnification.dictionaries as Dict

# speed of light in km.s^{-1}
C_LIGHT = 2.99792458e5


class CosmoStructure(object):
    """Class...
    """
    def __init__(self, cosmo_dict=None, dictype='nicaea'):
        """Initialize CosmoStructure attributes

        Parameters
        ----------
        cosmo_dict: dict, optional
            Input cosmological parameter set (default `None`)
        dictype: str, optional
            Name of cosmological parameter set
                * 'ivan':
                    Ivan cosmological parameter set
                * 'nicaea':
                    NICAEA default cosmological parameter set (default)
                * 'wmap':
                    WMAP 7Y cosmological parameter set

        """
        self.dictype = dictype
        if not cosmo_dict:
            if self.dictype in ['wmap', 'Wmap', 'WMAP']:
                cosmo_dict = Dict.Cosmodict()
            elif self.dictype in ['ivan', 'Ivan', 'IVAN']:
                cosmo_dict = Dict.IvanCosmodict()
            else:
                cosmo_dict = Dict.NicaeaCosmodict()

        self.cosmopar = {}
        self.set_cosmology(cosmo_dict)

        self.z_min = 1.e-3
        self.z_max = 5.0

        self._z_array = np.linspace(self.z_min, self.z_max, 60)

        self._chi_array = np.array([])
        self._z_spline = None
        self._chi_spline = None
        self._initialized_splines = False

    def set_cosmology(self, new_cosmo_dict):
        """
        Provide a cosmological parameters dictionary
        to the _init_parameters() method.

        Parameters
        ----------
        new_cosmo_dict: dict
            Input dictionary of cosmological parameters

        """
        self.cosmopar = {}
        self._init_parameters(new_cosmo_dict)

        self._initialized_splines = False

    def _init_parameters(self, cosmo_dict):
        """
        Initialize cosmological parameters.

        Parameters
        ----------
        cosmo_dict: dict
            Input dictionary of cosmological parameters

        """
        self.cosmopar = Dict.NicaeaCosmodict()
        self.cosmopar.h = cosmo_dict.h
        self.cosmopar.w0 = cosmo_dict.w0
        self.cosmopar.wa = cosmo_dict.wa
        self.cosmopar.n_s = cosmo_dict.n_s
        self.cosmopar.norm = cosmo_dict.norm
        if self.dictype in ['wmap', 'Wmap', 'WMAP']:
            self.cosmopar.omega_k = cosmo_dict.Omk
            self.cosmopar.omega_m = (
                (cosmo_dict.omc + cosmo_dict.omb) / self.cosmopar.h**2)
            self.cosmopar.omega_l = (
                1.0 - self.cosmopar.omega_m - self.cosmopar.omega_k)
            self.cosmopar.omega_b = cosmo_dict.omb / self.cosmopar.h**2
        else:
            self.cosmopar.omega_k = cosmo_dict.omega_k
            self.cosmopar.omega_m = cosmo_dict.omega_m
            self.cosmopar.omega_l = cosmo_dict.omega_l
            self.cosmopar.omega_b = cosmo_dict.omega_b

    def scale_factor(self, z):
        """
        Computes the cosmological scale factor

        Parameters
        ----------
        z: `numpy.ndarray`
            Redshift array

        Returns
        -------
        a(z): `numpy.ndarray`
            The cosmological scale factor at the given input redshift(s)

        """
        return 1.0 / (1. + np.array(z))

    def w(self, z):
        """
        Compute the dark energy equation-of-state parameter

        Parameters
        ----------
        z: `numpy.ndarray`
            Redshift array

        Returns
        -------
        w(z): `numpy.ndarray`
            The dark energy equation-of-state parameter at the given
            input redshift(s)

        """
        return (self.cosmopar.w0 +
                self.cosmopar.wa * (1. - self.scale_factor(z)))

    def _dark_energy_integrand(self, z):
        """
        The integration kernel for the redshift dependance of the
        dark energy.

        Parameters
        ----------
        z: float > 0
            A redshift

        Returns
        -------
        (1 + w(z)) / (1 + z)

        """
        # assert z >= 0
        return (1. + self.w(z)) / (1. + z)

    def omega_l(self, z):
        """
        The dark energy fraction of the total energy content

        Parameters
        ----------
        z: `numpy.ndarray`
            Redshift array

        Returns
        -------
        Omega_l(z): `numpy.ndarray`
            The dark energy fraction at the given input redshift(s)

        """
        func = np.vectorize(
            lambda z: Intg.quad(self._dark_energy_integrand, 0, z, limit=200))
        val, _ = func(z)

        return self.cosmopar.omega_l * np.exp(3. * val)

    def hubble_distance(self, z=0.0):
        """
        The Hubble distance c/H(z)

        Parameters
        ----------
        z: `numpy.ndarray`, optional
            Redshift array (default 0.0)

        Returns
        -------
        d_H(z): `numpy.ndarray`
            The Hubble distance at the given input redshift(s)

        """
        return C_LIGHT / self.hubble_rate(z)

    def hubble_rate(self, z):
        """
        The Hubble parameter H(z)

        Parameters
        ----------
        z: `numpy.ndarray`
            Redshift array

        Returns
        -------
        H(z): `numpy.ndarray`
            The Hubble parameter at the given input redshift(s)

        """
        return 100. * self.cosmopar.h * self.hubble_ratio(z)

    def hubble_ratio(self, z):
        """
        The Hubble ratio E(z) = H(z) / H0

        Parameters
        ----------
        z: `numpy.ndarray`
            Redshift array

        Returns
        -------
        E(z): `numpy.ndarray`
            The Hubble ratio at the given input redshift(s)

        """
        return np.sqrt(self.cosmopar.omega_k * (1.+z)**2 +
                       self.cosmopar.omega_m * (1.+z)**3 +
                       self.omega_l(z))

    def _ez_inverse_integrand(self, z):
        """
        The integration kernel 1 / E(z) for the cosmological distances

        Parameters
        ----------
        z: float
            A redshift

        Returns
        -------
        1 / E(z)

        """
        return 1. / self.hubble_ratio(z)

    def comoving_distance(self, z1, z2):
        """
        The comoving distance between two redshifts (in Mpc)

        Parameters
        ----------
        z1: `numpy.ndarray`
            Redshift array
        z2: `numpy.ndarray`
            Redshift array

        Returns
        -------
        chi(z1, z2): `numpy.ndarray`
            The comoving distance between z1 and z2

        """
        func = np.vectorize(
            lambda z1, z2: Intg.quad(self._ez_inverse_integrand,
                                     z1, z2, limit=200))
        val, _ = func(z1, z2)

        return np.abs(val) * self.hubble_distance()

    def dm(self, z):
        """
        Transverse comoving distance (Mpc)

        Parameters
        ----------
        z: `numpy.ndarray`
            Redshift array

        Returns
        -------
        chi_trans(z): `numpy.ndarray`
            The transverse comoving distance to the given input
            redshift(s)

        """
        dh = self.hubble_distance()
        # dc = self.comoving_distance(0.0, z)
        dc = self.dc0(z)
        if self.cosmopar.omega_k == 0.0:
            return dc
        elif self.cosmopar.omega_k > 0:
            sqrt_curv = np.sqrt(self.cosmopar.omega_k)
            return dh / sqrt_curv * np.sinh(sqrt_curv * dc / dh)
        else:
            sqrt_curv = np.sqrt(-self.cosmopar.omega_k)
            return dh / sqrt_curv * np.sin(sqrt_curv * dc / dh)

    def angular_distance(self, z1, z2):
        """Angular diameter distance between two redshifts (Mpc)

        Parameters
        ----------
        z1: `numpy.ndarray`
            Redshift array
        z2: `numpy.ndarray`
            Redshift array

        Returns
        -------
        chi_ang(z1, z2): `numpy.ndarray`
            The angular diameter distance between z1 and z2

        See Hogg astroph/9905116

        """
        z1 = np.array(z1)
        z2 = np.array(z2)
        dm1 = self.dm(z1)
        dm2 = self.dm(z2)
        omk = self.cosmopar.omega_k
        if omk == 0:
            return (dm2-dm1) * self.scale_factor(z2)
        elif omk > 0:
            dh = self.hubble_distance()
            part1 = dm1 * np.sqrt(1. + omk * (dm2/dh)**2.)
            part2 = dm2 * np.sqrt(1. + omk * (dm1/dh)**2.)
            return (part2 - part1) * self.scale_factor(z2)
        else:
            raise ValueError("Formula non valid for that value of omega_k")

    def _init_z_chi_spline(self):
        """Initialize redshift and comoving distance splines"""
        self._chi_array = self.comoving_distance(0., self._z_array)
        self._chi_spline = Intp.InterpolatedUnivariateSpline(
            self._z_array, self._chi_array)
        self._z_spline = Intp.InterpolatedUnivariateSpline(
            self._chi_array, self._z_array)
        self._initialized_splines = True

    def dc0(self, z):
        """
        Comoving distance between a given redshift and us (Mpc)

        Parameters
        ----------
        z: `numpy.ndarray`
            Redshift array

        Returns
        -------
        chi(0, z): `numpy.ndarray`
            The comoving distance to the given input redshift(s)

        """
        if not self._initialized_splines:
            self._init_z_chi_spline()
        z = np.array(z)

        return np.where((z >= self.z_min) & (z <= self.z_max),
                        self._chi_spline(z), 0.0)

    def da0(self, z):
        """Angular diameter distance Da(z) in Mpc

        Parameters
        ----------
        z: `numpy.ndarray`
            Redshift array

        Returns
        -------
        chi_ang(0, z): `numpy.ndarray`
            The angular diameter distance to the given input redshift(s)

        """
        return self.dm(z) * self.scale_factor(z)

    def dl0(self, z):
        """Luminosity distance Dl(z) in Mpc

        Parameters
        ----------
        z: `numpy.ndarray`
            Redshift array

        Returns
        -------
        chi_lum(0, z): `numpy.ndarray`
            The luminosity distance to the given input redshift(s)

        """
        return self.dm(z) / self.scale_factor(z)

    def growth_factor(self, z):
        """
        Cosmological growth factor

        Parameters
        ----------
        z: `numpy.ndarray`
            Redshift array

        Returns
        -------
        D_+(z): `numpy.ndarray`
            The growth factor at redshift z

        Fitting formula for the growth factor from Juszkiewicz et al. (2009)
        ==> arXiv:0901.0697

        """
        a = self.scale_factor(z)
        D_inf = (2.0 * Sp.gamma(2/3.) * Sp.gamma(11/6.) *
                (self.cosmopar.omega_m / (1.0-self.cosmopar.omega_m))**(1/3.) /
                 np.sqrt(np.pi))

        return a / (1.0 + (a / D_inf)**(2.3))**(1/2.3)

    def redshift(self, chi):
        """
        Redshift corresponding to comoving distance chi

        Parameters
        ----------
        chi: `numpy.ndarray`
            Comoving distance array

        Returns
        -------
        z(chi): `numpy.ndarray`
            Redshift array corresponding to the comoving distances

        """
        if not self._initialized_splines:
            self._init_z_chi_spline()
        return self._z_spline(chi)

    def dchidz(self, z):
        """
        Comoving distance gradient

        Parameters
        ----------
        z: float
            A redshift

        Returns
        -------
        [dchi / dz](z): float
            The comoving distance gradient evaluated at redshift z

        """
        if not self._initialized_splines:
            self._init_z_chi_spline()
        return self._chi_spline.derivatives(z)[1]

    def dzdchi(self, z):
        """Redshift gradient

        Parameters
        ----------
        z: float
            A redshift

        Returns
        -------
        [dz / dchi](z): float
            The redshift gradient wrt the comoving distance
            evaluated at redshift z

        """
        return 1.0 / self.hubble_distance(z)
