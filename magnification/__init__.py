"""
Magnification library
---------------------
Library that uses the matter power spectrum calculated by an external
code `Nicaea` (see README) to compute a weak lensing effect called
"cosmic magnification" that is the result of light amplification from
dark matter overdensities along the line of sight.

This code aims at quantifying the impact of this effect to constrain
the cosmological parameters of the Standard Model.

Written by: Alexandre Boucaud <boucaud.alexandre@gmail.com>
"""
from . import correlation
from . import cosmology
from . import covariance
from . import dictionaries
from . import fisher
from . import matter
from . import survey
from . import windowfunction
