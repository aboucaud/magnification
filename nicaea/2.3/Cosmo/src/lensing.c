/* ============================================================ *
 * lensing.c							*
 *								*
 * Martin Kilbinger, Karim Benabed 2006, 2007, 2008		*
 * With many thanks to P. Schneider, J. Hartlap and P. Simon.	*
 * ============================================================ */


#include "lensing.h"

/* TODO: Not hard-code these halomodel parameters */
#define C0        9.0
#define ALPHA_NFW 1.0
#define BETA_NFW  0.13
#define MASSFCT   st2


cosmo_lens *init_parameters_lens(double OMEGAM, double OMEGADE, double W0_DE, double W1_DE, 
				 double H100, double OMEGAB, double OMEGANUMASS,
				 double NEFFNUMASS, double NORM, double NSPEC,
				 int Nzbin, const int *Nnz, const nofz_t *nofz, double *par_nz,
				 nonlinear_t NONLINEAR, transfer_t TRANSFER,
				 growth_t GROWTH, de_param_t DEPARAM,
				 norm_t NORMMODE, tomo_t TOMO, reduced_t REDUCED, double Q_MAG_SIZE, sm2_error **err)
{
   cosmo_lens *res;
   double amin;

   res = malloc_err(sizeof(cosmo_lens), err);  forwardError(*err, __LINE__, NULL);

   res->redshift = init_redshift(Nzbin, Nnz, nofz, par_nz, NULL, err); forwardError(*err, __LINE__, NULL);

   amin = get_amin(res->redshift, err);
   forwardError(*err, __LINE__, NULL);

   res->cosmo = init_parameters(OMEGAM, OMEGADE, W0_DE, W1_DE, H100, OMEGAB, OMEGANUMASS,
				NEFFNUMASS, NORM, NSPEC, NONLINEAR, TRANSFER, GROWTH, DEPARAM,
				NORMMODE, amin, err);
   forwardError(*err, __LINE__, NULL);

   /* Lensing parameters */
   res->tomo       = TOMO;
   res->reduced    = REDUCED;
   res->q_mag_size = Q_MAG_SIZE;

   if (NONLINEAR==halodm) {
      res->hm = init_parameters_hm(res->cosmo->Omega_m, res->cosmo->Omega_de, res->cosmo->w0_de, res->cosmo->w1_de,
				   res->cosmo->h_100, res->cosmo->Omega_b, res->cosmo->Omega_nu_mass,
				   res->cosmo->Neff_nu_mass, res->cosmo->normalization, res->cosmo->n_spec,
				   res->redshift->Nzbin, res->redshift->Nnz, res->redshift->nofz,
				   res->redshift->par_nz,
				   halodm, res->cosmo->transfer, res->cosmo->growth, res->cosmo->de_param,
				   res->cosmo->normmode,
				   C0, ALPHA_NFW, BETA_NFW, MASSFCT, 0.0, 0.0, 0.0, 0.0, 0.0, hod_none, err);
      forwardError(*err, __LINE__, NULL);
   } else {
      res->hm = NULL;
   }

   /* Reset pre-computed tables */
   res->g_source  = NULL;
   res->Pshear    = NULL;
   res->Pg1       = NULL;
   res->xiP       = NULL;
   res->xiM       = NULL;
   res->gamma     = NULL;
   res->map_gauss = NULL;
   res->map_poly  = NULL;
 
   return res;
}

cosmo_lens* copy_parameters_lens_only(cosmo_lens* source, sm2_error **err)
{
   cosmo_lens *res;

   res = init_parameters_lens(source->cosmo->Omega_m,source->cosmo->Omega_de, source->cosmo->w0_de,
			      source->cosmo->w1_de, source->cosmo->h_100, source->cosmo->Omega_b,
			      source->cosmo->Omega_nu_mass, source->cosmo->Neff_nu_mass,
			      source->cosmo->normalization, source->cosmo->n_spec,
			      source->redshift->Nzbin, source->redshift->Nnz, source->redshift->nofz,
			      source->redshift->par_nz,
			      source->cosmo->nonlinear, source->cosmo->transfer,
			      source->cosmo->growth, source->cosmo->de_param, 
			      source->cosmo->normmode, source->tomo, source->reduced, source->q_mag_size, err);
   forwardError(*err, __LINE__, NULL);

   return res;
}

cosmo_lens* copy_parameters_lens(cosmo_lens* source, sm2_error **err)
{
   cosmo_lens *res;
   int Nzbin;

   res = copy_parameters_lens_only(source, err);             forwardError(*err, __LINE__, NULL);

   /* Reset cosmo and redshift */
   free_parameters(&res->cosmo);
   res->cosmo = copy_parameters(source->cosmo, err);         forwardError(*err, __LINE__, NULL);

   free_redshift(&res->redshift);
   res->redshift  = copy_redshift(source->redshift, err);    forwardError(*err, __LINE__, NULL);

   Nzbin = res->redshift->Nzbin;

   res->tomo       = source->tomo;
   res->reduced    = source->reduced;
   res->q_mag_size = source->q_mag_size;

   if (source->cosmo->nonlinear==halodm) {
      res->hm = copy_parameters_hm(source->hm, err);          forwardError(*err, __LINE__, NULL);
   } else {
      res->hm = NULL;
   }

   /* TODO: Nzbin^2 -> Nzcorr should also work */
   res->Pshear = copy_interTable_arr(source->Pshear, Nzbin*Nzbin, err);
   forwardError(*err, __LINE__, NULL);
   res->Pg1    = copy_interTable_arr(source->Pg1, Nzbin*Nzbin, err);
   forwardError(*err, __LINE__, NULL);
   res->xiP    = copy_interTable_arr(source->xiP, Nzbin*Nzbin, err);
   forwardError(*err, __LINE__, NULL);
   res->xiM    = copy_interTable_arr(source->xiM, Nzbin*Nzbin, err);
   forwardError(*err, __LINE__, NULL);
   res->gamma  = copy_interTable_arr(source->gamma, Nzbin*Nzbin, err);
   forwardError(*err, __LINE__, NULL);
   res->map_poly = copy_interTable_arr(source->map_poly, Nzbin*Nzbin, err);
   forwardError(*err, __LINE__, NULL);
   res->map_gauss = copy_interTable_arr(source->map_gauss, Nzbin*Nzbin, err);
   forwardError(*err, __LINE__, NULL);

   return res;
}

void read_cosmological_parameters_lens(cosmo_lens **self, FILE *F, error **err)
{
   cosmo_lens *tmp;
   struct { char cosmo_file[128], nofz_file[128], stomo[128], sreduced[128]; } tmp2;
   config_element c = {0, 0.0, ""};
   int j;
   FILE *FD;

   tmp = set_cosmological_parameters_to_default_lens(err);
   forwardError(*err, __LINE__,);

   CONFIG_READ_S(&tmp2, cosmo_file, s, F, c, err);
   if (strcmp(tmp2.cosmo_file, "-")!=0) {
      FD = fopen_err(tmp2.cosmo_file, "r", err);
      forwardError(*err, __LINE__,);
   } else {
      FD = F;
   }
   read_cosmological_parameters(&tmp->cosmo, FD, err);
   forwardError(*err, __LINE__,);
   if (strcmp(tmp2.cosmo_file, "-")!=0) fclose(FD);

   CONFIG_READ_S(&tmp2, nofz_file, s, F, c, err);
   if (strcmp(tmp2.nofz_file, "-")!=0) {
      FD = fopen_err(tmp2.nofz_file, "r", err);
      forwardError(*err, __LINE__,);
   } else {
      FD = F;
   }
   read_redshift_info(&(tmp->redshift), FD, err);
   forwardError(*err, __LINE__,);
   if (strcmp(tmp2.cosmo_file, "-")!=0) fclose(FD);


   /* Here: Lensing parameters */
   CONFIG_READ_S(&tmp2, stomo, s, F, c, err);
   STRING2ENUM(tmp->tomo, tmp2.stomo, tomo_t, stomo_t, j, Ntomo_t, err);

   CONFIG_READ_S(&tmp2, sreduced, s, F, c, err);
   STRING2ENUM(tmp->reduced, tmp2.sreduced, reduced_t, sreduced_t, j, Nreduced_t, err);
   if (tmp->reduced==reduced_K10) {
      CONFIG_READ(tmp, q_mag_size, d, F, c, err);
   }


   *self = copy_parameters_lens_only(tmp, err);
   forwardError(*err, __LINE__,);

   free_parameters_lens(&tmp);
}

void updateFrom_lens(cosmo_lens *avant, cosmo_lens *apres, error **err)
{
   int Nzbin;

   Nzbin = apres->redshift->Nzbin;

   if (change_g_source(avant,apres)) {
      del_interTable_arr(&(apres->g_source), Nzbin);
   }

   if (change_Pshear(avant,apres)) {
      del_interTable_arr(&(apres->Pshear), Nzbin*Nzbin);
      del_interTable_arr(&(apres->Pg1), Nzbin*Nzbin);
      del_interTable_arr(&(apres->xiP), Nzbin*Nzbin);
      del_interTable_arr(&(apres->xiM), Nzbin*Nzbin);
      del_interTable_arr(&(apres->gamma), Nzbin*Nzbin);
      del_interTable_arr(&(apres->map_poly), Nzbin*Nzbin);
      del_interTable_arr(&(apres->map_gauss), Nzbin*Nzbin);
   }

   updateFrom(avant->cosmo, apres->cosmo, err);
   forwardError(*err, __LINE__,);

   updateFrom_redshift(avant->redshift, apres->redshift);

   if (avant->cosmo->nonlinear==halodm) {
      updateFrom_hm(avant->hm, apres->hm, err);
      forwardError(*err, __LINE__,);
   }
}


/* Copies parameters from model->cosmo to model->hm->cosmo if   *
 * cosmo->nonlinear==halomodel					*/
void copy_parameters_lenshm_cosmo(cosmo_lens *model, error **err)
{
   cosmo *new;

   if (model->cosmo->nonlinear!=halodm) return;

   new = copy_parameters(model->cosmo, err);       forwardError(*err, __LINE__,);
   free_parameters(&model->hm->cosmo);
   model->hm->cosmo = new;
}

#define NZBIN 1
#define NNZ 5
cosmo_lens *set_cosmological_parameters_to_default_lens(error **err)
{
   int    Nnz[NZBIN]        = {NNZ};
   double par_nz[NZBIN*NNZ] = {0.0, 6.0, 0.612, 8.125, 0.62};
   nofz_t nofz[NZBIN]       = {ymmk};
   cosmo_lens *res;

   res = init_parameters_lens(0.25, 0.75, -1.0, 0.0, 0.70, 0.044, 0.0, 0.0, 0.80, 1.0,
			      NZBIN, Nnz, nofz, par_nz, smith03, eisenhu, growth_de, linder,
			      norm_s8, tomo_all, reduced_none, 0.0, err);
  forwardError(*err, __LINE__, NULL);

   return res;
}
#undef NNZ
#undef NZBIN

void free_parameters_lens(cosmo_lens** self)
{
   cosmo_lens *s;
   int Nzbin;

   s = *self;

   Nzbin = s->redshift->Nzbin;
   del_interTable_arr(&s->g_source, Nzbin);

   del_interTable_arr(&s->Pshear, Nzbin);
   del_interTable_arr(&s->Pg1, Nzbin);
   del_interTable_arr(&s->xiP, Nzbin);
   del_interTable_arr(&s->xiM, Nzbin);
   del_interTable_arr(&s->gamma, Nzbin);
   del_interTable_arr(&s->map_poly, Nzbin);
   del_interTable_arr(&s->map_gauss, Nzbin);

   if (s->hm!=NULL) {
      free_parameters_hm(&s->hm);
   }

   free_redshift(&s->redshift);
   free_parameters(&s->cosmo);

   free(s);
   s = NULL;
}

void dump_param_lens(cosmo_lens* self, FILE *F, int wnofz, error **err)
{
   dump_param(self->cosmo, F);
   if (wnofz) dump_redshift(self->redshift, F, err);
   forwardError(*err, __LINE__,);
   fprintf(F, "# (s)tomo = (%s)%d (s)reduced=(%s)%d q_mag_size=%g\n", stomo_t(self->tomo), self->tomo,
	   sreduced_t(self->reduced), self->reduced, self->q_mag_size);
   if (self->cosmo->nonlinear==halodm) dump_param_only_hm(self->hm, F);
}

double int_for_g(double aprime, void *intpar, error **err)
{
   cosmo_lensANDintANDdouble* cANDdANDe;
   cosmo_lens* self;
   double ww, wprime, res, fKwp, a;
   int n_bin;
   
   cANDdANDe = (cosmo_lensANDintANDdouble*)intpar;
   self      = cANDdANDe->self;
   a         = cANDdANDe->r;
   n_bin     = cANDdANDe->i;

   if (aprime>=a) return 0.0;

   ww     = w(self->cosmo, a, 0, err);                        forwardError(*err, __LINE__, 0);
   wprime = w(self->cosmo, aprime, 0, err);                   forwardError(*err, __LINE__, 0);
   res    = prob(self->redshift, 1.0/aprime-1.0, n_bin, err); forwardError(*err, __LINE__, 0);
   res   *= f_K(self->cosmo, wprime-ww, err)/dsqr(aprime);    forwardError(*err, __LINE__, 0);
   fKwp   = f_K(self->cosmo, wprime, err);                    forwardError(*err, __LINE__, 0);

   return res/fKwp;
}

int change_g_source(cosmo_lens* avant, cosmo_lens* apres)
{
   if (change_w(avant->cosmo, apres->cosmo) || NCOEQ(avant->cosmo, apres->cosmo, N_a))
     return 1;
   if (change_prob(avant->redshift, apres->redshift))
     return 1;
   return 0;
}

/* S98 2.9 */
double g_source(cosmo_lens* self, double a, int n_bin, error **err)
{
   double *table, res;
   double da = 0.0;
   double aa, wdelta, fKz0, wa;
   int    i, nn;
   cosmo_lensANDintANDdouble intpar;
	 
   if (self->redshift->nofz[n_bin]==single) {
      aa = 1.0/(1.0+get_zmin(self->redshift, n_bin));       /* zmin = zmax = z0 */
      if (aa>=a) return 0.0;
      wdelta = w(self->cosmo, aa, 0, err);           forwardError(*err,__LINE__,0);
      wa     = w(self->cosmo, a, 0, err);            forwardError(*err,__LINE__,0);
      res    = f_K(self->cosmo, wdelta-wa, err);     forwardError(*err,__LINE__,0);
      fKz0   = f_K(self->cosmo, wdelta, err);        forwardError(*err,__LINE__,0);

      return res/fKz0;
   }

   if (self->g_source==NULL) {
      da = (1.0-self->cosmo->a_min)/(self->cosmo->N_a-1.0);
      self->g_source = init_interTable_arr(self->redshift->Nzbin, self->cosmo->N_a,
      				  self->cosmo->a_min, 1.0, da, 0.0, 0.0, err);
      forwardError(*err,__LINE__,0);
      for (nn=0; nn<self->redshift->Nzbin; nn++) {
	 table       = self->g_source[nn]->table;
	 table[0]    = 0.0;
	 aa          = self->cosmo->a_min+da;
	 intpar.self = self;
	 intpar.i    = nn;

	 for (i=1;i<self->cosmo->N_a-1;i++,aa+=da) {
	    intpar.r  = aa;
	    // MKDEBUG: (1) was 1e-6.
	    table[i]  = sm2_qromberg(int_for_g, (void*)&intpar, self->cosmo->a_min, aa, 1.0e-5, err);
	    forwardError(*err, __LINE__, 0.0);
	 }

	 table[self->cosmo->N_a-1] = 1.0;
      }
   }

   res = interpol_wr(self->g_source[n_bin], a, err);
   forwardError(*err,__LINE__,0);

   return res;
}

/* lens efficiency */
double G(cosmo_lens* self, double a, int n_bin, error **err)
{
   double res;
   res  = 1.5/dsqr(R_HUBBLE)*(self->cosmo->Omega_m+self->cosmo->Omega_nu_mass)/a;
   res *= g_source(self, a, n_bin, err);
   forwardError(*err, __LINE__, 0);
   return res;	
}

/* ============================================================ *
 * Total (dark+baryon+neutrino) power spectrum			*
 * ============================================================ */
double P_NL_tot(cosmo_lens *self, double a, double k, error **err)
{
   double p_cb, p_nu, p_cb_nl, p_tot=0.0, f_nu, f_bc, Omega_m_tot;

   /*
   // **** MKDEBUG ****
   double K_MAX = 1.0;
   fprintf(stderr, "K_MAX = %g\n", K_MAX);
   if (k>K_MAX) return 0;
   */

   /* matter power spectrum */
   switch (self->cosmo->nonlinear) {
      case linear :
	 p_cb = P_L(self->cosmo, a, k, err);
	 forwardError(*err, __LINE__, -1.0);
	 break;

      case pd96 : case smith03 : case smith03_de :
	 p_cb = P_NL(self->cosmo, a, k, err);
	 forwardError(*err, __LINE__, -1.0);
	 break;

      case halodm :
	 p_cb = Pth_dm(self->hm, a, k, err);
	 forwardError(*err, __LINE__, -1.0);
	 break;

      default :
	 *err = addErrorVA(ce_unknown, "Unknown nonlinear flag %d", *err, __LINE__, self->cosmo->nonlinear);
	 return -1.0;
   }

   switch (self->cosmo->transfer) {
      case bbks: case eisenhu: case eisenhu_osc:
	 p_tot = p_cb;
	 break;

      case camb_vinschter :
	 /* Linear neutrino power spectrum */

	 Omega_m_tot = self->cosmo->Omega_m + self->cosmo->Omega_nu_mass;
	 testErrorRet(Omega_m_tot<0.0, ce_negative, "Omega_m + Omega_nu_mass < 0", *err, __LINE__, 0.0);

	 f_nu = self->cosmo->Omega_nu_mass/Omega_m_tot;
	 f_bc = self->cosmo->Omega_m/Omega_m_tot;
	 /* Hannestad et al. 2006 (3.4, 3.5) */
	 p_cb_nl = p_cb;
	 p_nu = P_nu(self->cosmo, a, k, err);                         forwardError(*err, __LINE__, 0);
	 p_tot = dsqr(f_nu*sqrt(p_nu) + f_bc*sqrt(p_cb_nl));
	 break;

      default :
	 *err = addError(ce_transfer, "wrong transfer type", *err, __LINE__);
	 return 0;
   }

   return p_tot;
}

double int_for_p_2(double a, void *intpar, error **err)
{
   double hoverh0, asqr, s, fKw, f, res, wa, gg;
   int i_bin, j_bin;
   cosmo_lensANDiid* cANDiid;
   cosmo_lens* self;


   cANDiid = (cosmo_lensANDiid*)intpar;
   self    = cANDiid->self;
   s       = cANDiid->r;
   i_bin   = cANDiid->i;
   j_bin   = cANDiid->j;
  
   testErrorRet(a>=1.0, ce_overflow, "Scale factor a>=1", *err, __LINE__, -1);
 
   asqr    = dsqr(a);
   wa      = w(self->cosmo, a, 0, err);          forwardError(*err, __LINE__, -1);
   fKw     = f_K(self->cosmo, wa, err);          forwardError(*err, __LINE__, -1);
   f       = s/fKw;
  
   hoverh0 = Esqr(self->cosmo, a, 0, err);       forwardError(*err, __LINE__,-1);
   hoverh0 = sqrt(hoverh0);

   gg   = g_source(self, a, i_bin, err);         forwardError(*err, __LINE__, -1);
   gg  *= g_source(self, a, j_bin, err);         forwardError(*err, __LINE__, -1);
   res  = gg/(asqr*asqr)/hoverh0*R_HUBBLE;
   res *= P_NL_tot(self, a, f, err);             forwardError(*err,__LINE__, -1);

   testErrorRet(!finite(res), ce_overflow, "Value not finite", *err, __LINE__, -1);

   return res;
}


int change_Pshear(cosmo_lens* avant, cosmo_lens* apres)
{
   if (change_w(avant->cosmo, apres->cosmo) || NCOEQ(avant->cosmo, apres->cosmo, N_a)) return 1;
   if (change_Tsqr(avant->cosmo, apres->cosmo)) return 1;
   if (change_prob(avant->redshift, apres->redshift)) return 1;
   if (change_P_NL(avant->cosmo, apres->cosmo)) return 1;
   if (NCOEQ(avant, apres, tomo)) return 1;
   if (NCOEQ(avant, apres, reduced)) return 1;

   return 0;
}

/* ============================================================ *
 * Returns the shear (cross-)power spectrum for Fourier         *
 * scale s redshift bins (i_bin,j_bin). Note that i_bin<=j_bin. *
 * See S98, eq. (3.4). Extrapolates outside of [s_min; s_max].	*
 * If self->reduced==K10, adds the first-order reduced-shear    *
 * correction according to K10. This correction is zero outside *
 * of [ELL_MIN_REDUCED=0.1; ELL_MAX_REDUCED=2e5].		*
 * ============================================================ */
double Pshear(cosmo_lens* self, double s, int i_bin, int j_bin, error **err)
{
   double *table;
   double ds, logsmin, logsmax, prefactor;
   double ss, slog, f1, f2;
   int    i, Nzbin, Nzcorr, ii, jj;
   double a, da;
   cosmo_lensANDiid intpar;

   // MKDEBUG!!!!!
   if (s<ELL_MIN_REDUCED || s>ELL_MAX_REDUCED) return 0.0;


   testErrorRetVA(s<=0, ce_negative, "Negative or zero 2d Fourier vector l=%g",* err, __LINE__, -1, s);
   testErrorRet(i_bin>j_bin, lensing_tomoij, "Pshear_ij defined for i<=j", *err, __LINE__, -1);

   Nzbin  = self->redshift->Nzbin;
   Nzcorr = Nzbin*(Nzbin+1)/2;
   if (self->Pshear==NULL) {

      logsmin   = log(s_min);
      logsmax   = log(s_max);
      ds        = (logsmax - logsmin)/(N_s - 1.0);
      da        = (1.0 - self->cosmo->a_min)/(self->cosmo->N_a-1.0);
      prefactor = 9.0/4.0*dsqr((self->cosmo->Omega_m+self->cosmo->Omega_nu_mass)/R_HUBBLE/R_HUBBLE);

      self->Pshear = init_interTable_arr(Nzcorr, N_s, logsmin, logsmax, ds, 1.0, -3.0, err);

      for (ii=0; ii<Nzbin; ii++) {
	 for (jj=ii; jj<Nzbin; jj++) {

	    if (self->tomo==tomo_auto_only && ii!=jj) continue;
	    if (self->tomo==tomo_cross_only && ii==jj) continue;

	    table = self->Pshear[idx_zz(ii,jj,Nzbin)]->table;
	    intpar.self = self;
	    intpar.i    = ii;
	    intpar.j    = jj;
	    for (i=0,slog=logsmin; i<N_s; i++,slog+=ds) {
	       ss = exp(slog);
	       intpar.r = ss;

#ifndef fastxi
	       *err = addError(lensing_fastxi, "The macro cosmo.h:fastxi is not defined", *err, __LINE__);
	       return -1.0;

	       /* Romberg-integration (slow) */
	       if (self->cosmo->a_min<0.7) {
		  f1 = sm2_qromberg(int_for_p_2, (void*)&intpar, self->cosmo->a_min, 0.7, 1.0e-6, err);
		  forwardError(*err, __LINE__, -1.0);
		  f2 = sm2_qrombergo(int_for_p_2, (void*)&intpar, 0.7, 1.0, sm2_midpntberg, 1.0e-7, err);
		  forwardError(*err, __LINE__, -1.0);
	       } else {
		  f1 = 0.0;
		  f2 = sm2_qrombergo(int_for_p_2, (void*)&intpar, self->cosmo->a_min, 1.0, sm2_midpntberg, 1.0e-7, err);
		  forwardError(*err, __LINE__, -1.0);
	       }
#else
	       /* Riemann summation (fast) */
	       for (a=self->cosmo->a_min,f1=0.0; a<1.0; a+=da) {
		  f1 += int_for_p_2(a, (void*)&intpar, err);
		  forwardError(*err, __LINE__, -1.0);
	       }
	       f1 = f1*da;
	       f2 = 0.0;
#endif

	       table[i] = log(prefactor*(f1+f2));
	       //MKDEBUG: Use the following line if K_MAX is in place
	       //table[i] = prefactor*(f1+f2);
	    }

	 }
      }

   }

   testErrorRetVA(self->tomo==tomo_auto_only && i_bin!=j_bin, lensing_tomoij,
		  "Cross-correlation (bins # %d,%d) not valid if tomo=tomo_auto_only",
		  *err, __LINE__, -1.0, i_bin, j_bin);
   testErrorRetVA(self->tomo==tomo_cross_only && i_bin==j_bin, lensing_tomoij,
		  "Cross-correlation (bin #%d) not valid if tomo=tomo_auto_only",
		  *err, __LINE__, -1.0, i_bin);

   slog = log(s);
   f1 = interpol_wr(self->Pshear[idx_zz(i_bin,j_bin,Nzbin)], slog, err);
   forwardError(*err, __LINE__, -1.0);

   f1 = exp(f1);


   /* Reduced-shear correction */
   if (self->reduced==reduced_K10) {
      f2 = Pg1(self, s, i_bin, j_bin, err);

      /* If error: return only P_kappa */
      forwardError(*err, __LINE__, f1);

      /* Add reduced-shear correction to P_kappa */
      f1 += f2;
   }

   return f1;
}

/* Used by Hankel transform functions, here and in halomodel. Returns Pshear^{ij}(l). *
 * If l outside of reduced-shear range, no error is forwarded.			      */
double P_projected_kappa(void *self, double l, int i_bin, int j_bin, error **err)
{
   double res;
   cosmo_lens *model;

   model = (cosmo_lens*)self;

   res = Pshear(model, l, i_bin, j_bin, err);

   /* If reduced-shear spectrum out of range: purge error. Return value is Pkappa. */
   if (model->reduced==reduced_K10 && getErrorValue(*err)==reduced_fourier_limit) {
      purgeError(err);
   }

   forwardError(*err, __LINE__, 0);

   return res;
}

/* ============================================================ *
 * Functions for reduced-shear correction (K10).		*
 * ============================================================ */
const int parameter[M_PAR] = {p_dummy, p_Omegam, p_Omegade, p_w0de, p_Omegab, p_h100, p_sigma8, p_ns};


/* Fiducial cosmology for reduced-shear */
cosmo_lens *set_cosmological_parameters_lens_to_WMAP7(const redshift_t *nofz, tomo_t tomo, error **err)
{
   /* Parameters are:
      Om Od w0 w1 h Ob Onu Neffnu s8 ns
      nonlin transfer growth deparam norm amin
   */

   cosmo_lens *self;
   
   self = init_parameters_lens(0.27, 0.73, -1.0, 0.0, 0.71, 0.045, 0.0, 0.0, 0.8, 0.96,
			       nofz->Nzbin, nofz->Nnz, nofz->nofz, nofz->par_nz,
			       smith03, eisenhu, growth_de, linder, norm_s8, tomo, reduced_none, 0.0, err);
   forwardError(*err, __LINE__, NULL);

   return self;
}

/* Returns a pointed to the parameter corresponding to type par. *
 * Note: don't dereference the result before forwardError, in    *
 * case of error a NULL pointer is returned.			 */
double *par_to_pointer(cosmo *self, par_t par, error **err)
{
   double *p;

   switch (par) {
      case p_Omegam :
	 p = &(self->Omega_m);
	 break;
      case p_sigma8 :
	 p = &(self->normalization);
	 break;
      case p_Omegade :
	 p = &(self->Omega_de);
	 break;
      case p_w0de :
	 p = &(self->w0_de);
	 break;
      case p_h100 :
	 p = &(self->h_100);
	 break;
      case p_ns :
	 p = &(self->n_spec);
	 break;
      case p_Omegab :
	 p = &(self->Omega_b);
	 break;
      default :
	 *err = addErrorVA(math_unknown, "Unknown par_t %d", *err, __LINE__, par);
	 return NULL;
   }

   return p;
}

#define EPS 1.0e-6
void fill_dpar(cosmo *model, cosmo *wmap7, double *dpar, error **err)
{
   int alpha;
   double *tmp;

   testErrorRetVA(fabs(wmap7->w1_de-model->w1_de)>EPS, reduced_par,
		  "Parameter w1de (= %g) different from fiducial WMAP7 value (= %g)",
		  *err, __LINE__,, model->w1_de, wmap7->w1_de);

   testErrorRetVA(fabs(wmap7->Omega_nu_mass-model->Omega_nu_mass)>EPS, reduced_par,
		  "Parameter w1de (= %g) different from fiducial WMAP7 value (= %g)",
		  *err, __LINE__,, model->Omega_nu_mass, wmap7->Omega_nu_mass);

   testErrorRetVA(fabs(wmap7->Neff_nu_mass-model->Neff_nu_mass)>EPS, reduced_par,
		  "Parameter w1de (= %g) different from fiducial WMAP7 value (= %g)",
		  *err, __LINE__,, model->Neff_nu_mass, wmap7->Neff_nu_mass);

   dpar[0] = 0.0; /* Unused */
   for (alpha=1; alpha<M_PAR; alpha++) {

      tmp         = par_to_pointer(model, parameter[alpha], err);
      forwardError(*err, __LINE__,);
      dpar[alpha] = *tmp;

      tmp         = par_to_pointer(wmap7, parameter[alpha], err);
      forwardError(*err, __LINE__,);
      dpar[alpha] = dpar[alpha] - *tmp;

   }
}
#undef EPS

double Fbar(cosmo_lens *self, double a, int m_bin, int n_bin, error **err)
{
   double dwda, gg_m, gg_n, ww, fK, res;
   cosmoANDint ci;

   ci.self = self->cosmo;
   ci.i    = 0;

   dwda = R_HUBBLE*int_for_w(a, (void*)(&ci), err); forwardError(*err, __LINE__, 0.0);
   gg_m = G(self, a, m_bin, err);                   forwardError(*err, __LINE__, 0.0);
   gg_n = G(self, a, n_bin, err);                   forwardError(*err, __LINE__, 0.0);
   ww   = w(self->cosmo, a, 0, err);                forwardError(*err, __LINE__, 0.0);
   fK   = f_K(self->cosmo, ww, err);                forwardError(*err, __LINE__, 0.0);

   res = dwda/fK*a*a*gg_m*gg_n*(gg_n + gg_m)/2.0;

   return res;

}

void fill_Fbar_array(cosmo_lens *self, double *fbar, int m_bin, int n_bin, double amin, int N_a,
		     double da, error **err)
{
   int k;
   double a;

   for (k=0,a=amin; k<N_a; k++,a+=da) {
      fbar[k]  = Fbar(self, a, m_bin, n_bin, err);
      forwardError(*err, __LINE__,);
   }
}

void fill_dFbar_dp_array(cosmo_lens *self, par_t par, double *dfbar_dp, int m_bin, int n_bin, double amin,
			 int N_a, double da, error **err)
{
   const int pm[2] = {+1, -1};
   double *param, orig, *fmn[2], h;
   cosmo_lens *self_mod;
   int j, k;

   self_mod = copy_parameters_lens_only(self, err);           forwardError(*err, __LINE__,);

   param  = par_to_pointer(self_mod->cosmo, par, err);        forwardError(*err, __LINE__,);
   orig   = *param;
   h      = FH*orig;

   for (j=0; j<2; j++) {
      *param = orig + pm[j]*h;
      updateFrom_lens(self, self_mod, err);                   forwardError(*err, __LINE__,);
      fmn[j] = malloc_err(N_a*sizeof(double), err);           forwardError(*err, __LINE__,);
      fill_Fbar_array(self_mod, fmn[j], m_bin, n_bin, amin, N_a, da, err);
      forwardError(*err, __LINE__,);
   }

   for (k=0; k<N_a; k++) {
      dfbar_dp[k] = (fmn[0][k] - fmn[1][k])/(2.0*h);
   }

   free_parameters_lens(&self_mod);
   free(fmn[0]);
   free(fmn[1]);
}

/* ============================================================ *
 * Reduced-shear power spectrum correction (K10).		*
 * If q_mag_size!=0, magnification and size bias is added to    *
 * the reduced-shear result, where q = 2*(alpha+beta-1), and    *
 * alpha (beta) are the number density slopes with flux (size). *
 * ============================================================ */
#define EPS 1.0e-6
double Pg1(cosmo_lens *self, double s, int i_bin, int j_bin, error **err)
{
   int Nzbin, Nzcorr, Na, alpha, i, ii, jj;
   double logsmin, logsmax, dlogs, da, *fmn, *dfmn_dp[M_PAR], dpar[M_PAR], res, slog, *table;
   cosmo_lens *wmap7;   /* Fiducial model */


   testErrorRetVA(s<ELL_MIN_REDUCED || s>ELL_MAX_REDUCED, reduced_fourier_limit,
		  "Fourier scale %g out of range [%g;%g], setting Pg^(1)=0",
		  *err, __LINE__, 0.0, s, ELL_MIN_REDUCED, ELL_MAX_REDUCED);

   testErrorRetVA(self->tomo==tomo_auto_only && i_bin!=j_bin, lensing_tomoij,
		  "Cross-correlation (bins # %d,%d) not valid if tomo=tomo_auto_only",
		  *err, __LINE__, -1.0, i_bin, j_bin);
   testErrorRetVA(self->tomo==tomo_cross_only && i_bin==j_bin, lensing_tomoij,
		  "Cross-correlation (bin #%d) not valid if tomo=tomo_auto_only",
		  *err, __LINE__, -1.0, i_bin);
   testErrorRetVA(s<=0, ce_negative, "Negative or zero 2d Fourier scale ell=%g",* err, __LINE__, -1, s);
   testErrorRet(i_bin>j_bin, lensing_tomoij, "Pg_ij defined for i<=j", *err, __LINE__, -1);


   Nzbin  = self->redshift->Nzbin;
   Nzcorr = Nzbin*(Nzbin+1)/2;
   Na     = self->cosmo->N_a;
   //Na     = 60;
   if (self->Pg1==NULL) {

      logsmin   = log(ELL_MIN_REDUCED);
      logsmax   = log(ELL_MAX_REDUCED);
      dlogs     = (logsmax - logsmin)/((double)NELL_REDUCED - 1.0);
      da        = (1.0 - self->cosmo->a_min)/((double)Na); /* Not Na-1 to avoid a=1! */

      self->Pg1 = init_interTable_arr(Nzcorr, NELL_REDUCED, logsmin, logsmax, dlogs, 0.0, 0.0, err);

      fmn       = malloc_err(Na*sizeof(double), err);  forwardError(*err, __LINE__, 0.0);
      for (alpha=1; alpha<M_PAR; alpha++) {
	 dfmn_dp[alpha] = calloc_err(Na, sizeof(double), err);  forwardError(*err, __LINE__, 0.0);
      }

      wmap7 = set_cosmological_parameters_lens_to_WMAP7(self->redshift, self->tomo, err);
      forwardError(*err, __LINE__, 0.0);
      fill_dpar(self->cosmo, wmap7->cosmo, (double *)dpar, err);
      forwardError(*err, __LINE__, 0.0);
      alpha = check_limits(dpar);
      testErrorRetVA(alpha!=0, reduced_limit, "Parameter dpar[%d]=%g out of range [%g;%g] for reduced-shear correction",
		     *err, __LINE__, 0.0, alpha, dpar[alpha], limits_lower[alpha], limits_upper[alpha]);

      for (ii=0; ii<Nzbin; ii++) {
	 for (jj=ii; jj<Nzbin; jj++) {

	    if (self->tomo==tomo_auto_only && ii!=jj) continue;
	    if (self->tomo==tomo_cross_only && ii==jj) continue;

	    table = self->Pg1[idx_zz(ii,jj,Nzbin)]->table;

	    /* F^{mn}, K10 eq. 10 */
	    fill_Fbar_array(wmap7, fmn, ii, jj, self->cosmo->a_min, Na, da, err);
	    forwardError(*err, __LINE__, 0.0);

	    /* Calculate dF^{mn}/dp for p!=p_0 (fiducial WMAP7 cosmology) */
	    for (alpha=1; alpha<M_PAR; alpha++) {
	       if (fabs(dpar[alpha])>EPS) {
		   fill_dFbar_dp_array(wmap7, parameter[alpha], dfmn_dp[alpha], ii, jj,
		  		      self->cosmo->a_min, Na, da, err);
		  forwardError(*err, __LINE__, 0.0);
	       }
	    }

	    for (i=0,slog=logsmin; i<NELL_REDUCED; i++,slog+=dlogs) {
	       res = sum_a_for_Pg1(slog, self->cosmo->a_min, Na, da, fmn, (const double **)dfmn_dp, dpar);
	       testErrorRetVA(!finite(res), math_negative, "Pg^1 (=%g) has to be positive",
			      *err, __LINE__, 0.0, res);
	       table[i] = res;
	    }

	 }
      }

      free_parameters_lens(&wmap7);
      free(fmn);
      for (alpha=1; alpha<M_PAR; alpha++) free(dfmn_dp[alpha]);
   }

   slog = log(s);
   res = interpol_wr(self->Pg1[idx_zz(i_bin,j_bin,Nzbin)], slog, err);
   forwardError(*err, __LINE__, -1.0);

   /* The factor 2 is already included in the fitting formulae */
   res = res + res/2.0*self->q_mag_size;

   return res;
}
#undef EPS


int change_xi(cosmo_lens* avant, cosmo_lens* apres)
{
   return change_Pshear(avant, apres);
}

/* ============================================================ *
 * Shear correlation function xi_+ (pm=+1) and xi_- (pm=-1).    *
 * ============================================================ */
double xi(cosmo_lens* self, int pm, double theta, int i_bin, int j_bin, error **err)
{
   double *table[2];
   double dlogtheta, logthetamin, logthetamax;
   double res;
   int Nzbin, Nzcorr, ii, jj, index;


   testErrorRetVA(theta<=0, ce_negative, "Negative angular scale theta=%g", *err, __LINE__, -1, theta);

   Nzbin  = self->redshift->Nzbin;
   Nzcorr = Nzbin*(Nzbin+1)/2;
   if (self->xiP==NULL || self->xiM==NULL) {
      self->xiP = init_interTable_arr(Nzcorr, N_thetaH, 0, 1, 1, 0.0, 0.0, err);
      forwardError(*err,__LINE__,0);
      self->xiM = init_interTable_arr(Nzcorr, N_thetaH, 0, 1, 1, 0.0, 0.0, err);
      forwardError(*err,__LINE__,0);

      for (ii=0; ii<Nzbin; ii++) {
	 for (jj=ii; jj<Nzbin; jj++) {

	    if (self->tomo==tomo_auto_only && ii!=jj) continue;
	    if (self->tomo==tomo_cross_only && ii==jj) continue;

	    index = idx_zz(ii,jj,Nzbin);
	    table[0] = self->xiP[index]->table;
	    table[1] = self->xiM[index]->table;
	    tpstat_via_hankel(self, table, &logthetamin, &logthetamax, tp_xipm, P_projected_kappa,
			      ii, jj, err);
	    forwardError(*err,__LINE__,0);

	    dlogtheta = (logthetamax-logthetamin)/((double)N_thetaH-1.0);
	    self->xiP[index]->a  = logthetamin;
	    self->xiM[index]->a  = logthetamin;
	    self->xiP[index]->b  = logthetamax;
	    self->xiM[index]->b  = logthetamax;
	    self->xiP[index]->dx = dlogtheta;
	    self->xiM[index]->dx = dlogtheta;

	 }
      }
   }

   testErrorRetVA(self->tomo==tomo_auto_only && i_bin!=j_bin, lensing_tomoij,
		  "Cross-correlation (bins # %d,%d) not valid if tomo=tomo_auto_only",
		  *err, __LINE__, -1.0, i_bin, j_bin);
   testErrorRetVA(self->tomo==tomo_cross_only && i_bin==j_bin, lensing_tomoij,
		  "Cross-correlation (bin #%d) not valid if tomo=tomo_auto_only",
		  *err, __LINE__, -1.0, i_bin);

   if (pm==1) {
      res = interpol_wr(self->xiP[idx_zz(i_bin,j_bin,Nzbin)], log(theta), err);
      forwardError(*err,__LINE__,0);
   } else if (pm==-1) {
      res = interpol_wr(self->xiM[idx_zz(i_bin,j_bin,Nzbin)], log(theta), err);
      forwardError(*err,__LINE__,0);
   } else {
      *err = addErrorVA(lensing_pm, "pm=%d not valid, has to be +1 or -1", *err, __LINE__, pm);
      res = -1.0;
      return res;
   }

   testErrorRetVA(self->reduced==reduced_K10 && pm==+1 && (theta<THETA_P_MIN_REDUCED || theta>THETA_MAX_REDUCED),
		  reduced_realsp_limit,
		  "Angular scale theta=%g' out of range [%g';%g'] for xi+ using reduced-shear correction",
		  *err, __LINE__, -1.0, theta/arcmin, THETA_P_MIN_REDUCED/arcmin, THETA_MAX_REDUCED/arcmin);
   testErrorRetVA(self->reduced==reduced_K10 && pm==-1 && (theta<THETA_M_MIN_REDUCED || theta>THETA_MAX_REDUCED),
		  reduced_realsp_limit,
		  "Angular scale theta=%g' out of range [%g';%g'] for xi- using reduced-shear correction",
		  *err, __LINE__, -1.0, theta/arcmin, THETA_M_MIN_REDUCED/arcmin, THETA_MAX_REDUCED/arcmin);

   return res;
}

int change_gamma2(cosmo_lens* avant, cosmo_lens* apres)
{
   return change_Pshear(avant,apres);
}

/* ============================================================ *
 * Tophat shear variance <|gamma|!2>.				*
 * ============================================================ */
double gamma2(cosmo_lens* self, double theta, int i_bin, int j_bin, error **err)
{
   double *table;
   double dlogtheta, logthetamin, logthetamax;
   double res;
   int Nzbin, Nzcorr, ii, jj, index;
   
   testErrorVA(theta<=0, ce_negative,"Negative scale theta=%g", *err, __LINE__, -1.0, theta);

   Nzbin  = self->redshift->Nzbin;
   Nzcorr = Nzbin*(Nzbin+1)/2;
   if (self->gamma==NULL) {

      self->gamma = init_interTable_arr(Nzcorr, N_thetaH, 0, 1, 1, 0.0, 0.0,err);
      forwardError(*err,__LINE__,0);

      for (ii=0; ii<Nzbin; ii++) {
	 for (jj=ii; jj<Nzbin; jj++) {

	    if (self->tomo==tomo_auto_only && ii!=jj) continue;
	    if (self->tomo==tomo_cross_only && ii==jj) continue;

	    index   = idx_zz(ii,jj,Nzbin);
	    table   = self->gamma[index]->table;
	    tpstat_via_hankel(self, &table, &logthetamin, &logthetamax, tp_gsqr, P_projected_kappa,
			      ii, jj, err);
	    forwardError(*err,__LINE__,0);
	    dlogtheta = (logthetamax-logthetamin)/((double)N_thetaH-1.0);
	    self->gamma[index]->a  = logthetamin;
	    self->gamma[index]->b  = logthetamax;
	    self->gamma[index]->dx = dlogtheta;

	 }
      }
   }

   testErrorRetVA(self->tomo==tomo_auto_only && i_bin!=j_bin, lensing_tomoij,
		  "Cross-correlation (bins # %d,%d) not valid if tomo=tomo_auto_only",
		  *err, __LINE__, -1.0, i_bin, j_bin);
   testErrorRetVA(self->tomo==tomo_cross_only && i_bin==j_bin, lensing_tomoij,
		  "Cross-correlation (bin #%d) not valid if tomo=tomo_auto_only",
		  *err, __LINE__, -1.0, i_bin);

   res = interpol_wr(self->gamma[idx_zz(i_bin,j_bin,Nzbin)], log(theta), err);
   forwardError(*err,__LINE__,0);
   return res;

}

int change_map2(cosmo_lens* avant, cosmo_lens* apres)
{
   return change_Pshear(avant,apres);
}

/* ============================================================ *
 * Aperture mass variance <M_ap^2> with polynomial filter.	*
 * ============================================================ */
double map2(cosmo_lens* self, double theta, int i_bin, int j_bin, error **err)
{
  double res;
  res = map2_poly(self, theta, i_bin, j_bin, err);
  forwardError(*err, __LINE__, 0);
  return res;
}


double map2_poly(cosmo_lens* self, double theta,  int i_bin, int j_bin, error **err)
{
   double *table;
   double dlogtheta, logthetamin, logthetamax;
   double res;
   int Nzbin, Nzcorr, ii, jj, index;


   testErrorRetVA(theta<=0, ce_negative, "Negative angular scale theta=%g", *err, __LINE__, -1, theta);

   Nzbin = self->redshift->Nzbin;
   Nzcorr = Nzbin*(Nzbin+1)/2;
   if (self->map_poly == NULL) {
      self->map_poly = init_interTable_arr(Nzcorr, N_thetaH, 0, 1, 1, 0.0, 0.0, err);
      forwardError(*err, __LINE__, 0);
      for (ii=0; ii<Nzbin; ii++) {
	 for (jj=ii; jj<Nzbin; jj++) {

	    if (self->tomo==tomo_auto_only && ii!=jj) continue;
	    if (self->tomo==tomo_cross_only && ii==jj) continue;

	    index   = idx_zz(ii,jj,Nzbin);
	    table   = self->map_poly[index]->table;
	    tpstat_via_hankel(self, &table, &logthetamin, &logthetamax, tp_map2_poly, P_projected_kappa,
			      ii, jj, err);
	    forwardError(*err, __LINE__, 0);
	    dlogtheta = (logthetamax-logthetamin)/((double)N_thetaH-1.0);
	    self->map_poly[index]->a = logthetamin;
	    self->map_poly[index]->b = logthetamax;
	    self->map_poly[index]->dx = dlogtheta;

	 }
      }
   }

   testErrorRetVA(self->tomo==tomo_auto_only && i_bin!=j_bin, lensing_tomoij,
		  "Cross-correlation (bins # %d,%d) not valid if tomo=tomo_auto_only",
		  *err, __LINE__, -1.0, i_bin, j_bin);
   testErrorRetVA(self->tomo==tomo_cross_only && i_bin==j_bin, lensing_tomoij,
		  "Cross-correlation (bin #%d) not valid if tomo=tomo_auto_only",
		  *err, __LINE__, -1.0, i_bin);

   res = interpol_wr(self->map_poly[idx_zz(i_bin,j_bin,Nzbin)], log(theta), err);
   forwardError(*err, __LINE__, 0);

   return res;
}

double map2_gauss(cosmo_lens* self, double theta, int i_bin, int j_bin, error **err)
{
   double *table;
   double dlogtheta, logthetamin, logthetamax;
   double res;
   int Nzbin, Nzcorr, ii, jj, index;

   testErrorRetVA(theta<=0, ce_negative, "Negative angular scale theta=%g", *err, __LINE__, -1, theta);

   Nzbin = self->redshift->Nzbin;
   Nzcorr = Nzbin*(Nzbin+1)/2;
   if (self->map_gauss==NULL) {
      self->map_gauss = init_interTable_arr(Nzcorr, N_thetaH, 0, 1, 1, 0.0, 0.0, err);
      forwardError(*err, __LINE__, 0);
      for (ii=0; ii<Nzbin; ii++) {
	 for (jj=ii; jj<Nzbin; jj++) {

	    if (self->tomo==tomo_auto_only && ii!=jj) continue;
	    if (self->tomo==tomo_cross_only && ii==jj) continue;

	    index   = idx_zz(ii,jj,Nzbin);
	    table   = self->map_gauss[index]->table;
	    tpstat_via_hankel(self, &table, &logthetamin, &logthetamax, tp_map2_gauss, P_projected_kappa,
			      ii, jj, err);
	    forwardError(*err, __LINE__, 0);
	    dlogtheta = (logthetamax-logthetamin)/((double)N_thetaH-1.0);
	    self->map_gauss[index]->a = logthetamin;
	    self->map_gauss[index]->b = logthetamax;
	    self->map_gauss[index]->dx = dlogtheta;
	 }
      }
   }

   testErrorRetVA(self->tomo==tomo_auto_only && i_bin!=j_bin, lensing_tomoij,
		  "Cross-correlation (bins # %d,%d) not valid if tomo=tomo_auto_only",
		  *err, __LINE__, -1.0, i_bin, j_bin);
   testErrorRetVA(self->tomo==tomo_cross_only && i_bin==j_bin, lensing_tomoij,
		  "Cross-correlation (bin #%d) not valid if tomo=tomo_auto_only",
		  *err, __LINE__, -1.0, i_bin);

   res = interpol_wr(self->map_gauss[idx_zz(i_bin,j_bin,Nzbin)], log(theta), err);
   forwardError(*err, __LINE__, 0);

   return res;
}

/* Returns the generalised ring statistic using a as the coefficients *
 * of the T_+ decomposition, see FK09 (4, 8, 11)		      *
 * If a==NULL, Z+ from SK07 is used.				      */
double RR(cosmo_lens *lens, double THETA_MIN, double THETA_MAX, const double *a, int N,
	  poly_t poly, int pm, error **err)
{
   double theta, dtheta, res[2]={0,0}, A, B, x, summand;

   testErrorRetVA(abs(pm)!=1, mr_incompatible, "pm=%d not valid, has to be +1 or -1",
		  *err, __LINE__, 0.0, pm);

   if (a==NULL) {
#ifdef __MRING_H
      /* Using the (non-optimised) Schneider&Kilbinger (2007) functions Z+,Z- */
      R_from_xi(lens, THETA_MIN/THETA_MAX, THETA_MAX, res, res+1, err);
      forwardError(*err, __LINE__, 0.0);
      return res[0];
#else
      *err = addError(mr_null, "Coefficients a=NULL", *err, __LINE__);
      return 0.0;
#endif
   }


   /* Decrease step size dtheta for more precision,     *
    * in particular, to make (numerical) B-mode smaller	*/
   dtheta  = 1.0*arcsec/10.0;
   if (THETA_MAX>30*arcmin) dtheta *= (THETA_MAX/(30*arcmin));


   /* FK09 (8) */
   A = (THETA_MAX - THETA_MIN)/2.0;
   B = (THETA_MAX + THETA_MIN)/2.0;

   for (theta=THETA_MIN,res[0]=0.0; theta<=THETA_MAX; theta+=dtheta) {

      x        = (theta-B)/A;

      summand  = xi(lens, pm, theta, 0, 0, err)*theta/dsqr(THETA_MAX);
      forwardError(*err, __LINE__, -1.0);

      if (pm==+1)      summand *= Tp(x, a, N, poly, err);
      else if (pm==-1) summand *= Tm(x, a, N, poly, B/A, err);
      forwardError(*err, __LINE__, -1.0);

      res[0] += summand;

   }

   testErrorRet(!finite(res[0]), math_infnan, "R_E is not finite", *err, __LINE__, -1.0);

   return res[0]*dtheta;
}

double int_for_map2_slow(double logell, void *intpar, error **err)
{
   cosmo_lensANDiiid *cplusplus;
   double theta, etasqr, res, ell;
   tpstat_t tpstat;
   int i_bin, j_bin;

   ell       = exp(logell);
   cplusplus = (cosmo_lensANDiiid*)intpar;
   theta     = cplusplus->r;
   etasqr    = dsqr(theta*ell);
   tpstat    = (tpstat_t)cplusplus->t;
   i_bin     = cplusplus->i;
   j_bin     = cplusplus->j;

   res = dsqr(ell)*Pshear(cplusplus->self, ell, i_bin, j_bin, err);
   forwardError(*err, __LINE__, 0);
   if (tpstat==tp_map2_poly) {
      /* TODO: implement Bessel function J_4 */
      *err = addError(ce_unknown, "wrong tpstat", *err, __LINE__);
      return -1;
   } else if (tpstat==tp_map2_gauss) {
      res *= dsqr(etasqr)/4.0*exp(-etasqr);
   } else {
      *err = addError(ce_unknown, "wrong tpstat", *err, __LINE__);
      return -1;
   }

   return res;
}

double map2_slow(cosmo_lens *self, double theta, tpstat_t tpstat, int i_bin, int j_bin, error **err)
{
   cosmo_lensANDiiid intpar;
   double res;

   intpar.self = self;
   intpar.t    = (int)tpstat;
   intpar.r    = theta;
   intpar.i    = i_bin;
   intpar.j    = j_bin;

   res = 1.0/(2.0*pi)*sm2_qromberg(int_for_map2_slow, (void*)&intpar, log(s_min), log(s_max),
				   1.0e-6, err);
   forwardError(*err, __LINE__, 0);

   return res;
}

/* ============================================================ *
 * Data input, likelihood stuff (used to be in likeli.c).	*
 * ============================================================ */

/* ============================================================= *
 * Only needed for n(z) data, for lensing use init_data_cov_tomo *
 * instead, also for one redshift bin.				 *
 * ============================================================= */

datcov *init_data_cov(char* dataname, char* covname, lensdata_t type, int isnofz, error **err)
{
   datcov *res;
   double det;
   char *data_name, *cov_name;
   char odata_name[] = "map2";
   char ocov_name[]  = "CovMap51_0.93+B.block";
   int i;
	
   if (strcmp(dataname,"")==0) {
      data_name=odata_name;
   } else {
      data_name=dataname;
   }
   if (strcmp(covname,"")==0 && isnofz!=1) {
      cov_name=ocov_name;
   } else {
      cov_name=covname;
   }

   res = malloc_err(sizeof(datcov), err);        forwardError(*err,__LINE__,NULL);
   read_data(res, data_name, err, isnofz);       forwardError(*err,__LINE__,NULL);

   if (isnofz==0 || isnofz==2) {
      read_cov(res, cov_name, err);              forwardError(*err,__LINE__,NULL);
      det = sm2_inverse(res->cov, res->n, err);  forwardError(*err,__LINE__,NULL);
      res->usecov = 1;
   } else if (isnofz==1) {
      for (i=0,det=1.0; i<res->n; i++) {
	 det *= res->var[i];
      }
      res->usecov = 0;
   } else {
      *err = addError(ce_unknown, "Unknown flag isnofz (has to be 0, 1 or 2)", *err, __LINE__);
   }

   res->type = type;

   testErrorRet(type<0||type>=Nlensdata_t, ce_unknown, "Unknown lens data type", *err, __LINE__, NULL);
   testErrorRet((isnofz==0 || isnofz==2) && type==nofz, lensing_inconsistent,
   		"Inconsistent flags type and isnofz",
   		*err, __LINE__, NULL);
   testErrorRet(isnofz==1 && type!=nofz, lensing_inconsistent, "Inconsistent flags type and isnofz",
   		*err, __LINE__, NULL);

   return res;
}

/* ============================================================ *
 * Useful if a datcov structure only contains the covariance.   *
 * ============================================================ */
datcov *init_datcov_for_cov_only(int Nzbin, int Ntheta, error **err)
{
   datcov *dc;

   dc = malloc_err(sizeof(datcov), err);         forwardError(*err, __LINE__, NULL);
   dc->Nzbin  = Nzbin;
   dc->Nzcorr = Nzbin*(Nzbin+1)/2;
   dc->Ntheta = Ntheta;
   dc->n = dc->Nzcorr*dc->Ntheta;
   dc->usecov = 1;
   dc->Nexclude = 0;
   dc->theta = dc->data = dc->var = dc->cov = NULL;

   return dc;
}

datcov *init_data_cov_tomo(char* dataname, char* covname, lensdata_t type, decomp_eb_filter_t decomp_eb_filter, 
			   lensformat_t format, double corr_invcov, 
			   double a1, double a2, int Nexclude, const int *exclude, error **err)
{
   datcov *res;
   int Nzbin, i;
   gsl_matrix_view A;

   testErrorRetVA(type<0||type>=Nlensdata_t, ce_unknown, "Unknown lens data type %d",
		  *err, __LINE__, NULL, type);

   res = malloc_err(sizeof(datcov), err);           forwardError(*err,__LINE__,NULL);

   res->format = format;
   res->usecov = 1;
   res->type   = type;
   res->decomp_eb_filter = decomp_eb_filter;

   /* These are zero if format!=angle_wquadr */
   res->a1 = a1;
   res->a2 = a2;
   res->Nexclude = Nexclude;
   if (exclude==NULL) {
      res->exclude = NULL;
   } else {
      res->exclude = malloc_err(sizeof(int)*Nexclude, err);  forwardError(*err, __LINE__, NULL);
      for (i=0; i<Nexclude; i++)  res->exclude[i] = exclude[i];
   }

   Nzbin = 0; /* Will be set according to data file */

   read_data_tomo(res, dataname, Nzbin, err);                forwardError(*err,__LINE__,NULL);

   testErrorRetVA(res->Nzcorr<Nexclude, lensing_inconsistent, "Nexclude=%d larger than Nzcorr=%d",
		  *err, __LINE__, NULL, Nexclude, res->Nzcorr);

   read_cov_tomo(res, covname, err);                         forwardError(*err,__LINE__,NULL);

   A = gsl_matrix_view_array(res->cov, res->n, res->n);
   /* Replace res->cov with L, where C = L L^T */
   if (gsl_linalg_cholesky_decomp(&A.matrix) == GSL_EDOM) {
      del_data_cov(&res);
      *err = addError(mv_cholesky,"Cholesky decomposition failed",*err,__LINE__);
      return NULL;
   }

   multiply_all(res->cov, res->n*res->n, sqrt(1.0/corr_invcov));

   //sm2_inverse(res->cov, res->n, err);                 forwardError(*err,__LINE__,NULL);
   //multiply_all(res->cov, res->n*res->n, corr_invcov);

   return res;
}

void del_data_cov(datcov** dc)
{
   datcov *d;
   d = *dc;

   if (d->theta) free(d->theta);
   if (d->data) free(d->data);
   if (d->usecov) free(d->cov);
   else free(d->var);
   free(d);
   d = NULL;
}

void read_data(datcov *dc, char data_name[], error **err, int isnofz)
{
   FILE *F;
   int n, res, i;
   double d[6];

   F = fopen(data_name, "r");
   if (F==NULL) {
      *err = addErrorVA(ce_file, "Cannot open data file %s. Make sure it exists and is readable",
			*err, __LINE__, data_name);
      return;
   }

   /* Count line number */
   n = 0;
   do {
		
      res = fscanf(F, "%lf %lf %lf %lf %lf %lf\n", d, d+1, d+2, d+3, d+4, d+5);
      testErrorRet(res<2 && res!=EOF, ce_badFormat, "Number of columns <2", *err, __LINE__,);
      n++;
		
   } while (res!=EOF);
   n--;

   dc->n      = n;
   dc->Ntheta = n;
   dc->data  = malloc_err(sizeof(double)*n, err);               forwardError(*err, __LINE__,);
   dc->theta = malloc_err(sizeof(double)*n, err);               forwardError(*err, __LINE__,);
   if (isnofz==1) dc->var = malloc_err(sizeof(double)*n, err);  forwardError(*err, __LINE__,);

   /* Read file */
   rewind(F);
   for (i=0; i<n; i++) {
      fscanf(F, "%lf %lf %lf %lf %lf %lf\n", dc->theta+i, d+1, d+2, d+3, d+4, d+5);
      if (isnofz==0 || isnofz==1) dc->data[i] = d[1];
      else if (isnofz==2) dc->data[i] = d[3];
      if (isnofz==1) dc->var[i] = d[2];
   }
	 
   if (!isnofz) {
      /* [theta] from arcmin to rad */
      for (i=0; i<dc->n; i++) {
	 dc->theta[i] *= arcmin;
      }
   }

   dc->Nzbin = 1;

}

/* ============================================================ *
 * Reads data file into structure dc (has to be initialised     *
 * before). The file has to have the structure                  *
 *   theta[arcmin] c_00 c_01 ... c_0{n-1} c_11 ... c_{n-1}{n-1} *
 * with c_ij some (correlation) quantity corr. to bins i, j.    *
 * If Nzbin>0 on input performs a consistency check.		*
 * The file can contain xi+ and xi- data, in this case it       *
 * corresponds to 'cat xi+.dat xi-.dat > xi+-.dat'		*
 * ============================================================ */
void read_data_tomo(datcov *dc, char data_name[], int Nzbin, error **err)
{
   int n, i, i_bin, j_bin, Ncol;
   size_t Nrec, Ntheta, Nzcorr;
   double dNcol, dNzbin;
   double *ptr;

   /* Read file to double pointer */
   Nrec = 0;
   ptr = (double*)read_any_list_count(data_name, &Nrec, "%lg", sizeof(double), &Ntheta, err);
   forwardError(*err, __LINE__,);

   /* Number of lines and columns */
   dNcol = (double)Nrec/(double)Ntheta;
   Ncol = Nrec/Ntheta;
   testErrorRet(fabs(dNcol-(double)Ncol)>EPSILON, lensing_inconsistent, "Columns do not have equal length",
		*err, __LINE__,);

   if (dc->format==angle_mean || dc->format==angle_wlinear || dc->format==angle_wquadr) Nzcorr = Ncol - 2;
   else Nzcorr = Ncol - 1;

   dNzbin = 0.5*(sqrt(8.0*Nzcorr+1.0) - 1.0);
   testErrorRet(fabs(dNzbin-floor(dNzbin+0.5))>EPSILON, lensing_inconsistent,
		"Number of z-correlations inconsistent", *err, __LINE__,);
   n = (int)floor(dNzbin+0.5);

   if (Nzbin>0) {
      testErrorRetVA(Nzbin!=n, lensing_inconsistent,
		     "Number of z bins in file (%d) inconsistent with input value (%d)",
		     *err, __LINE__,, n, Nzbin);
   }
   Nzbin = n;


   /* Fill datcov structure */
   dc->Ntheta = (int)Ntheta;
   dc->Nzbin  = Nzbin;
   dc->Nzcorr = Nzcorr;
   testErrorRet(dc->Nzcorr!=dc->Nzbin*(dc->Nzbin+1)/2, lensing_inconsistent, "Nzcorr!=Nzbin*(Nzbin+1)/2",
		*err, __LINE__,);
   dc->n      = Ntheta*Nzcorr;

   dc->data  = malloc_err(sizeof(double)*dc->n, err);          forwardError(*err, __LINE__,);
   dc->theta = malloc_err(sizeof(double)*Ntheta, err);         forwardError(*err, __LINE__,);
   if (dc->format==angle_mean || dc->format==angle_wlinear || dc->format==angle_wquadr) {
      dc->theta2 = malloc_err(sizeof(double)*Ntheta, err);     forwardError(*err, __LINE__,);
   } else {
      dc->theta2 = NULL;
   }
   dc->var   = NULL;

   /* First column: angular scale */
   for (i=0; i<Ntheta; i++) {
      dc->theta[i] = ptr[i*Ncol+0]*arcmin;
      if (dc->format==angle_mean || dc->format==angle_wlinear || dc->format==angle_wquadr) {
	 /* Second column: upper bin limit */
	 dc->theta2[i] = ptr[i*Ncol+1]*arcmin;
      }
   }

   for (i_bin=0,n=0; i_bin<Nzbin; i_bin++) {
      for (j_bin=i_bin; j_bin<Nzbin; j_bin++) {
	 for (i=0; i<Ntheta; i++,n++) {
	    testErrorRetVA(n>=dc->n, math_overflow, "Index overflow (%d>=%d)",
			   *err, __LINE__,, n, dc->n);

	    if (dc->format==angle_mean || dc->format==angle_wlinear || dc->format==angle_wquadr) {
	       dc->data[n] = ptr[i*Ncol+2+idx_zz(i_bin,j_bin,Nzbin)];
	    } else {
	       dc->data[n] = ptr[i*Ncol+1+idx_zz(i_bin,j_bin,Nzbin)];
	    }
	 }
      }
   }

}

#define FACTOR_EXCLUDE (100.0*100.0)
void read_cov_tomo(datcov* dc, char cov_name[], error **err)
{
   int i, j, i_bin, j_bin, k_bin, l_bin, index_file, nz, mz, count;
   // int in;
   size_t Nrec, Nrow;
   double *ptr;


   testErrorRet(dc->Ntheta<=0, lensing_initialised,
		"Data file has to be read before covariance, to set number of bins (Ntheta)", *err, __LINE__,);
   testErrorRet(dc->Nzcorr<=0, lensing_initialised,
		"Data file has to be read before covariance, to set number of bins (Nzcorr)", *err, __LINE__,);
   testErrorRet(dc->Nzbin<=0, lensing_initialised,
		"Data file has to be read before covariance, to set number of bins (dc->Nzbin)", *err, __LINE__,);

   /* Read file to double pointer */
   Nrec = 0;
   ptr  = (double*)read_any_list_count(cov_name, &Nrec, "%lg", sizeof(double), &Nrow, err);
   forwardError(*err, __LINE__,);

   testErrorVA(Nrow!=dc->n, lensing_inconsistent,
	       "Covariance matrix (%dx%d) inconsistent with data vector of length %d",
	       *err, __LINE__, Nrow, dc->n);
   testErrorRetVA(Nrec!=Nrow*Nrow, lensing_inconsistent, "Covariance matrix is not square, Nrow=%d, Nrec=%d\n",
		  *err, __LINE__,, Nrec, Nrow);

   dc->cov = malloc_err(dc->n*dc->n*sizeof(double), err);   forwardError(*err, __LINE__,);

   for (i_bin=nz=0,index_file=0; i_bin<dc->Nzbin; i_bin++) {
      for (j_bin=i_bin; j_bin<dc->Nzbin; j_bin++, nz++) {
	 for (i=0; i<dc->Ntheta; i++) {

	    for (k_bin=mz=0; k_bin<dc->Nzbin; k_bin++) {
	       for (l_bin=k_bin; l_bin<dc->Nzbin; l_bin++,mz++) {
		  for (j=0;j<dc->Ntheta;j++,index_file++) {

		     dc->cov[index_file] = ptr[index_file];

		     /* Exclude z-bins from analysis */
		     for (count=0; count<dc->Nexclude; count++) {
			if (nz!=mz) {
			   /* Off-diagonal */
			   if (dc->exclude[count]==nz || dc->exclude[count]==mz) {
			      dc->cov[index_file] = 0.0;
			   }
			} else {
			   /* Diagonal */
			   if (dc->exclude[count]==nz) {
			      dc->cov[index_file] *= FACTOR_EXCLUDE;
			   }
			}
		     }

		  }
	       }
	    }
	 }
      }
   }

}
#undef FACTOR_EXCLUDE

/* ============================================================ *
 * Creates the vectors xip, xim, theta and theta2 (if required  *
 * by the lens format), all of size N,				*
 * and copies the content of datcov to those vectors.		*
 * ============================================================ */
void datcov2xipm(const datcov *dc, int i_bin, int j_bin, double **xip, double **xim, double **theta,
		 double **theta2, int *N, error **err)
{
   int i, index;

   testErrorRetVA(dc->type!=xipm, lensing_type, "lenstype has to be %d('%s'), not %d",
   		  *err, __LINE__,, xipm, slensdata_t(xipm), dc->type);

   testErrorRetVA(i_bin>j_bin, lensing_tomoij, "i_bin(=%d) has to be smaller or equal j_bin(=%d)",
		  *err, __LINE__,, i_bin, j_bin);

   testErrorRetVA(j_bin>=dc->Nzbin, lensing_tomoij, "j_bin(=%d) has to be smaller than Nzbin=%d\n",
   		  *err, __LINE__,, j_bin, dc->Nzbin);

   *N = dc->Ntheta/2;

   *xip   = malloc_err(*N*sizeof(double), err);  forwardError(*err, __LINE__,);
   *xim   = malloc_err(*N*sizeof(double), err);  forwardError(*err, __LINE__,);
   *theta = malloc_err(*N*sizeof(double), err);  forwardError(*err, __LINE__,);

   if (dc->format==angle_mean || dc->format==angle_wlinear || dc->format==angle_wquadr) {
      *theta2 = malloc_err(*N*sizeof(double), err);  forwardError(*err, __LINE__,);
   } else {
      *theta2 = NULL;
   }


   for (i=0; i<*N; i++) {
      index = idx_tzz(i, i_bin, j_bin, dc->Nzbin);
       (*xip)[i]   = dc->data[index];
       (*theta)[i] = dc->theta[i];
      if (dc->format==angle_mean || dc->format==angle_wlinear || dc->format==angle_wquadr) {
	 (*theta2)[i] = dc->theta2[i];
      }
   }
   
   for (i=*N; i<dc->Ntheta; i++) {
      index = idx_tzz(i, i_bin, j_bin, dc->Nzbin);
      (*xim)[i-(*N)] = dc->data[index];
   }
   
}

void read_cov(datcov* dc, char cov_name[], error **err)
{
   FILE *F;
   int n, i;
	
   F       = fopen_err(cov_name, "r", err);                 forwardError(*err, __LINE__,);
   dc->cov = malloc_err(sizeof(double)*dc->n*dc->n, err);   forwardError(*err, __LINE__,);
	
   for (i=0; i<dc->n*dc->n; i++) {
      n=fscanf(F,"%lf",&(dc->cov[i]));
      if (n!=1 || n==EOF) {
	 *err = addError(ce_badFormat,"bad format",*err,__LINE__);
	 return;
      }
   }
   fclose(F);
}

/* Really only reads a covariance (in column format) for xi+ */
void read_cov_col(datcov *dc, char cov_name[], error **err)
{
   FILE *F;
   int i, j, res, nn;
   unsigned int ncomment;
   double dummy[5];

   nn        = numberoflines_comments(cov_name, &ncomment, err); forwardError(*err, __LINE__,);
   dc->n     = (int)sqrt(nn);
   dc->Ntheta = dc->n;
   dc->Nzbin = dc->Nzcorr = 1;
   F         = fopen_err(cov_name, "r", err);                 forwardError(*err, __LINE__,);
   dc->cov   = malloc_err(sizeof(double)*dc->n*dc->n, err);   forwardError(*err, __LINE__,);
   dc->theta = malloc_err(sizeof(double)*dc->n, err);         forwardError(*err, __LINE__,);

   for (i=0; i<dc->n; i++) {
      for (j=0; j<dc->n; j++) {
	 res = fscanf(F, "%lf %lf %lf %lf %lf\n", dummy, dummy+1, dummy+2, dummy+3, dummy+4);
	 testErrorRet(res==EOF, io_eof, "Premature eof", *err, __LINE__,);
	 if (i==0) {
	    dc->theta[j] = dummy[1];
	 }
	 dc->cov[i*dc->n+j] = dummy[2];
      }
   }

}

/* ============================================================ *
 * Lensing signal, shear second-order statistics for angular    *
 * scale theta [rad].						*
 * ============================================================ */

double lensing_signal(cosmo_lens *model, double theta, int i_bin, int j_bin, lensdata_t type,
		      decomp_eb_filter_t decomp_eb_filter, error **err)
{
   double res, resE, resB, eta;
   const double *a;
   int N;

   testErrorRetVA(type!=decomp_eb && decomp_eb_filter!=decomp_eb_none, lensing_type,
		  "lensdata type (%d) and decomp_eb_filter type (%d) not compatible",
		  *err, __LINE__, 0.0, type, decomp_eb_filter);

   switch (type) {
      case xip :
	 res = xi(model, +1, theta, i_bin, j_bin, err);
	 forwardError(*err, __LINE__, 0);
	 break;
      case xim :
	 res = xi(model, -1, theta, i_bin, j_bin, err);
	 forwardError(*err, __LINE__, 0);
	 break;
      case map2poly :
	 res = map2_poly(model, theta, i_bin, j_bin, err);
	 forwardError(*err, __LINE__, 0);
	 break;
      case map2gauss :
	 res = map2_gauss(model, theta, i_bin, j_bin, err);
	 forwardError(*err, __LINE__, 0);
	 break;
      case gsqr :
	 res = gamma2(model, theta, i_bin, j_bin, err);
	 forwardError(*err, __LINE__, 0);
	 break;
      case decomp_eb :
	 switch (decomp_eb_filter) {
	    case FK10_SN        : a = a_FK10_SN;        eta = eta_FK10_SN;        N = N_FK10; break;
	    case FK10_FoM_eta10 : a = a_FK10_FoM_eta10; eta = eta_FK10_FoM_eta10; N = N_FK10; break;
	    case FK10_FoM_eta50 : a = a_FK10_FoM_eta50; eta = eta_FK10_FoM_eta10; N = N_FK10; break;
	    default : *err = addErrorVA(lensing_type, "Unknown decomp_eb_filter type %d",
				      *err, __LINE__, decomp_eb_filter);
	       return 0.0;
	 }
	 resE = 0.5*RR(model, theta*eta, theta, a, N, cheby2, +1, err);
	 forwardError(*err, __LINE__, 0.0);
	 resB = 0.5*RR(model, theta*eta, theta, a, N, cheby2, -1, err);
	 forwardError(*err, __LINE__, 0.0);
	 res = resE + resB;
	 break;
      case xipm : case nofz :
      default :
	 *err = addErrorVA(ce_unknown, "Unknown or invalid lensdata type %d(%s)",
			   *err, __LINE__, type, slensdata_t(type));
	 return 0.0;
   }

   return res;
}

#define NPERBIN 20
double chi2_lensing(cosmo_lens* csm, datcov* dc, error **err)
{
   double *data_minus_model, model, th, dth, w, wtot;
   int i, j, in,i_bin, j_bin, Nzbin;
   double res;
   lensdata_t type;
   gsl_matrix_view A;
   gsl_vector_view x;

   /* Unphysically high baryon fraction, caused infinite chi^2 values. */
   testErrorRet(csm->cosmo->Omega_b/csm->cosmo->Omega_m>BARYON_FRAC, lensing_baryon_fraction,
		"Baryon fraction unphysically high", *err, __LINE__, 0.0);

   Nzbin = csm->redshift->Nzbin;
   testErrorRetVA(Nzbin!=dc->Nzbin, redshift_Nzbin,
		  "Number of redshift bins for model (%d) inconsistent with data (%d)",
		  *err, __LINE__, 0, Nzbin, dc->Nzbin);

   data_minus_model = malloc_err(sizeof(double)*dc->n, err);
   forwardError(*err, __LINE__, 0);


   for (i_bin=0,in=0; i_bin<Nzbin; i_bin++) {
      for (j_bin=i_bin; j_bin<Nzbin; j_bin++) {
	 for (j=0; j<dc->Ntheta; j++,in++) {

	    data_minus_model[in] = 0.0;

	    if (dc->type==xipm) {
	       if (j<dc->Ntheta/2) type = xip;
	       else type = xim;
	    } else {
	       type = dc->type;
	    }

	    if (dc->format==angle_mean) {

	       /* Average model over bin width */
	       dth = (dc->theta2[j]-dc->theta[j])/(double)(NPERBIN-1.0);
	       for (i=0,model=0.0,th=dc->theta[j]; i<NPERBIN; i++,th+=dth) {
		  model += lensing_signal(csm, th, i_bin, j_bin, type, dc->decomp_eb_filter, err);
		  forwardError(*err, __LINE__, 0.0);
	       }
	       model /= (double)NPERBIN;

	    }  else if (dc->format==angle_wlinear) {

	       dth = (dc->theta2[j]-dc->theta[j])/(double)(NPERBIN-1.0);
	       for (i=0,model=wtot=0.0,th=dc->theta[j]; i<NPERBIN; i++,th+=dth) {
		  w      = th/arcmin;
		  model += w*lensing_signal(csm, th, i_bin, j_bin, type, dc->decomp_eb_filter, err);
		  forwardError(*err, __LINE__, 0.0);
		  wtot  += w;
	       }
	       model /= wtot;

	    }  else if (dc->format==angle_wquadr) {

	       dth = (dc->theta2[j]-dc->theta[j])/(double)(NPERBIN-1.0);
	       for (i=0,model=wtot=0.0,th=dc->theta[j]; i<NPERBIN; i++,th+=dth) {
		  w      = dc->a1*th/arcmin + dc->a2*dsqr(th/arcmin);
		  model += w*lensing_signal(csm, th, i_bin, j_bin, type, dc->decomp_eb_filter, err);
		  forwardError(*err, __LINE__, 0.0);
		  wtot  += w;
	       }
	       model /= wtot;

	    } else {

	       /* Model at bin center */
	       model = lensing_signal(csm, dc->theta[j], i_bin, j_bin, type, dc->decomp_eb_filter, err);
	       forwardError(*err, __LINE__, -1);

	    }

	    testErrorRetVA(in>=dc->n, math_overflow, "Overflow, data index %d>=%d", *err, __LINE__, 0, in, dc->n);

	    data_minus_model[in] = dc->data[in] - model;

	 }
      }
   }


   res = 0.0;

   if (dc->usecov) {

      x = gsl_vector_view_array(data_minus_model, dc->n);
      A = gsl_matrix_view_array(dc->cov, dc->n, dc->n);

      /* Calculate L^{-1} * (data-model) */
      gsl_blas_dtrsv(CblasLower, CblasNoTrans, CblasNonUnit, &A.matrix, &x.vector);
      /* Square of the above */
      for (i=0; i<dc->n; i++) {
	 res += dsqr(gsl_vector_get(&x.vector, i));
      }

      /* // MKDEBUG: old scalar product using inverse covariance
	 for (i=0; i<dc->n; i++) {
	 for (j=0; j<dc->n; j++) {
	    res += data_minus_model[i]*dc->cov[i*dc->n+j]*data_minus_model[j];
	 }
      }
      */


   } else {

      /* TODO!! Needed anyway? */
      for (i=0;i<dc->n;i++) {
	 res += dsqr(data_minus_model[i]/dc->var[i]);
      }

   }

   testErrorRetVA(res<0.0, math_negative, "Negative chi^2 %g. Maybe the covariance matrix is not positive",
		  *err, __LINE__, -1.0, res);

   testErrorRet(!finite(res), ce_infnan, "inf or nan", *err, __LINE__, -1);
   free(data_minus_model);

   /* det C ... */
   res = -0.5*res;

   return res;
}
#undef NPERBIN
