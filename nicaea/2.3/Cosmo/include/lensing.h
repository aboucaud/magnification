/* ============================================================ *
 * lensing.h							*
 *								*
 * Martin Kilbinger, Karim Benabed 2006-2009			*
 * ============================================================ */


#ifndef __LENSING_H
#define __LENSING_H

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#include <gsl/gsl_linalg.h>

#include "errorlist.h"
#include "maths.h"
#include "io.h"
#include "mvdens.h"
#include "halomodel.h"
#include "cosmo.h"
#include "nofz.h"
#include "decomp_eb.h"
#include "reduced_fit.h"
#include "par.h"


/* Dimensions of interpolation tables */
#define N_s     200
#define N_theta 100

/* Ranges of interpolation table for convergence power spectrum. *
 * Power-law extrapolation outside these ranges.		 */
#define s_min     1.0e-2
#define s_max     1.0e6

/* Ranges of interpolation table for reduced-shear correction    *
 * power spectrum. No extrapolation outside these ranges.	 */
#define ELL_MIN_REDUCED            0.1
#define ELL_MAX_REDUCED            2.0e5
#define THETA_P_MIN_REDUCED        (0.1*arcmin)
#define THETA_M_MIN_REDUCED        (0.5*arcmin)
#define THETA_MAP_MIN_REDUCED      (0.2*arcmin)
#define THETA_GSQR_MIN_REDUCED     (0.1*arcmin)
#define THETA_MAPGAUSS_MIN_REDUCED (0.1*arcmin)
#define THETA_MAX_REDUCED          (1000.0*arcmin)

#define NELL_REDUCED 50


#define lensing_base                     -1400
#define lensing_inconsistent             -1 + lensing_base
#define lensing_baryon_fraction          -2 + lensing_base
#define lensing_tomoij			 -3 + lensing_base
#define lensing_initialised              -4 + lensing_base
#define lensing_unknown                  -5 + lensing_base
#define lensing_pm                       -6 + lensing_base
#define lensing_type                     -7 + lensing_base
#define lensing_fastxi                   -8 + lensing_base

/* If Ob/Oc > BARYON_FRAC, chi2 produces an error */
#define BARYON_FRAC 0.75


#define Nlensdata_t 8
typedef enum {xipm, xip, xim, map2poly, map2gauss, gsqr, decomp_eb, nofz} lensdata_t;
#define slensdata_t(i) ( \
 i==xipm      ? "xipm" : \
 i==xip       ? "xip"  : \
 i==xim       ? "xim"  : \
 i==map2poly  ? "map2poly" : \
 i==map2gauss ? "map2gauss" : \
 i==gsqr      ? "gsqr" : \
 i==decomp_eb ? "decomp_eb" : \
 i==nofz      ? "nofz" : \
 "")

typedef enum {decomp_eb_none, FK10_SN, FK10_FoM_eta10, FK10_FoM_eta50} decomp_eb_filter_t;
#define sdecomp_eb_filter_t(i) (		\
 i==decomp_eb_none ? "none" : \
 i==FK10_SN        ? "FK10_SN" : \
 i==FK10_FoM_eta10 ? "FK10_FoM_eta10" : \
 i==FK10_FoM_eta50 ? "FK10_FoM_eta50" : \
 "")
#define Ndecomp_eb_filter_t 4

/* The following arrays are defined in decomp_eb.c */
extern const double a_FK10_SN[], a_FK10_FoM_eta10[], a_FK10_FoM_eta50[];

typedef enum {angle_center, angle_mean, angle_wlinear, angle_wquadr} lensformat_t;
#define slensformat_t(i) ( \
 i==angle_center  ? "angle_center" : \
 i==angle_mean    ? "angle_mean" : \
 i==angle_wlinear ? "angle_wlinear" : \
 i==angle_wquadr  ? "angle_wquadr" : \
 "")
#define Nlensformat_t 4

typedef enum {tomo_all, tomo_auto_only, tomo_cross_only} tomo_t;
#define stomo_t(i) ( \
 i==tomo_all        ? "tomo_all" : \
 i==tomo_auto_only  ? "tomo_auto_only" : \
 i==tomo_cross_only ? "tomo_cross_only" : \
 "")
#define Ntomo_t 3

typedef enum {reduced_none, reduced_K10} reduced_t;
#define sreduced_t(i) ( \
  i==reduced_none ? "none" : \
  i==reduced_K10  ? "K10" : \
  "")
#define Nreduced_t 2

typedef struct {

  /* Basic cosmology */
  cosmo *cosmo;

  /* Redshift distribution(s) */
  redshift_t *redshift;

  /* Tomography type */
  tomo_t tomo;

  /* Reduced-shear correction */
  reduced_t reduced;
  double q_mag_size;  /* q_mag_size = 2(alpha+beta-1),         *
		       * alpha, beta: slopes of number density *
		       * with flux (alpha), size (beta)        */

  /* Halomodel stuff (only initialised if cosmo->nonlinear=halodm) */
  cosmo_hm *hm;

  /* ============================================================ *
   * Precomputed stuff.						  *
   * ============================================================ */

  interTable **g_source;
  interTable **Pshear, **Pg1;

  /* Shear second-order functions */
  interTable **xiP, **xiM, **gamma, **map_gauss, **map_poly;
} cosmo_lens;

typedef struct {
  double r;
  cosmo_lens* self;
} cosmo_lensANDdouble;

typedef struct {
  int i;
  double r;
  cosmo_lens *self;
} cosmo_lensANDintANDdouble;

typedef struct {
  int i, j;
  double r;
  cosmo_lens *self;
} cosmo_lensANDiid;

typedef struct {
  int i, j, t;
  double r;
  cosmo_lens *self;
} cosmo_lensANDiiid;

typedef struct {
  int N;	     /* total nb of data points, e.g. for xipm: xip and *
		      * xim have N/2 data points each                   */
  double *theta;     /* bin centers in arcmin */
} bin_data;

typedef struct {
  int Ntheta, Nzbin;  /* Number of angular and redshift bins */
  int Nzcorr;         /* Number of z-correlations, Nzcorr=Nzbin*(Nzbin+1)/2 */
  int n;              /* Number of total entries in data vector, n=Ntheta*Nzcorr */
  double *theta;      /* n-dimensional vector of angular scales */
  double *theta2;     /* For lensformat janminmax: (theta,theta2) = (lower,upper) bin limits */ 
  double *data;       /* n-dimensional data vector */
  double *var;        /* n-dimensional vector with variance */
  double *cov;        /* nxn-dimensional covariance matrix */
  double a1, a2;      /* Coefficients for 'angle_wquadr' */
  int Nexclude, *exclude;   /* Number and indices of z-bins to exclude from analysis */
  int usecov;
  lensdata_t type;
  lensformat_t format;
  decomp_eb_filter_t decomp_eb_filter;
} datcov;

/* ============================================================ *
 * Initialisation.						*
 * ============================================================ */

cosmo_lens *init_parameters_lens(double OMEGAM, double OMEGAV, double W0_DE, double W1_DE, 
				 double H100, double OMEGAB, double OMEGANUMASS,
				 double NEFFNUMASS, double NORM, double NSPEC,
				 int Nzbin, const int *Nnz, const nofz_t *nofz, double *par_nz,
				 nonlinear_t NONLINEAR, transfer_t TRANSFER,
				 growth_t GROWTH, de_param_t DEPARAM,
				 norm_t normmode, tomo_t TOMO, reduced_t REDUCED, double Q_MAG_SIZE, error **err);
cosmo_lens* copy_parameters_lens_only(cosmo_lens* source, error **err);
cosmo_lens* copy_parameters_lens(cosmo_lens* source, sm2_error **err);
void updateFrom_lens(cosmo_lens* avant, cosmo_lens* apres, error **err);
void copy_parameters_lenshm_cosmo(cosmo_lens *model, error **err);
void read_cosmological_parameters_lens(cosmo_lens **self, FILE *F, error **err);
cosmo_lens* set_cosmological_parameters_to_default_lens(error **err);
void free_parameters_lens(cosmo_lens** self);
void dump_param_lens(cosmo_lens* self, FILE *F, int wnofz, error **err);

/* ============================================================ *
 * Lensing functions.						*
 * ============================================================ */

/* Projection */
double int_for_g(double aprime, void *intpar, error **err);
double g_source(cosmo_lens*, double a, int n_bin, error **err);
double G(cosmo_lens* self, double a, int n_bin, error **err);
double P_NL_tot(cosmo_lens *self, double a, double k, error **err);
double int_for_p_2(double a, void *intpar,error **err);
double Pshear(cosmo_lens *self, double a, int i_bin, int j_bin, error **err);
double P_projected_kappa(void *self, double l, int i_bin, int j_bin, error **err);

/* Reduced-shear correction (K10) */
extern const int parameter[M_PAR];
cosmo *set_cosmological_parameters_to_WMAP7(const redshift_t *nofz, tomo_t tomo, error **err);
double *par_to_pointer(cosmo *self, par_t par, error **err);
void fill_dpar(cosmo *model, cosmo *wmap7, double *dpar, error **err);
double Fbar(cosmo_lens *self, double a, int m_bin, int n_bin, error **err);
void fill_Fbar_array(cosmo_lens *self, double *fbar, int m_bin, int n_bin, double amin, int N_a,
		     double da, error **err);
void fill_dFbar_dp_array(cosmo_lens *self, par_t par, double *dfbar_dp, int m_bin, int n_bin, double amin,
			 int N_a, double da, error **err);
double Pg1(cosmo_lens *self, double s, int i_bin, int j_bin, error **err);

/* Second-order shear functions */
double xi(cosmo_lens*, int pm, double theta, int i_bin, int j_bin, error **err);
double gamma2(cosmo_lens*, double theta, int i_bin, int j_bin, error **err);
double map2_poly(cosmo_lens*, double theta, int i_bin, int j_bin, error **err);
double map2_gauss(cosmo_lens*, double theta, int i_bin, int j_bin, error **err);
double RR(cosmo_lens *lens, double THETA_MIN, double THETA_MAX, const double *a, int N,
	  poly_t poly, int pm, error **err);
double int_for_map2_slow(double ell, void *intpar, error **err);
double map2_slow(cosmo_lens *self, double theta, tpstat_t tpstat, int i_bin, int j_bin, error **err);

/* Reading data files */
datcov *init_data_cov(char* dataname, char* covname, lensdata_t type, int isnofz, error **err);
datcov *init_data_cov_tomo(char* dataname, char* covname, lensdata_t type, decomp_eb_filter_t decomp_eb_filter, 
			   lensformat_t format, double corr_invcov,
			   double a1, double a2, int Nexclue, const int *exclude, error **err);
datcov *init_datcov_for_cov_only(int Nzbin, int Ntheta, error **err);

void del_data_cov(datcov** dc);
void read_data(datcov *dc, char data_name[], error **err, int isnofz);
void read_data_tomo(datcov *dc, char data_name[], int Nzbin, error **err);
void read_cov_tomo(datcov* dc, char cov_name[], error **err);
void datcov2xipm(const datcov *dc, int i_bin, int j_bin, double **xip, double **xim, double **theta,
		 double **theta2, int *N, error **err);

void read_cov(datcov* dc, char cov_name[], error **err);
void read_cov_col(datcov *dc, char cov_name[], error **err);

double lensing_signal(cosmo_lens *model, double theta, int i_bin, int j_bin, lensdata_t type,
		      decomp_eb_filter_t decomp_eb_filter, error **err);
double chi2_lensing(cosmo_lens* csm, datcov* dc, error **err);

#define CHANGE(fct) int change_##fct(cosmo_lens*, cosmo_lens*)
CHANGE(g_source);
CHANGE(Pshear);
CHANGE(xi);
CHANGE(gamma2);
CHANGE(map2);
#undef CHANGE


#endif /* __LENSING_H */

