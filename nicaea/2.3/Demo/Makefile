# -*- Makefile -*-
#
# Standalone Makefile for Demo
# Demo/Makefile.sa
# In the 'nicaea' package, this file is renamed to 'Demo/Makefile'
# Martin Kilbinger 2008, 2009

CC         = gcc

FFTW       = /usr/local
GSL        = /usr/local

WARNINGS   = -Wall -Wuninitialized -pedantic
OPTIM      = -O3
FPIC	   = -fPIC
ARCHFLAGS  = -std=gnu9x
# ARCHFLAGS  = -arch=x86_64


cflags     = -I$(FFTW)/include -I$(GSL)/include $(OPTIM) $(WARNINGS) $(ARCHFLAGS) $(FPIC)
lflags     = -L$(FFTW)/lib -L$(GSL)/lib -lm -lgsl -lgslcblas -lfftw3

#objs      := $(patsubst %.h,%.o,$(wildcard *.h))
objs       = cosmo.o lensing.o reduced_fit.o decomp_eb.o sn1a.o nofz.o \
			 cmb_bao.o halomodel.o hod.o errorlist.o io.o maths.o mvdens.o config.o par.o


all: links_check lensingdemo # halomodeldemo decomp_eb_demo

links_check:
	@ if ! test -e cosmo.c ; \
	then echo "Type 'make links' before 'make' to compile demo programs"; \
	fi

libnicaea.so: $(objs)
	$(CC) -shared -o $@ $^ $(lflags)

lensingdemo: $(objs) lensingdemo.o
	$(CC) -o $@ $^ $(lflags)

halomodeldemo : $(objs) halomodeldemo.o $(libs)
	$(CC) -o $@ $^ $(lflags)

decomp_eb_demo: $(objs) decomp_eb.o decomp_eb_demo.o
	$(CC) -o $@ $^ $(lflags)
decomp_eb_demo_data: $(objs) decomp_eb.o decomp_eb_demo_data.o
	$(CC) -o $@ $^ $(lflags)

%.o:	%.c
	$(CC) $< -c $(cflags) -o $@

.h:



PHONY: links clean linksclean

links:
	ln -sf ../Cosmo/src/cosmo.c
	ln -sf ../Cosmo/src/lensing.c
	ln -sf ../Cosmo/src/reduced_fit.c
	ln -sf ../Cosmo/src/decomp_eb.c
	ln -sf ../Cosmo/src/sn1a.c
	ln -sf ../Cosmo/src/nofz.c
	ln -sf ../Cosmo/src/cmb_bao.c
	ln -sf ../halomodel/src/halomodel.c
	ln -sf ../halomodel/src/hod.c
	ln -sf ../tools/src/errorlist.c
	ln -sf ../tools/src/io.c
	ln -sf ../tools/src/maths.c
	ln -sf ../tools/src/config.c
	ln -sf ../tools/src/mvdens.c
	ln -sf ../tools/src/par.c

	ln -sf ../Cosmo/include/cosmo.h
	ln -sf ../Cosmo/include/lensing.h
	ln -sf ../Cosmo/include/reduced_fit.h
	ln -sf ../Cosmo/include/decomp_eb.h
	ln -sf ../Cosmo/include/sn1a.h
	ln -sf ../Cosmo/include/nofz.h
	ln -sf ../Cosmo/include/cmb_bao.h
	ln -sf ../halomodel/include/halomodel.h
	ln -sf ../halomodel/include/hod.h
	ln -sf ../tools/include/errorlist.h
	ln -sf ../tools/include/io.h
	ln -sf ../tools/include/maths.h
	ln -sf ../tools/include/maths_base.h
	ln -sf ../tools/include/config.h
	ln -sf ../tools/include/mvdens.h
	ln -sf ../tools/include/par.h

linksclean:
	rm -f cosmo.{c,h} lensing.{c,h} sn1a.{c,h} errorlist.{c,h} io.{c,h} \
	maths.{c,h} maths_base.{c,h} decomp_eb.{c,h}

clean:
	rm -rf *~ *.o *.a *.so *.oct *.dylib

