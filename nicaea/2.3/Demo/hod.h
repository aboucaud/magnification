/* ============================================================ *
 * hod.h							*
 * Martin Kilbinger, Henry J. McCracken 2008-2010		*
 * ============================================================ */

#ifndef __HOD_H
#define __HOD_H

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include <string.h>

#include <gsl/gsl_sf_erf.h>
#include <gsl/gsl_integration.h>

#include "io.h"
#include "errorlist.h"
#include "config.h"
#include "maths.h"

#include "cosmo.h"
#include "nofz.h"
#include "halomodel.h"


/* Limits for xi(r), 1- and 2-halo, in Mpc.h */
#define RMIN1 0.01
#define RMAX1 5.0
#define RMIN2 0.1
#define RMAX2 400.0

#define RMIN  RMIN1
#define RMAX  RMAX2
#define NR    50


#define MAXCHAR 1024
#define NLINES 100


#define Nhalodata_t 1
typedef enum {woftheta} halodata_t; 
#define shalodata_t(i) ( \
 i==woftheta ? "woftheta" : \
 "")

#define Nhalomode_t 3
typedef enum {hjmcc, hjmcc_cov, logw} halomode_t;
#define shalomode_t(i) ( \
 i==hjmcc      ? "hjmcc" : \
 i==hjmcc_cov  ? "hjmcc_cov" : \
 i==logw       ? "logw" : \
 "")

#define Nngal_fit_t 5
typedef enum {ngal_log_fit, ngal_lin_fit, ngal_no_fit, ngal_lin_fit_only, ngal_match_M1} ngal_fit_t;
#define sngal_fit_t(i) ( \
  i==ngal_log_fit  ? "ngal_log_fit"  : \
  i==ngal_lin_fit  ? "ngal_lin_fit"  : \
  i==ngal_no_fit   ? "ngal_no_fit"   : \
  i==ngal_lin_fit_only ? "ngal_lin_fit_only" : \
  i==ngal_match_M1 ? "ngal_match_M1" : \
  "")

typedef struct { 
  char WTHETA[500];
  char COVNAME[500];
  char DNDZ[500];
  char OUTFILE[500];
  int nbins; 
  double ngal, ngalerr, area_deg, intconst, delta;
  double alpha_min,alpha_max,dalpha;
  double M1_min,M1_max,dM1;
  double Mmin_min,Mmin_max,dMmin;
  double Ngal_min,Ngal_max,dNgal;
  ngal_fit_t ngal_fit_type;
} config_hm_info;


typedef struct {
  double Mmin;
  double M1, M0, sigma_log_M;
  double alpha;
  double Ngal;
} haloparams;


/* This structure contains the w_theta results */
typedef struct {
  int nbins;
  double *w;
  double *werr;   /* Error bars */
  double *wcov;   /* Covariance matrix */
  double *th;
} wt_t;


void haloplot(haloparams halo_in, const char *parfile, FILE *oF, FILE *oFh);


double n_gal(cosmo_hm *self, double a, error **err);
double ngd_obs_weighted(cosmo_hm *self, double area, double ngal, error **err);
double int_for_ngd_obs_weighted1(double z, void *intpar, error **err);
double int_for_ngd_obs_weighted2(double z, void *intpar, error **err);

double int_for_ngal(double logM, void *intpar, error **err);
double Ngal_M (cosmo_hm *self, double M,error **err);
double Ngal_satellite_M(cosmo_hm *self, double M, double ncen, error **err);
double Ngal_central_M(cosmo_hm *self, double M, error **err);
double Ngal_M_pair(cosmo_hm *self, double M, error **err);
double Ngal_den(cosmo_hm *self, error **err);
double int_for_nden1(double z, void *intpar, error **err);
double int_for_nden2(double z, void *intpar, error **err);
double Vc(cosmo_hm *self, error **err);

double av_frsat(cosmo_hm *self, double a, error **err);
double int_for_av_frsat1(double logM, void *intpar, error **err);
double int_for_av_frsat2(double logM, void *intpar, error **err);

double av_ngh(cosmo_hm *self, double a, error **err);
double int_for_av_ngh1(double logM, void *intpar, error **err);
double int_for_av_ngh2(double logM, void *intpar, error **err);

double av_gal_bias(cosmo_hm *self, double a, error **err);
double int_for_galbias1(double logM, void *intpar, error **err);

double av_halo_mass(cosmo_hm *self, double a, error **err);
double int_for_avhalo1(double logM, void *intpar, error **err);
double int_for_avhalo2(double logM, void *intpar, error **err);

double xi1_cs(cosmo_hm *self, double a, double r,  error **err);        
double int_for_xi1_cs(double k, void *intpar,  error **err);  // central-satellite term
double xi1_ss(cosmo_hm *self, double a, double r,  error **err);        
double int_xi1_ss_qawo(double x, void *params);
double int_for_xi1_ss(double k, void *intpar,  error **err);  // satellite-satellite term
double xi1_ss_FT(cosmo_hm *self, double a, double r, error **err);
double xi2(cosmo_hm *self, double a, double r, error **err);
double xir_g(cosmo_hm *self, double a, double r, pofk_t pokf_xir, error **err);
double int_xi2_qawo(double x, void *params);
double int_for_xi2g(double k, void *intpar,  error **err);

double P1h_g_ss(cosmo_hm *self, double a, double k, error **err);
double int_for_P1h_g_ss(double logM, void *intpar, error **err);
double int_for_P1h_g(double logM, void *intpar, error **err);

double P1h_g(cosmo_hm *self, double a, double k, error **err);
double P2h_g(cosmo_hm *self, double a, double k, error **err);
double int_for_P2h_g(double logM, void *intpar, error **err);
double Pth_g(cosmo_hm *self, double a, double k, error **err);

/* Halo exclusion stuff */
double n_gal_hexcl_triaxial(cosmo_hm *self, double r, double a, error **err);
double n_gal_hexcl(cosmo_hm *self, double a, double logMlim, error **err);
double M_lim(cosmo_hm *self, double r, double a, error **err);
double P2h_g_hexcl(cosmo_hm *self, double a, double k, double r, error **err);

wt_t *read_wtheta(config_hm_info *config, error **err);
wt_t *read_wtheta_mk(const char *name, double delta, double intconst, error **err);
nz_t *read_dndz (char *infile, error **err);
void read_config_hm_file(config_hm_info *config, char cname[], error **err);

double w_of_theta(cosmo_hm *self, double theta, int i_bin, int j_bin, error **err);
double w_lim(cosmo_hm *self, nz_t *nzr, double theta, pofk_t pofk,
	     error **err);
double w_lim_fast(cosmo_hm *self, nz_t *nzr, double theta, pofk_t pofk,
		  error **err);
double w_lim_hankel(cosmo_hm *self, double theta, int i_bin, int j_bin, pofk_t pofk, error **err);
double int_over_xir_u(cosmo_hm *self, double theta, double z, pofk_t pofk, error **err);
double w_from_xir(cosmo_hm *self, double theta, int i_bin, int j_bin, pofk_t pofk_w_from_xir, error **err);
double w_zed(double z, void *inpar, error **err);
double limber(double k, void *intpar, error **err);
double ngd_volume(cosmo_hm *model, double ngal, double area, error **err);

double compute_chisq(cosmo_hm *self, const wt_t *wth, 
		     double ngd_obs, double ngd_err, ngal_fit_t ngal_fit_type,
		     double *ngd, int dologw, error **err);
double chi2_hm(cosmo_hm *model, halomode_t halomode, const wt_t* data, ngal_fit_t ngal_fit_type, 
	       double ngal, double ngalerr, double area, error **err);


double int_for_P_projected_hm(double z, void *intpar, error **err);
double P_projected_hm(void *cs2v, double l, int i_bin, int j_bin, error **err);


#endif
