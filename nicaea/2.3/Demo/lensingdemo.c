/* ============================================================ *
 * lensingdemo.c						*
 * Martin Kilbinger 2008-2010					*
 * ============================================================ */

#include <stdio.h>
#include <stdlib.h>

#include "errorlist.h"
#include "io.h"
#include "maths.h"
#include "cosmo.h"
#include "cmb_bao.h"
#include "lensing.h"
#include "nofz.h"


int main(int argc, char** argv)
{
   cosmo_lens* model;
   double k, ell, theta, a, pk, pg, f, z;
   int i_bin, j_bin, Nzbin;
   error *myerr = NULL, **err;
   FILE *F;


   err = &myerr;

   printf("# lensingdemo (M. Kilbinger, 2006-2010)\n");


   /* Setting up cosmology */

   F = fopen_err("cosmo_lens.par", "r", err);
   quitOnError(*err, __LINE__, stderr);
   read_cosmological_parameters_lens(&model, F, err);
   quitOnError(*err, __LINE__, stderr);
   fclose(F);

   Nzbin = model->redshift->Nzbin;

   printf("# Cosmological parameters:\n");
   dump_param_lens(model, stdout, 1, err);
   quitOnError(*err, __LINE__, stderr);
   printf("a_min = %g\n", model->cosmo->a_min);


   for (i_bin=0; i_bin<Nzbin; i_bin++) {
      printf("Mean redshift (bin %d) = %.3f\n", i_bin, zmean(model->redshift, i_bin, err));
      quitOnError(*err, __LINE__, stderr);
   }

   /* Density power spectrum */

   printf("Calculating transfer function, writing file T\n");
   F = fopen("T", "w");
   dump_param_lens(model, F, 1, err);
   quitOnError(*err, __LINE__, stderr);

   fprintf(F, "# k[h/Mpc] T(k)\n");
   for (k=0.0001; k<=100.0; k*=1.05) {
      fprintf(F, "%e %e\n", k, Transfer_function(model->cosmo, k, err));
      quitOnError(*err, __LINE__, stderr);
   }
   fclose(F);


   printf("Calculating density power spectrum, writing file P_delta\n");
   F = fopen("P_delta", "w");
   dump_param_lens(model, F, 1, err);
   quitOnError(*err, __LINE__, stderr);

   fprintf(F, "# k[Mpc/h] Delta_NL(a=0.5,1.0,k)\n");
   for (k=0.0001; k<=100.0; k*=1.05) {
      //f = k*k*k/(2.0*pi_sqr);
      f = 1.0;
      fprintf(F, "%e ", k);
      for (a=1.0; a>=0.5; a-=0.5) {
	 fprintf(F, "%e ", P_NL(model->cosmo, a, k, err)*f);
	 quitOnError(*err, __LINE__, stderr);
      }
      fprintf(F, "\n");
   }
   fclose(F);


   /* Redshift distribution */
   printf("Printing redshift distribution to file nz\n");
   F = fopen("nz", "w");
   dump_param_lens(model, F, 1, err);
   quitOnError(*err, __LINE__, stderr);

   for (z=0; z<=10; z+=0.01) {
      fprintf(F, "%.3f", z);
      for (i_bin=0; i_bin<Nzbin; i_bin++) {
	 fprintf(F, " %g", prob(model->redshift, z, i_bin, err));
	 quitOnError(*err, __LINE__, stderr);
      }
      fprintf(F, "\n");
   }
   fclose(F);


   /* Convergence power spectrum */

   printf("Calculating convergence power spectrum, writing file P_kappa\n");
   F = fopen("P_kappa", "w");
   dump_param_lens(model, F, 1, err);
   quitOnError(*err, __LINE__, stderr);

   fprintf(F, "# Shear power spectra for %d z-bin%s\n", Nzbin, Nzbin==1?"":"s");
   fprintf(F, "# l            ");
   for (i_bin=0; i_bin<Nzbin; i_bin++) {
      for (j_bin=i_bin; j_bin<Nzbin; j_bin++) {
	    if (model->tomo==tomo_auto_only && i_bin!=j_bin) continue;
	    if (model->tomo==tomo_cross_only && i_bin==j_bin) continue;
	    fprintf(F, "P_%s^%d%d(l)    ", model->reduced==reduced_K10?"t":"k", i_bin, j_bin);
      }
   }
   fprintf(F, "   ");
   for (i_bin=0; i_bin<Nzbin; i_bin++) {
      for (j_bin=i_bin; j_bin<Nzbin; j_bin++) {
	 if (model->tomo==tomo_auto_only && i_bin!=j_bin) continue;
	 if (model->tomo==tomo_cross_only && i_bin==j_bin) continue;
	 fprintf(F, "P_g1^%d%d(l)   ", i_bin, j_bin);
      }
   }
   fprintf(F, "\n");

   for (ell=0.01; ell<=5.0e5; ell*=1.5) {
      f = ell*(ell+1)/twopi;
      fprintf(F, "%e  ", ell);
      for (i_bin=0; i_bin<Nzbin; i_bin++) {
	 for (j_bin=i_bin; j_bin<Nzbin; j_bin++) {
	    if (model->tomo==tomo_auto_only && i_bin!=j_bin) continue;
	    if (model->tomo==tomo_cross_only && i_bin==j_bin) continue;
	    pk = Pshear(model, ell, i_bin, j_bin, err);
	    if (getErrorValue(*err)==reduced_fourier_limit) {
	       purgeError(err);
	    }
	    quitOnError(*err, __LINE__, stderr);
	    fprintf(F, " %e", pk*f);
	 }
      }
      fprintf(F, "   ");
      for (i_bin=0; i_bin<Nzbin; i_bin++) {
	 for (j_bin=i_bin; j_bin<Nzbin; j_bin++) {
	    if (model->tomo==tomo_auto_only && i_bin!=j_bin) continue;
	    if (model->tomo==tomo_cross_only && i_bin==j_bin) continue;
	    if (model->reduced!=reduced_none) {
	       pg = Pg1(model, ell, i_bin, j_bin, err);
	       if (getErrorValue(*err)==reduced_fourier_limit) {
		  purgeError(err);
	       }
	       quitOnError(*err, __LINE__, stderr);
	    } else {
	       pg = 0.0;
	    }
	    fprintf(F, " %e", pg*f);
	 }
      }
      fprintf(F, "\n");
   }
   fclose(F);


   /* Shear correlation functions */

   printf("Calculating shear correlation function, writing files xi_p, xi_m\n");
 
   F = fopen("xi_p", "w");
   dump_param_lens(model, F, 1, err);
   quitOnError(*err, __LINE__, stderr);

   fprintf(F, "# theta[arcmin] xi+^{mn}(theta)\n#        ");
   for (i_bin=0; i_bin<Nzbin; i_bin++) {
      for (j_bin=i_bin; j_bin<Nzbin; j_bin++) {
	 if (model->tomo==tomo_auto_only && i_bin!=j_bin) continue;
	 if (model->tomo==tomo_cross_only && i_bin==j_bin) continue;
	 fprintf(F, "           %d%d", i_bin, j_bin);
      }
   }
   fprintf(F, "\n");

   for (theta=0.5*arcmin; theta<400*arcmin; theta*=1.1) {
      fprintf(F, "%9.3f", theta/arcmin);
      for (i_bin=0; i_bin<Nzbin; i_bin++) {
	 for (j_bin=i_bin; j_bin<Nzbin; j_bin++) {
	    if (model->tomo==tomo_auto_only && i_bin!=j_bin) continue;
	    if (model->tomo==tomo_cross_only && i_bin==j_bin) continue;
	    fprintf(F, " %e", xi(model, +1, theta, i_bin, j_bin, err));
	    quitOnError(*err, __LINE__, stderr);
	 }
      }
      fprintf(F, "\n");
   }
   fclose(F);

   F = fopen("xi_m", "w");
   dump_param_lens(model, F, 1, err);
   quitOnError(*err, __LINE__, stderr);

   fprintf(F, "# theta[arcmin] xi-^{mn}(theta)\n#        ");
   for (i_bin=0; i_bin<Nzbin; i_bin++) {
      for (j_bin=i_bin; j_bin<Nzbin; j_bin++) {
	 if (model->tomo==tomo_auto_only && i_bin!=j_bin) continue;
	 if (model->tomo==tomo_cross_only && i_bin==j_bin) continue;
	 fprintf(F, "           %d%d", i_bin, j_bin);
      }
   }
   fprintf(F, "\n");

   for (theta=0.5*arcmin; theta<400*arcmin; theta*=1.1) {
      fprintf(F, "%9.3f", theta/arcmin);
      for (i_bin=0; i_bin<Nzbin; i_bin++) {
	 for (j_bin=i_bin; j_bin<Nzbin; j_bin++) {
	    if (model->tomo==tomo_auto_only && i_bin!=j_bin) continue;
	    if (model->tomo==tomo_cross_only && i_bin==j_bin) continue;
	    fprintf(F, " %e", xi(model, -1, theta, i_bin, j_bin, err));
	    quitOnError(*err, __LINE__, stderr);
	 }
      }
      fprintf(F, "\n");
   }
   fclose(F);


   /* Tophat shear variance */

   printf("Calculating tophat shear variance, writing file gammasqr\n");
   F = fopen("gammasqr", "w");
   dump_param_lens(model, F, 1, err);
   quitOnError(*err, __LINE__, stderr);

   fprintf(F, "# theta[arcmin] <|gamma|^2>^{mn}(theta)\n#        ");
   for (i_bin=0; i_bin<Nzbin; i_bin++) {
      for (j_bin=i_bin; j_bin<Nzbin; j_bin++) {
	 if (model->tomo==tomo_auto_only && i_bin!=j_bin) continue;
	 if (model->tomo==tomo_cross_only && i_bin==j_bin) continue;
	 fprintf(F, "           %d%d", i_bin, j_bin);
      }
   }
   fprintf(F, "\n");

   for (theta=1.0*arcmin; theta<200*arcmin; theta*=1.1) {
      fprintf(F, "%9.3f", theta/arcmin);
      for (i_bin=0; i_bin<Nzbin; i_bin++) {
	 for (j_bin=i_bin; j_bin<Nzbin; j_bin++) {
	    if (model->tomo==tomo_auto_only && i_bin!=j_bin) continue;
	    if (model->tomo==tomo_cross_only && i_bin==j_bin) continue;
	    fprintf(F, " %e", gamma2(model, theta, i_bin, j_bin, err));
	    quitOnError(*err, __LINE__, stderr);
	 }
      }
      fprintf(F, "\n");
   }
   fclose(F);


   /* Aperture mass variance */

   printf("Calculating aperture mass variance (polynomial filter), writing files mapsqr, mapsqr_gauss\n");
   F = fopen("mapsqr", "w");
   dump_param_lens(model, F, 1, err);
   quitOnError(*err, __LINE__, stderr);

   fprintf(F, "# theta[arcmin] <M_ap^2>^{mn}(theta){polynomial}\n#        ");
   for (i_bin=0; i_bin<Nzbin; i_bin++) {
      for (j_bin=i_bin; j_bin<Nzbin; j_bin++) {
	 if (model->tomo==tomo_auto_only && i_bin!=j_bin) continue;
	 if (model->tomo==tomo_cross_only && i_bin==j_bin) continue;
	 fprintf(F, "           %d%d", i_bin, j_bin);
      }
   }
   fprintf(F, "\n");

   for (theta=1.0*arcmin; theta<200.0*arcmin; theta*=1.1) {
      fprintf(F, "%9.3f", theta/arcmin);
      for (i_bin=0; i_bin<Nzbin; i_bin++) {
	 for (j_bin=i_bin; j_bin<Nzbin; j_bin++) {
	    if (model->tomo==tomo_auto_only && i_bin!=j_bin) continue;
	    if (model->tomo==tomo_cross_only && i_bin==j_bin) continue;
	    fprintf(F, " %e", map2_poly(model, theta, i_bin, j_bin, err));
	    quitOnError(*err, __LINE__, stderr);
	 }
      }
      fprintf(F, "\n");
   }
   fclose(F);

   F = fopen("mapsqr_gauss", "w");
   dump_param_lens(model, F, 1, err);
   quitOnError(*err, __LINE__, stderr);

   fprintf(F, "# theta[arcmin] <M_ap^2>^{mn}(theta){Gaussian}\n#        ");
   for (i_bin=0; i_bin<Nzbin; i_bin++) {
      for (j_bin=i_bin; j_bin<Nzbin; j_bin++) {
	 if (model->tomo==tomo_auto_only && i_bin!=j_bin) continue;
	 if (model->tomo==tomo_cross_only && i_bin==j_bin) continue;
	 fprintf(F, "           %d%d", i_bin, j_bin);
      }
   }
   fprintf(F, "\n");

   for (theta=1.0*arcmin; theta<200.0*arcmin; theta*=1.1) {
      fprintf(F, "%9.3f", theta/arcmin);
      for (i_bin=0; i_bin<Nzbin; i_bin++) {
	 for (j_bin=i_bin; j_bin<Nzbin; j_bin++) {
	    if (model->tomo==tomo_auto_only && i_bin!=j_bin) continue;
	    if (model->tomo==tomo_cross_only && i_bin==j_bin) continue;
	    fprintf(F, " %e", map2_gauss(model, theta, i_bin, j_bin, err));
	    quitOnError(*err, __LINE__, stderr);
	 }
      }
      fprintf(F, "\n");
   }
   fclose(F);


   printf("Done\n");

   free_parameters_lens(&model);

   return 0;
}
