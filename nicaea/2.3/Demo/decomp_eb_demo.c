/* ============================================================ *
 * decomp_eb_demo.c						*
 * Martin Kilbinger, Liping Fu 2009				*
 * ============================================================ */

#include <stdio.h>
#include <stdlib.h>

#include "errorlist.h"
#include "io.h"
#include "maths.h"
#include "cosmo.h"
#include "lensing.h"
#include "nofz.h"
#include "decomp_eb.h"


#define N 6
int main(int argc, char** argv)
{
   error *myerr = NULL, **err;
   double x, eta, R, tp, tm, Psi, rp, rm;
   const double *a;
   char choice;
   cosmo_lens *model;
   FILE *F;


   err = &myerr;

   printf("decomp_eb_demo (M. Kilbinger, L. Fu 2009)\n");

   printf("Choice of coefficients for filter function:\n");
   printf("  Covariance from CFHTLS Wide 3rd data release (Fu et al. 2008)\n");
   printf("     S/N, eta=1/50          [0]\n");
   printf("     FoM, eta=1/10          [1]\n");
   printf("     FoM, eta=1/50          [2]\n");
   printf("Choice? ");

   choice = getchar();
   switch (choice) {
      case '0' : a = a_FK10_SN;        eta = eta_FK10_SN; break;
      case '1' : a = a_FK10_FoM_eta10; eta = eta_FK10_FoM_eta10; break;
      case '2' : a = a_FK10_FoM_eta50; eta = eta_FK10_FoM_eta10; break;
      default  : assert(0);
   }

   R = (1.0+eta)/(1.0-eta);    /* FK09 (10) */

   printf("Writing filter functions T+, T- to file 'Tpm'\n");
   F = fopen_err("Tpm", "w", err);
   quitOnError(*err, __LINE__, stderr);
   for (x=-1.0; x<=1.0005; x+=0.005) {
      tp = Tp(x, a, N, cheby2, err);
      quitOnError(*err, __LINE__, stderr);
      tm = Tm(x, a, N, cheby2, R, err);
      quitOnError(*err, __LINE__, stderr);
      fprintf(F, "% f % f % f\n", x, tp, tm);
   }
   fclose(F);

   F = fopen_err("cosmo_lens.par", "r", err);
   quitOnError(*err, __LINE__, stderr);
   read_cosmological_parameters_lens(&model, F, err);
   quitOnError(*err, __LINE__, stderr);
   fclose(F);

   printf("Writing shear functions R_E, R_B to file 'REB'\n");
   F = fopen_err("REB", "w", err);
   quitOnError(*err, __LINE__, stderr);
   for (Psi=1.0*arcmin; Psi<=250*arcmin; Psi*=1.2) {
      rp = RR(model, Psi*eta, Psi, a, N, cheby2, +1, err);
      quitOnError(*err, __LINE__, stderr);
      rm = RR(model, Psi*eta, Psi, a, N, cheby2, -1, err);
      quitOnError(*err, __LINE__, stderr);
      fprintf(F, "% f % g % g\n", Psi/arcmin, 0.5*fabs(rp+rm), 0.5*fabs(rp-rm));
      fflush(F);
   }
   fclose(F);


   return 0;
}
