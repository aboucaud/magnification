/* ============================================================ *
 * hod.c							*
 * Martin Kilbinger, Henry J. McCracken 2008-2010		*
 * Refs:							*
 *   - Hamana et al 2004 (MNRAS 347, 813)			*
 *   - Zehavi et al. 2005					*
 *   - Zheng 2004 (ApJ 610, 61)					*
 *   - Brown et al. 2008 (ApJ 682, 937)			*
 *   - Berlind & Weinberg 2002 (ApJ 575, 587)			*
 * ============================================================ */


#include "hod.h"


// this is the observed number density of galaxies 
// weighted by the redshift distribution, as given in equation (2)
// in Ross, Brunner, Percival. 
double ngd_obs_weighted(cosmo_hm *self, double area, double ngal, error **err)
{ 
  double res,zmin,zmax,ans1,ans2; 
  double fobs;
  int n_bin = 0;

  zmin = get_zmin(self->redshift, n_bin);
  zmax = get_zmax(self->redshift, n_bin);
  // observed fraction of the sky
  fobs = area/(4.0*pi*(180.0/pi)*(180.0/pi));

  ans1 = sm2_qromberg(int_for_ngd_obs_weighted1, (void*)self, zmin, zmax, 1.e-3, err);
  forwardError(*err, __LINE__, 0);

  ans2 = sm2_qromberg(int_for_ngd_obs_weighted2, (void*)self, zmin, zmax, 1.e-3, err);
  forwardError(*err, __LINE__, 0);

  ans1 = ans1/(fobs*4.0*pi);
  res = ans1 / ans2 * ngal; 
  // factor ngal needed because we are using the normalised dndz


  return res;
}

/* this corresponds to the function in Ross et al.*/
double int_for_ngd_obs_weighted2(double z, void *intpar, error **err)

{ 
  double zfac; 
  int n_bin = 0;
  cosmo_hm *self;

  self = (cosmo_hm*)intpar;
  zfac = prob(self->redshift, z, n_bin, err);   forwardError(*err, __LINE__, 0.0);

  return zfac*zfac;
}

double int_for_ngd_obs_weighted1(double z, void *intpar, error **err)
{
  int wOmegar = 0;
  double x,zfac,a,E,ans;
  int n_bin =0;
  cosmo_hm *self;

  self = (cosmo_hm*)intpar;
  a    = 1.0/(1.0+z);
  E  = Esqr(self->cosmo, a, wOmegar, err);          forwardError(*err,__LINE__, 0.0);
  x  = w(self->cosmo, a, wOmegar, err);             forwardError(*err, __LINE__, 0.0);
  //Esqr(z) = [H(z)/H_0]^2; H(z)=sqrt(E)*H0; R_HUBBLE=c/H0;
  zfac = prob(self->redshift, z, n_bin, err);   forwardError(*err, __LINE__, -1);
  ans = zfac*zfac*zfac/x/x/R_HUBBLE*sqrt(E);

  return ans;
}

double n_gal(cosmo_hm *self, double a, error **err)
{
  double res; 
  cosmo_hmANDstuff2 intpar;

  intpar.self = self;
  intpar.a    = a;
  intpar.asymptotic = 0;

  res = sm2_qromberg(int_for_ngal, (void*)&intpar, logMmin, logMmax,
		     1.e-3, err);
  forwardError(*err, __LINE__, 0);

  return res;
}

/* integate over mass function to get total number of galaxies. */
double int_for_ngal(double logM, void *intpar, error **err)
{
  double Ng,M;
  double res,dndlnM; 
  cosmo_hmANDstuff2 *cANDs2_in;
  cosmo_hmANDstuff2 cANDs2;


  /* transfer parameters. */

  cANDs2_in = (cosmo_hmANDstuff2*)intpar;

  M = exp(logM);

  cANDs2.self = cANDs2_in->self;
  cANDs2.a    = cANDs2_in->a;
  cANDs2.asymptotic = cANDs2_in->asymptotic;

  dndlnM = dn_dlnM(M, (void*)&cANDs2, err);    forwardError(*err, __LINE__, 0);
  Ng     = Ngal_M(cANDs2_in->self,M,err);      forwardError(*err, __LINE__, 0);
  res    = dndlnM*Ng;

  //printf("int_for_ngal %g(%g)=%g*%g, a=%g\n", res, logM, dndlnM, Ng, cANDs2.a);

  return res; 
}

/* Number of galaxies per halo of mass M. */
double Ngal_M(cosmo_hm *self, double M, error **err)
{
  double res, Nc, Ns;

  switch (self->hod) {
     case hamana04 :
	if (M > self->M_min) {
	   res = pow(M/self->M1, self->alpha);
	   //printf("res M_min %g %g\n", res, self->M_min);
	} else {
	   res = 0;
	}
	break;

     case berwein02 : case berwein02_hexcl :
	Nc  = Ngal_central_M(self, M, err);        forwardError(*err, __LINE__, 0.0);
	Ns  = Ngal_satellite_M(self, M, Nc, err);  forwardError(*err, __LINE__, 0.0);
	res = Nc*(1.0 + Ns);
	break;

     default :
	*err = addErrorVA(ce_unknown, "Wrong HOD type %d", *err, __LINE__, self->hod);
	return 0.0;
  }

  return res; 
}

/* Number of central galaxies per halo of mass M, HOD type berwein02. *
 * See Zheng (2004) eq. (26), Brown et al. (2008) eq. (8) */
double Ngal_central_M(cosmo_hm *self, double M, error **err)
{
   double res;

   testErrorRetVA(self->hod!=berwein02 && self->hod!=berwein02_hexcl, ce_unknown,
		  "Wrong or not supported HOD type %d",
		  *err, __LINE__, 0.0, self->hod);

   res = 0.5*(1.0 + gsl_sf_erf(dlog(M/self->M_min)/self->sigma_log_M));

   return res; 
}

/* Number of satellite galaxies per halo of mass M, HOD type berwein02. *
 * See Brown et al. (2008) eq. (10) with M_0=M_min                      *
 * Input ncen>=0 is interpreted as (pre-calculated) Ngal_central.       */
double Ngal_satellite_M(cosmo_hm *self, double M, double ncen, error **err)
{
   double res, Ncentral, M0;

   testErrorRetVA(self->hod!=berwein02 && self->hod!=berwein02_hexcl, ce_unknown,
		  "Wrong or not supported HOD type %d",
		  *err, __LINE__, 0.0, self->hod);

   /* NEW: If self->M0<0, set it to M_min (e.g. Brown et al. 2008) */
   if (self->M0<0) {
      M0 = self->M_min;
   } else {
      M0 = self->M0;
   }

   if (M - M0<0) return 0.0;

   if (ncen<0) {
      Ncentral = Ngal_central_M(self, M, err);
      forwardError(*err, __LINE__, 0.0);
   } else {
      /* Pre-computed number of central galaxies */
      Ncentral = ncen;
   }

   // MKDEBUG
   res = Ncentral*pow((M - M0)/self->M1, self->alpha);

   //res = pow((M - M0)/self->M1, self->alpha);

   /* Zheng et al. (2005), no Ncentral */
   //res = pow((M - self->M_min)/self->M1, self->alpha);

   /* Blake et al. (2008): M0=0 */
   // res = pow(M/self->M1, self->alpha);

   return res;
}

/* This is the model from Bullock et al. */ 
double Ngal_M_pair(cosmo_hm *self, double M, error **err)
{
   double Ngal=0.0, res=0.0;

   testErrorRetVA(self->hod!=hamana04, ce_unknown, "Wrong or not supported HOD type %d",
		  *err, __LINE__, 0.0, self->hod);

   Ngal = Ngal_M(self,M,err);              forwardError(*err, __LINE__, 0.0);

   if (Ngal>0.25 && Ngal < 1.0) {
      res = Ngal*Ngal*log(4.0*Ngal)/log(4.0);
   } else if (Ngal>1.0) {
      res = Ngal*Ngal;
   } else if (Ngal<0.25) {
      res = 0.0;
   }
   
   return res;
}

/* Compute the number density of galaxies from the halo density *
 * E.g. Hamana et al. (2004) eq. (11)                           */
#define EPS 1.0e-3
double Ngal_den(cosmo_hm *self, error **err) {
  
  double ans1,ans2;
  double zmin,zmax;
  int n_bin;

  //fprintf(stderr, "Ngal_den start\n");

  if (self->ngd<0) {

     n_bin = 0;

     zmin = get_zmin(self->redshift, n_bin);
     zmax = get_zmax(self->redshift, n_bin);

     ans1 = sm2_qromberg(int_for_nden1,(void*)self,zmin,zmax,
			 EPS, err);
     forwardError(*err, __LINE__, 0.0);

     ans2 = sm2_qromberg(int_for_nden2,(void*)self,zmin,zmax,
			 EPS, err);
     forwardError(*err, __LINE__, 0.0);

     self->ngd = ans1/ans2;

     //fprintf(stderr, "Recalculating ngd = %g\n", self->ngd);
  }

  //fprintf(stderr, "Returning ngd = %g\n", self->ngd);
  return self->ngd;
}
#undef EPS

double int_for_nden1(double z, void *intpar, error **err)
{
  double vol,res,ngh;
  double a;
  cosmo_hm *self;
  double zfac;
  int n_bin;

  n_bin = 0;

  self = (cosmo_hm*)intpar;

  a    = 1.0/(1.0+z);
  zfac = prob(self->redshift, z, n_bin, err);   forwardError(*err, __LINE__, -1);
  vol  = dvdz(self->cosmo,a,err);               forwardError(*err, __LINE__, -1); 
  ngh  = n_gal(self,a,err);                     forwardError(*err, __LINE__, -1);
  res  = vol*zfac*ngh;

  return res;
}

/* Integral for comoving volume */
double int_for_nden2(double z, void *intpar, error **err)
{
  double vol,zfac,res,a;
  cosmo_hm *self;
  int n_bin;

  n_bin = 0;

  self = (cosmo_hm *)intpar;
  a    = 1.0/(1.0+z);
  zfac = prob(self->redshift, z, n_bin, err);  forwardError(*err, __LINE__, -1);
  vol  = dvdz(self->cosmo, a, err);            forwardError(*err, __LINE__, -1);
  res  = vol*zfac;

  //printf("int_for_nden2: %g(%g)=%g*%g\n", res, z, vol, zfac);

  return res;
}

/* Comoving volume weighted with redshift distribution */
double Vc(cosmo_hm *self, error **err)
{
  double ans2;
  double zmin,zmax;
  int n_bin;

  if (self->vc<0) {

     // MKDEBUG: TODO: more than one bin! */
     n_bin = 0;

     zmin = get_zmin(self->redshift, n_bin);
     zmax = get_zmax(self->redshift, n_bin);

     ans2 = sm2_qromberg(int_for_nden2, (void*)self, zmin, zmax, 1.e-3, err);
     forwardError(*err, __LINE__, 0);

     self->vc = ans2;

     //fprintf(stderr, "Recalculated vc = %g\n", self->vc);
  }

  return self->vc;
}

/* Satelite fraction (see, for example eq. (32) in Blake et al.)*/
double av_frsat(cosmo_hm *self, double a, error **err)
{
  double res1, res2; 
  double logMinMass;
  cosmo_hmANDstuff2 intpar;

  // MKDEBUG
  //logMinMass = logMmin;
  logMinMass = log(self->M_min);

  intpar.self = self;
  intpar.a    = a;
  intpar.asymptotic = 0;

  res1 = sm2_qromberg(int_for_av_frsat1, (void*)&intpar, logMinMass, logMmax,
		     1.e-3, err);
  forwardError(*err, __LINE__, 0.0);

  res2 = sm2_qromberg(int_for_av_frsat2, (void*)&intpar, logMinMass, logMmax,
		     1.e-3, err);
  forwardError(*err, __LINE__, 0.0);

  return (1.0-res1/res2);
}


double int_for_av_frsat1(double logM, void *intpar, error **err)
{
  double M;
  double res;
  double dndlnM;
  double Nc;
  cosmo_hmANDstuff2 *cANDs2_in;
  cosmo_hmANDstuff2 cANDs2;

  cANDs2_in = (cosmo_hmANDstuff2*)intpar;
  M         = exp(logM);
  
  cANDs2.self = cANDs2_in->self;
  cANDs2.a    = cANDs2_in->a;
  cANDs2.asymptotic = cANDs2_in->asymptotic;
  Nc  = Ngal_central_M(cANDs2_in->self, M, err);          forwardError(*err, __LINE__, 0.0);
  dndlnM = dn_dlnM(M, (void*)&cANDs2, err);    forwardError(*err, __LINE__, 0);
  //  printf ("%e %e\n", Ng, dndlnM);
  res    = dndlnM*Nc;
  return res;
}

double int_for_av_frsat2(double logM, void *intpar, error **err)
{
  double M;
  double res;
  double dndlnM;
  double Nc,Ns;
  cosmo_hmANDstuff2 *cANDs2_in;
  cosmo_hmANDstuff2 cANDs2;

  cANDs2_in = (cosmo_hmANDstuff2*)intpar;
  M         = exp(logM);
  
  cANDs2.self = cANDs2_in->self;
  cANDs2.a    = cANDs2_in->a;
  cANDs2.asymptotic = cANDs2_in->asymptotic;
  dndlnM = dn_dlnM(M, (void*)&cANDs2, err);    forwardError(*err, __LINE__, 0);
  Nc  = Ngal_central_M(cANDs2_in->self, M, err);          forwardError(*err, __LINE__, 0.0);
  Ns  = Ngal_satellite_M(cANDs2_in->self, M, Nc, err);    forwardError(*err, __LINE__, 0.0);
  res    = dndlnM*Nc*(1.0+Ns);
  return res;
}

/* Average number of galxies per halo */
double av_ngh(cosmo_hm *self, double a, error **err)
{
  double res1, res2; 
  double logMinMass, logMaxMass;
  cosmo_hmANDstuff2 intpar;

  // MKDEBUG
  logMinMass = log(self->M_min);
  //logMinMass = logMmin;
  logMaxMass = logMmax;

  intpar.self = self;
  intpar.a    = a;
  intpar.asymptotic = 0;

  res1 = sm2_qromberg(int_for_av_ngh1, (void*)&intpar, logMinMass, logMaxMass,
		     1.e-5, err);
  forwardError(*err, __LINE__, 0.0);

  res2 = sm2_qromberg(int_for_av_ngh2, (void*)&intpar, logMinMass, logMaxMass,
		     1.e-5, err);
  forwardError(*err, __LINE__, 0.0);

  return res1/res2;
}

/* Calculate average number of galaxies per halo */
double int_for_av_ngh1(double logM, void *intpar, error **err)
{
  double M;
  double res;
  double dndlnM;
  double Ng;
  cosmo_hmANDstuff2 *cANDs2_in;
  cosmo_hmANDstuff2 cANDs2;

  cANDs2_in = (cosmo_hmANDstuff2*)intpar;
  M         = exp(logM);
  
  cANDs2.self = cANDs2_in->self;
  cANDs2.a    = cANDs2_in->a;
  cANDs2.asymptotic = cANDs2_in->asymptotic;
  dndlnM = dn_dlnM(M, (void*)&cANDs2, err);    forwardError(*err, __LINE__, 0);
  Ng     = Ngal_M(cANDs2_in->self,M,err);      forwardError(*err, __LINE__, 0);
  //  printf ("%e %e\n", Ng, dndlnM);
  res    = dndlnM*Ng;
  return res;
}

/* Calculate average number of galaxies per halo */
double int_for_av_ngh2(double logM, void *intpar, error **err)
{
  double M;
  double res;
  double dndlnM;
  cosmo_hmANDstuff2 *cANDs2_in;
  cosmo_hmANDstuff2 cANDs2;

  cANDs2_in = (cosmo_hmANDstuff2*)intpar;
  M         = exp(logM);
  
  cANDs2.self = cANDs2_in->self;
  cANDs2.a    = cANDs2_in->a;
  cANDs2.asymptotic = cANDs2_in->asymptotic;
  dndlnM = dn_dlnM(M, (void*)&cANDs2, err);    forwardError(*err, __LINE__, 0);
  res    = dndlnM;

  return res;
}

/* Calculate galaxy number-weighted bias factor */
double av_gal_bias(cosmo_hm *self, double a, error **err)
{
  double res1,res2; 
  cosmo_hmANDstuff2 intpar;
  double logMinMass;

  intpar.self = self;
  intpar.a    = a;
  intpar.asymptotic = 0;

  // MKDEBUG
  logMinMass = log(self->M_min);
  //logMinMass = logMmin;

  /* compute the average mass of a halo.*/
  
  res1 = sm2_qromberg(int_for_galbias1, (void*)&intpar, logMinMass, logMmax,
		      1.e-3, err);
  forwardError(*err, __LINE__, 0);

  res2 = sm2_qromberg(int_for_avhalo2, (void*)&intpar, logMinMass, logMmax,
		      1.e-3, err);
  forwardError(*err, __LINE__, 0);

  return res1/res2;
}

/* Calculate average halo mass */
double int_for_galbias1(double logM, void *intpar, error **err)
{
  double M,b;
  double res;
  double dndlnM;
  double Ng;
  cosmo_hmANDstuff2 *cANDs2_in;
  cosmo_hmANDstuff2 cANDs2;

  cANDs2_in = (cosmo_hmANDstuff2*)intpar;
  M         = exp(logM);
  
  cANDs2.self = cANDs2_in->self;
  cANDs2.a    = cANDs2_in->a;
  cANDs2.asymptotic = cANDs2_in->asymptotic;
  dndlnM = dn_dlnM(M, (void*)&cANDs2, err);            forwardError(*err, __LINE__, 0);
  Ng     = Ngal_M(cANDs2_in->self,M,err);              forwardError(*err, __LINE__, 0);
  b      = bias(cANDs2_in->self,M,cANDs2_in->a,1,err); forwardError(*err, __LINE__, 0); 
  res    = dndlnM*Ng*b;

  return res;
}

/* Average halo mass */
double av_halo_mass(cosmo_hm *self, double a, error **err)
{
  double res1, res2, logMinMass;
  cosmo_hmANDstuff2 intpar;

  intpar.self = self;
  intpar.a    = a;
  intpar.asymptotic = 0;

  // MKDEBUG
  logMinMass = log(self->M_min);
  //logMinMass = logMmin;

  /* compute the average mass of a halo.*/
  
  res1 = sm2_qromberg(int_for_avhalo1,(void*)&intpar, logMinMass, logMmax,
		      1.e-3, err);
  forwardError(*err, __LINE__, 0);

  res2 = sm2_qromberg(int_for_avhalo2,(void*)&intpar, logMinMass, logMmax,
		      1.e-3, err);
  forwardError(*err, __LINE__, 0);

  return res1/res2;
}


/* Integrand (nominator) for average halo mass */
double int_for_avhalo1(double logM, void *intpar, error **err)
{
  double M;
  double res;
  double dndlnM;
  double Ng;
  cosmo_hmANDstuff2 *cANDs2_in;
  cosmo_hmANDstuff2 cANDs2;

  cANDs2_in = (cosmo_hmANDstuff2*)intpar;
  M         = exp(logM);
  
  cANDs2.self = cANDs2_in->self;
  cANDs2.a    = cANDs2_in->a;
  cANDs2.asymptotic = cANDs2_in->asymptotic;
  dndlnM = dn_dlnM(M, (void*)&cANDs2, err);    forwardError(*err, __LINE__, 0);
  Ng     = Ngal_M(cANDs2_in->self,M,err);   forwardError(*err, __LINE__, 0);
  res    = dndlnM*Ng*M;

  return res;
}
  
/* Integrand (denominator) for average halo mass */
double int_for_avhalo2(double logM, void *intpar, error **err)
{
  double M;
  double res,Ng;
  double dndlnM;
  cosmo_hmANDstuff2 *cANDs2_in;
  cosmo_hmANDstuff2 cANDs2;

  cANDs2_in = (cosmo_hmANDstuff2*)intpar;
  M         = exp(logM);
  
  cANDs2.self = cANDs2_in->self;
  cANDs2.a    = cANDs2_in->a;
  cANDs2.asymptotic = cANDs2_in->asymptotic;

  Ng     = Ngal_M(cANDs2_in->self,M,err);      forwardError(*err, __LINE__, 0);
  dndlnM = dn_dlnM(M, (void*)&cANDs2, err);    forwardError(*err, __LINE__, 0);
  res    = dndlnM*Ng;

  return res;
}

/* Real-space satelite-central correlation function.             *
 * Lower mass is virial mass corresponding to halo of size r.    */
#define NM 25
double xi1_cs(cosmo_hm *self, double a, double r, error **err)
{
   double val,Ngalh; 
   double logMinMass, logMaxMass, dlogM, logM;
   double rhobar;
   double Dvir;
   cosmo_hmANDhjmcc2 intpar; 

  // variables for integration 
  intpar.r    = r; 
  intpar.a    = a; 
  intpar.self = self;

  /* compute the mass corresponding the to the virial radius r */
  rhobar     = rho_c0*self->cosmo->Omega_m;

  /* 200 as e.g. in the Tinker and Blake papers */
  //Dvir       = Delta_vir(self, a);
  Dvir       = 200.0;

  logMinMass = log(4.0/3.0*r*r*r*pi*rhobar*Dvir);
  logMaxMass = logMmax;

  dlogM = (logMaxMass - logMinMass)/((double)NM-1.0);
  for (logM=logMinMass,val=0.0; logM<logMaxMass-dlogM/10.0; logM+=dlogM) {
     val += sm2_qromberg(int_for_xi1_cs, (void*)&intpar, logM, logM+dlogM, 1.e-3, err);
  }

  Ngalh = n_gal(self, a, err);       forwardError(*err, __LINE__,0);
  val   = val/(Ngalh*Ngalh/2.0);

  return val;

}
#undef NM

#define EPS 1.0e-6
double int_for_xi1_cs(double logM, void *intpar,  error **err)
{
  double val;
  double a,r,c; 
  cosmo_hm *self; 
  double dndlnM, Nc, Ns,M;
  double rho, r_vir, Dvir;
  double rho_norm;
  cosmo_hmANDhjmcc2 *cANDs;
  cosmo_hmANDstuff2 cANDs2;

  /* Get the extra variables */
  cANDs = (cosmo_hmANDhjmcc2*)intpar;
  self  = cANDs->self; 
  a     = cANDs->a; 
  r     = cANDs->r; 

  /* For the mass function */
  cANDs2.self = self;
  cANDs2.a    = a;
  cANDs2.asymptotic = 0;
  M      = exp(logM);
  dndlnM = dn_dlnM(M,(void*)&cANDs2, err);        forwardError(*err, __LINE__, 0);
  c      = concentration(self, M, a, err);        forwardError(*err, __LINE__, 0);

  r_vir = 0.0; /* To recalculate r_vir in rsqr_rho_halo */
  rho   = rsqr_rho_halo(self, r, M, a, r_vir, err)/(r*r);

  /* Normalisation for NFW halo */
  testErrorRetVA(fabs(self->alpha_NFW-1.0)>EPS, hm_nfw,
		 "alpha_NFW is %g, normalisation for non-NFW (alpha_NFW=1) not implemented yet",
		 *err, __LINE__, 0.0, self->alpha_NFW);

  Dvir     = 200.0;
  rho_norm = rho_c0*self->cosmo->Omega_m * Dvir *c*c*c / 3.0 / (log(1.0+c)-c/(1.0+c));
  rho      = rho*rho_norm;

  /* Compute central and satellite term */
  Nc  = Ngal_central_M(self, M, err);             forwardError(*err, __LINE__, 0.0);
  Ns  = Ngal_satellite_M(self, M, Nc, err);       forwardError(*err, __LINE__, 0.0);
  val = Nc*Ns*rho*dndlnM/M;

  return val; 
}
#undef EPS

/* ============================================================ *
 * Satellite-satellite real-space correlation function.		*
 * integrate the power spectrum over all k.			*
 * Baustelle!! (under construction)				*
 * ============================================================ */
#define EPS 1.0e-8
double xi1_ss(cosmo_hm *self, double a, double r, error **err)
{
  double val; 

  cosmo_hmANDhjmcc2err intpar;

  intpar.a = a;
  intpar.r = r; 
  intpar.self = self; 

  testErrorRetVA(r<EPS, math_infnan, "Division by zero (r=%g)", *err, __LINE__, 0.0, r);

  gsl_integration_qawo_table *table;
  gsl_function xi1_ss_qawo;
  gsl_integration_workspace *ws;
  double abserr, epsabs, epsrel;

  table = gsl_integration_qawo_table_alloc(1, (k_max-k_min)*r, GSL_INTEG_SINE, 10000);
  ws    = gsl_integration_workspace_alloc(1000);

  epsabs = 0.02;
  epsrel = 0.02;
  xi1_ss_qawo.function = &int_xi1_ss_qawo;
  intpar.err = err;
  xi1_ss_qawo.params = (void*)&intpar;
  gsl_integration_qawo(&xi1_ss_qawo, k_min*r, epsabs, epsrel, 1000, ws, table, &val, &abserr);

  gsl_integration_qawo_table_free(table);
  gsl_integration_workspace_free(ws);

  /* this is the integral of the power spectrum by hand */
  /*
  k   = k_min;
  val = 0.0;
  dk  = pi/r/2.0/20;
  while (k+dk<=k_max) {
     val += int_for_xi1_ss(k, (void*)&intpar, err);
     forwardError(*err, __LINE__, 0.0);
     k   += dk;
  }
  val = val*dk;
  */

  val /= 2.0*pi*pi;

  return val;
}
#undef Nk
#undef EPS

double int_xi1_ss_qawo(double x, void *params)
{
  cosmo_hmANDhjmcc2err *cANDs;
  cosmo_hm *self;
  double a, r, val, k;
  error **err;

  cANDs = (cosmo_hmANDhjmcc2err *)params;
  a     = cANDs->a;
  r     = cANDs->r;
  self  = cANDs->self;
  err   = cANDs->err;
  k     = x/r;

  val  = x/r/r/r;
  val *= P1h_g_ss(self, a, k, err);
  forwardError(*err, __LINE__, 0.0);

  return val;
}

double int_for_xi1_ss(double k, void *intpar,  error **err)
{
  double val; 
  cosmo_hmANDhjmcc2 *cANDs;
  cosmo_hm *self; 
  double a, r;

  cANDs = (cosmo_hmANDhjmcc2 *)intpar;
  a     = cANDs->a;
  r     = cANDs->r;
  self  = cANDs->self; 

  /* Depending on how this routine is called, multiply with another k for log-integration! */
  val   = k*k*sin(k*r)/(k*r);
  val  *= P1h_g_ss(self,a,k,err); forwardError(*err, __LINE__, 0.0);

  return val; 
}

/* This function probably does not work correctly */
#define N_FFT 4096
double xi1_ss_FT(cosmo_hm *self, double a, double r, error **err)
{
   fftw_plan plan;
   double k, dk, dr, drX, res;
   fftw_complex *P, *f_P;
   int i;
   static interTable *xiss = NULL;

   if (xiss==NULL) {

      dr = 0.005;
      dk = 2.0*pi/dr/(double)N_FFT;

      /* Factor pi looks nice, but why? why?? */
      drX = dr*pi;

      xiss = init_interTable(N_FFT, 0.0, N_FFT*drX, drX, 0.0, 0.0, err);
      forwardError(*err, __LINE__, 0.0);

      P    = fftw_malloc(N_FFT*sizeof(fftw_complex));
      f_P  = fftw_malloc(N_FFT*sizeof(fftw_complex));
      plan = fftw_plan_dft_1d(N_FFT, P, f_P, FFTW_BACKWARD, FFTW_ESTIMATE);

      /* Tabulate k^*P(k) */
      for (i=0; i<N_FFT; i++) {
	 if (i<N_FFT/2.0) k = i*dk;
	 else k = (i-N_FFT)*dk;
	 //k = abs(k);
	 if (k>0) {
	    P[i][0] = k*k*P1h_g_ss(self,a,k,err);
	    forwardError(*err, __LINE__, 0.0);
	 } else {
	    P[i][0] = 0.0;
	 }
	 P[i][1] = 0.0;
      }

      /* FT power spectrum */
      fftw_execute(plan);

      for (i=0; i<N_FFT; i++) {
	 f_P[i][0] /= N_FFT*(2*pi*2*pi);
	 f_P[i][1] /= N_FFT*(2*pi*2*pi);
	 xiss->table[i] = f_P[i][0];
      }

      fftw_free(P);
      fftw_free(f_P);
      fftw_destroy_plan(plan);
   }

   res = interpol_wr(xiss, r, err);
   forwardError(*err, __LINE__, 0.0);

   return res;
}

/* ============================================================ *
 * Real-space correlation function for the two-halo term.	*
 * ============================================================ */
#define EPS 1.0e-4
double xi2(cosmo_hm *self, double a, double r, error **err)
{
  double val; 
  cosmo_hmANDhjmcc2err intpar;
  double ng, ngp, Mlim, xi;

  if (r<RMIN2) return 0.0;

  intpar.a = a;
  intpar.r = r; 
  intpar.self = self; 

  testErrorRetVA(r<EPS, math_infnan, "Division by zero (r=%g)", *err, __LINE__, 0.0, r);

  /* GSL integration */
  gsl_integration_qawo_table *table;
  gsl_function xi2_qawo;
  gsl_integration_workspace *ws;
  double abserr, epsabs, epsrel;

  table = gsl_integration_qawo_table_alloc(1, (k_max-k_min)*r, GSL_INTEG_SINE, 1000);
  ws    = gsl_integration_workspace_alloc (1000);

  epsabs = 0.001;
  epsrel = 0.001;
  xi2_qawo.function = &int_xi2_qawo;
  intpar.err = err;
  xi2_qawo.params = (void*)&intpar;
  gsl_integration_qawo(&xi2_qawo, k_min*r, epsabs, epsrel, 1000, ws, table, &val, &abserr);

  gsl_integration_qawo_table_free(table);
  gsl_integration_workspace_free(ws);

  /* Integral over the two-halo power-spectrum by hand */
  /*
  double k, dk;
  k   = k_min;
  val = 0.0;
  dk  = pi/r/2.0/5;
    while (k+dk<=k_max) {
     val += int_for_xi2g(k, (void*)&intpar, err);
     forwardError(*err, __LINE__, 0.0);
     k   += dk;
   }
   val = val*dk;
  */


  val /= 2.0*pi*pi;

  if (self->hod==berwein02_hexcl) {
     /* Halo exclusion: Tinker et al. (2005), eq. (B9) */
     ng   = n_gal(self, a, err);                       forwardError(*err, __LINE__, 0.0);
     Mlim = M_lim(self, r, a, err);                    forwardError(*err, __LINE__, 0.0);
     ngp  = n_gal_hexcl(self, a, log(Mlim), err);      forwardError(*err, __LINE__, 0.0);

     //xi = dsqr(ngp/ng)*(1.0+val) - 1.0;
     xi   = dsqr(ngp/ng)*val;
     val = xi;
  }

  return val;
}
#undef EPS

double int_xi2_qawo(double x, void *params)
{
  cosmo_hmANDhjmcc2err *cANDs;
  cosmo_hm *self;
  double a, r, val, k;
  error **err;

  cANDs = (cosmo_hmANDhjmcc2err *)params;
  a     = cANDs->a;
  r     = cANDs->r;
  self  = cANDs->self;
  err   = cANDs->err;
  k     = x/r;

  val   = x/r/r/r;

  switch (self->hod) {
     case berwein02_hexcl :
	val   *= P2h_g_hexcl(self, a, k, r, err);
	break;
     default :
	val  *= P2h_g(self, a, k, err);
	break;
  }
  forwardError(*err, __LINE__, 0.0);

  return val;
}

#define EPS 1.0e-8
double int_for_xi2g(double k, void *intpar, error **err)
{
  double val; 
  cosmo_hmANDhjmcc2 *cANDs;
  cosmo_hm *self; 
  double a, r;

  cANDs = (cosmo_hmANDhjmcc2 *)intpar;
  a     = cANDs->a;
  r     = cANDs->r;
  self  = cANDs->self; 

  if (r<EPS) {
     val = 0.0;
  } else {
     val   = k*k*sin(k*r)/(k*r);
  }
  
  switch (self->hod) {
     case berwein02_hexcl :
	val   *= P2h_g_hexcl(self, a, k, r, err);
	break;
     default :
	val  *= P2h_g(self,a,k,err);
	break;
  }
  forwardError(*err, __LINE__, 0.0);

  return val; 
}
#undef EPS

/* ============================================================ *
 * Real-space galaxy correlation function, for given scale      *
 * factor a.							*
 * ============================================================ */
#define EPS 1.0e-5
double xir_g(cosmo_hm *self, double a, double r, pofk_t pofk_xir, error **err)
{
   double dlogr, logr, rr, res;
   int j;

   testErrorRetVA(self->hod!=berwein02 && self->hod!=berwein02_hexcl, hm_hodtype,
		  "Invalid hod type (%d), only berwein02(%d) hod implemented for xi(r)",
		  *err, __LINE__, 0.0, self->hod, berwein02);

   testErrorRetVA(pofk_xir!=p1hgcs && pofk_xir!=p1hgss && pofk_xir!=p2hg && pofk_xir!=p1hgcs+p1hgss &&
		  pofk_xir!=p1hgcs+p2hg && pofk_xir!=p1hgss+p2hg && pofk_xir!=p1hgcs+p1hgss+p2hg, hm_pofk,
       "Wrong pofk type (%d), has to be p1gcs(%d), p1hgss(%d), p2hg(%d) or a bit-wise combination thereof",
		  *err, __LINE__, 0.0, pofk_xir, p1hgcs, p1hgss, p2hg);

   if (self->pofk_xir!=pofk_xir) {
      if (self->xir!=NULL) {
	 del_interTable(&self->xir);
	 self->pofk_xir = pofk_undef;
      }
   }

   /* Delete previous table if calculated for different scale factor a */
   if (fabs(self->a_xir-a)>EPS) {
      del_interTable(&self->xir);
   }

   if (self->xir==NULL) {

      dlogr       = (log(RMAX)-log(RMIN))/((double)NR-1.0);
      self->xir   = init_interTable(NR, log(RMIN), log(RMAX), dlogr, 0.0, 0.0, err);
      forwardError(*err, __LINE__, 0.0);
      self->a_xir = a;
      self->pofk_xir = pofk_xir;

      /* Earlier: Tabulation in a as well */

      for (j=0,logr=log(RMIN); j<NR; j++,logr+=dlogr) {
	 rr = exp(logr);

	 res = 0.0;
	 if (rr<RMAX1) {
	    /* 1h on small scales */
	    if (self->pofk_xir&p1hgcs) {
	       res += xi1_cs(self, a, rr, err);   forwardError(*err, __LINE__, 0.0);
	    }
	    if (self->pofk_xir&p1hgss) {
	       /* This is slow */
	       res += xi1_ss(self, a, rr, err);   forwardError(*err, __LINE__, 0.0);
	    }
	 }
	 if (rr>RMIN2) {
	    /* 2h on large scales */
	    if (self->pofk_xir&p2hg) {
	       /* This is slow */
	       res += xi2(self, a, rr, err);      forwardError(*err, __LINE__, 0.0);
	    }
	 }
	 self->xir->table[j] = res;

	 //fprintf(stderr, "."); fflush(stderr);
      }

   }

   if (r<RMIN || r>RMAX) return 0.0;

   logr = log(r);

   res = interpol_wr(self->xir, logr, err);
   forwardError(*err, __LINE__, 0.0);

   return res;
}
#undef EPS

/* ============================================================ *
 * 1-halo satellite-satellite term (for berwein02 HOD model).   *
 * ============================================================ */
double P1h_g_ss(cosmo_hm *self, double a, double k, error **err) 
{
  double val,Ngalh, logMinMass, logMaxMass; 
  cosmo_hmANDhjmcc intpar;

  intpar.self = self;
  intpar.a = a;
  intpar.k = k;
  Ngalh    = n_gal(self, a, err);       forwardError(*err, __LINE__,0);

  logMinMass  = logMmin;
  logMaxMass  = logMmax;

  val = 1.0/(Ngalh*Ngalh)*
    sm2_qromberg(int_for_P1h_g_ss, (void*)&intpar, logMinMass, logMaxMass, 1.e-4, err);
  // MKDEBUG: was 1e-3
  forwardError(*err, __LINE__, 0.0);

  return val;
}

double int_for_P1h_g_ss(double logM, void *intpar, error **err)
{
  double a, dndlnM, rhohat, M, k, res;
  cosmo_hmANDhjmcc *cANDs;
  cosmo_hmANDstuff2 cANDs2;
  cosmo_hm *self;
  double Nc, Ns;

  cANDs = (cosmo_hmANDhjmcc*)intpar;
  self  = cANDs->self;
  a     = cANDs->a;
  k     = cANDs->k;
  M     = exp(logM);
  cANDs2.self = self;
  cANDs2.a    = a;
  cANDs2.asymptotic = 0;
  dndlnM  = dn_dlnM(M,(void*)&cANDs2, err);             forwardError(*err, __LINE__, 0.0);
  rhohat  = rhohat_halo(self, k, M, a, 1, err);         forwardError(*err, __LINE__, 0.0);

  switch (self->hod) {

     case berwein02 : case berwein02_hexcl :
	Nc  = Ngal_central_M(self, M, err);             forwardError(*err, __LINE__, 0.0);
	Ns  = Ngal_satellite_M(self, M, Nc, err);       forwardError(*err, __LINE__, 0.0);
	res = dndlnM*Nc*Ns*Ns*rhohat*rhohat;
	break;

     default :
	*err = addErrorVA(ce_unknown, "Wrong or not supported HOD type %d", *err, __LINE__, self->hod);
	return 0.0;
  }

  return res;
}
#undef EPS

/* The following flag is used in P1h_g and P2h_g */
#define INTERP 1

/* 1-halo term of galaxy power spectrum, k in h/Mpc */
double P1h_g(cosmo_hm *self, double a, double k, error **err)
{
   double dlogk, logk, da, aa, kk, val, logkmin, logkmax;
   int i, j;
   double Ngalh;
   cosmo_hmANDhjmcc intpar;
   double logMinMass, logMaxMass;   

   if (INTERP && self->P1hg==NULL) {

      logkmin = log(k_min);
      logkmax = log(k_max);
      dlogk   = (logkmax - logkmin)/(N_k-1.0);
      da      = (1.0-self->cosmo->a_min)/(Na_hm-1.0);

      /* MK: TODO upper extrapolation index ??? */
      self->P1hg = init_interTable2D(Na_hm, self->cosmo->a_min, 1.0, da,
				     N_k, logkmin, logkmax, dlogk, self->cosmo->n_spec, -3.0, err);
      forwardError(*err, __LINE__, 0.0);

      intpar.self = self;
      logMinMass  = logMmin;
      logMaxMass  = logMmax;

      for (i=0,aa=self->cosmo->a_min; i<Na_hm; i++,aa+=da) {
	 intpar.a = aa;
	 Ngalh    = n_gal(self, aa, err);       forwardError(*err, __LINE__,0);
	 for (j=0,logk=logkmin; j<N_k; j++,logk+=dlogk) {
	    kk = exp(logk);
	    intpar.k = kk;

	    val = 1.0/(Ngalh*Ngalh)*
	      sm2_qromberg(int_for_P1h_g, (void*)&intpar, logMinMass, logMaxMass, 1.e-3, err);
	    forwardError(*err, __LINE__, 0);
	    self->P1hg->table[i][j] = log(val);
	 }
      }
   }

   logk = log(k);

   if (INTERP && logk<self->P1hg->b2) {
      /* TODO: extrapolation */
      val = interpol2D(self->P1hg, a, logk, err);            forwardError(*err, __LINE__, 0);
      val = exp(val);
   } else {
      Ngalh       = n_gal(self, a, err);       forwardError(*err, __LINE__,0);
      intpar.self = self;
      intpar.a    = a;
      intpar.k    = k;
      logMinMass  = logMmin;
      logMaxMass  = logMmax;
      val = 1.0/(Ngalh*Ngalh)*
	sm2_qromberg(int_for_P1h_g, (void*)&intpar, logMinMass, logMaxMass, 1.e-3, err);
      forwardError(*err, __LINE__, 0);
   }

   return val;
}

#define EPS 1.0e-12
double int_for_P1h_g(double logM, void *intpar, error **err)
{
  double a, dndlnM, rhohat, M, k, res;
  cosmo_hmANDhjmcc *cANDs;
  cosmo_hmANDstuff2 cANDs2;
  cosmo_hm *self;
  double NgalM, Nc, Ns;

  cANDs = (cosmo_hmANDhjmcc*)intpar;
  self  = cANDs->self;
  a     = cANDs->a;
  k     = cANDs->k;
  M     = exp(logM);
  cANDs2.self = self;
  cANDs2.a    = a;
  cANDs2.asymptotic = 0;
  dndlnM = dn_dlnM(M,(void*)&cANDs2, err);             forwardError(*err, __LINE__, 0);
  rhohat = fabs(rhohat_halo(self, k, M, a, 1, err));   forwardError(*err, __LINE__, 0);

  switch (self->hod) {
     case hamana04 :
	NgalM  = Ngal_M_pair(self, M, err);            forwardError(*err, __LINE__, 0);
	if (NgalM>1.0) {
	   rhohat *= rhohat;
	}
	res = dndlnM*NgalM*rhohat;
	break;

     case berwein02 : case berwein02_hexcl :
	Nc  = Ngal_central_M(self, M, err);             forwardError(*err, __LINE__, 0.0);
	Ns  = Ngal_satellite_M(self, M, Nc, err);       forwardError(*err, __LINE__, 0.0);
	/* Blake et al. (2008) eqs. (7) [with lower integration boundary=0], (9) */
	res = dndlnM*(2.0*Nc*Ns*rhohat + 0*Nc*Ns*Ns*rhohat*rhohat);
	break;

     default :
	*err = addErrorVA(ce_unknown, "Wrong or not supported HOD type %d", *err, __LINE__, self->hod);
	return 0.0;
  }

  return res;
}
#undef EPS

/* this is the simplified (non-generalised) galaxy one-halo term. */ 
/* 2-halo term of power spectrum, k in h/Mpc */

#define EPS 1.0e-3
double P2h_g(cosmo_hm *self, double a, double k, error **err)
{
   double dlogk, logk, da, aa, kk, val, logkmin, logkmax;
   int i, j;
   double Ngal;
   cosmo_hmANDhjmcc intpar;
   double logMinMass, logMaxMass;

   testErrorRetVA(self->hod==berwein02_hexcl, hm_hodtype, "Wrong hod type %s. Use P2h_g_hexcl for 2-halo term",
		  *err, __LINE__, 0.0, shod_t(berwein02_hexcl));

   if (INTERP && self->P2hg==NULL) {

      logkmin = log(k_min);
      logkmax = log(k_max);
      dlogk   = (logkmax - logkmin)/(N_k-1.0);
      da      = (1.0-self->cosmo->a_min)/(Na_hm-1.0);

      /* TODO: extrapolation! */
      self->P2hg = init_interTable2D(Na_hm, self->cosmo->a_min, 1.0, da,
				     N_k, logkmin, logkmax, dlogk, self->cosmo->n_spec, 0.0, err);
      forwardError(*err, __LINE__, 0.0);

      intpar.self = self;

      for (i=0,aa=self->cosmo->a_min; i<Na_hm; i++,aa+=da) {
	 intpar.a = aa;
	 for (j=0,logk=logkmin; j<N_k; j++,logk+=dlogk) {
	    kk = exp(logk);
	    intpar.k = kk;

	    Ngal       = n_gal(self,aa,err);          forwardError(*err, __LINE__, 0);
	    logMinMass = logMmin;
	    logMaxMass = logMmax;

	    val = dsqr(1.0/Ngal*sm2_qromberg(int_for_P2h_g, (void*)&intpar, logMinMass,
					     logMaxMass, EPS, err)); 
	    forwardError(*err, __LINE__, 0);
	    val *= P_L(self->cosmo, aa, kk, err);         forwardError(*err, __LINE__, 0);
	    // This is the line to change to go to the non-linear power spectrum 
	    self->P2hg->table[i][j] = log(val);
	 }
      }
   }

   logk = log(k);

   if (INTERP && logk>self->P2hg->a2 && logk<self->P2hg->b2) {
      val = interpol2D(self->P2hg, a, logk, err);            forwardError(*err, __LINE__, 0);
      val = exp(val);
   } else {
      intpar.self = self;
      intpar.a    = a;
      intpar.k    = k;
      Ngal       = n_gal(self, a, err);          forwardError(*err, __LINE__, 0);
      logMinMass = logMmin;
      logMaxMass = logMmax;
      val = dsqr(1.0/Ngal*sm2_qromberg(int_for_P2h_g, (void*)&intpar, logMinMass,
				       logMaxMass, EPS, err)); 
      forwardError(*err, __LINE__, 0);
      val *= P_L(self->cosmo, a, k, err);         forwardError(*err, __LINE__, 0);
   }

   return val;

   /*   p2h_hjmcc =dsqr(av_gal_bias(self,a,err))*P_L(self->cosmo, a, k, err); */
   /* the two-halo term should approximated by this at large scales. */
}
#undef EPS
#undef INTERP

double int_for_P2h_g(double logM, void *intpar, error **err)
{
  double a, b, k, dndlnM, rhohat, M, res, r;
  cosmo_hmANDhjmcc3 *cANDs;
  cosmo_hmANDstuff2 cANDs2;
  cosmo_hm *self;
  double NgalM;
  
  cANDs = (cosmo_hmANDhjmcc3*)intpar;
  self  = cANDs->self;
  a     = cANDs->a;
  k     = cANDs->k;
  M     = exp(logM);

  /* Tinker et al. (2005) bias for halo-exclusion model */

  switch (self->hod) {
     case berwein02_hexcl :
	r     = cANDs->r;
 	b = sqrt(bsq_halo_tinker(self,a,r,M,err));
	//b = bias(self, M, a, 1, err);
	forwardError(*err, __LINE__, 0);
	// MKDEBUG. Normalise bias??!!
	//b /= bias_norm(self, a, err);
	//forwardError(*err, __LINE__, 0);
	break;
     default :
	b = bias(self, M, a, 1, err);
	forwardError(*err, __LINE__, 0);       
	break;
  }

  cANDs2.self = self;
  cANDs2.a    = a;
  cANDs2.asymptotic = 0;

  dndlnM = dn_dlnM(M, (void*)&cANDs2, err);      forwardError(*err, __LINE__, 0);
  rhohat = rhohat_halo(self,k, M, a, 1, err);    forwardError(*err, __LINE__, 0);
  NgalM  = Ngal_M(self, M, err);                 forwardError(*err, __LINE__, 0);
  res    = dndlnM*b*rhohat*NgalM;

  return res;
}

/* 1h+2h=total galaxy power spectrum, k in h/Mpc */
#define eps_a  1.0e-5
double Pth_g(cosmo_hm *self, double a, double k, error **err)
{
   double res;

   res  = P1h_g(self, a, k, err);   forwardError(*err, __LINE__, 0);
   res += P2h_g(self, a, k, err);   forwardError(*err, __LINE__, 0);

   return res;
}

/* ============================================================ *
 * Restricted number of galaxies, exclusion effect for triaxial *
 * halos. Blake et al. 2009 (eq. 14), Tinker et al. (2005).	*
 * ============================================================ */
#define NM 20
double n_gal_hexcl_triaxial(cosmo_hm *self, double r, double a, error **err)
{
   double M1, M2, logM1, logM2, R1, R2, x, y, P, n, res1, res2, rhobar, Dvir,
     dlogM;
   cosmo_hmANDstuff2 cANDs2;

   rhobar = rho_c0*self->cosmo->Omega_m;
   Dvir   = 200.0;

   cANDs2.self       = self;
   cANDs2.a          = a;
   cANDs2.asymptotic = 0;

   dlogM = (logMmax-logMmin)/((double)NM-1.0);
   for (logM1=logMmin,n=0.0; logM1<logMmax; logM1+=dlogM) {
      M1 = exp(logM1);
      R1 = cbrt(3.0*M1/(4.0*pi*rhobar*Dvir));

      res1 = dn_dlnM(M1, (void*)&cANDs2, err);
      forwardError(*err, __LINE__, 0);

      res1 *= Ngal_M(self, M1, err);
      forwardError(*err, __LINE__, 0);

      for (logM2=logMmin; logM2<logMmax; logM2+=dlogM) {
	 M2 = exp(logM2);
	 R2 = cbrt(3.0*M2/(4.0*pi*rhobar*Dvir));

	 res2 = dn_dlnM(M2, (void*)&cANDs2, err);
	 forwardError(*err, __LINE__, 0);

	 res2 *= Ngal_M(self, M2, err);
	 forwardError(*err, __LINE__, 0);

	 x  = r/(R1+R2);

	 /* Calculate P = probability of non-overlap */
	 y  = (x-0.8)/0.29;
	 if (y<0) {
	    P = 0.0;
	 } else if (y>1) {
	    P = 1.0;
	 } else {
	    P  = 3.0*y*y - 2.0*y*y*y;
	 }

	 n += res1*res2*P;
      }

   }

   return sqrt(n);
}

/* Number density of galaxies with halo exclusion effect, i.e. *
 * integrate up only to Mlim.				       */
double n_gal_hexcl(cosmo_hm *self, double a, double logMlim, error **err)
{
   double res, logM;
   cosmo_hmANDstuff2 cANDs2;

   cANDs2.self       = self;
   cANDs2.a          = a;
   cANDs2.asymptotic = 0;

   if (logMlim<logMmax) logM = logMlim;
   else logM = logMmax;

   res = sm2_qromberg(int_for_ngal, (void*)&cANDs2, logMmin, logM,
		      1.e-3, err);
   forwardError(*err, __LINE__, 0.0);

   return res;
}
#undef NM

/* ============================================================ *
 * Limiting mass for 2h-integration, taking into account halo   *
 * exclusion. See Tinker et al. (2005).	Finds Mlim by demanding	*
 * that n_gal_hexcl = n_gal_hexcl_triaxial.			*
 * ============================================================ */
double M_lim(cosmo_hm *self, double r, double a, error **err)
{
   double ng_triax, ng, logMlim, dlogM;

   ng_triax = n_gal_hexcl_triaxial(self, r, a, err);
   forwardError(*err, __LINE__, 0.0);

   dlogM   = 0.05;
   logMlim = logMmin;
   do {

      ng = n_gal_hexcl(self, a, logMlim, err);
      forwardError(*err, __LINE__, 0.0);
      logMlim += dlogM;

      if (logMlim>logMmax) {
	 return exp(logMmax);
      }

   } while (ng<ng_triax);

   return exp(logMlim);
}

/* ============================================================ *
 * Galaxy 2h-term power spectrum with halo exclusion.		*
 * ============================================================ */
#define EPS 1.0e-3
#define NR2h 20
#define NK2h 50
double P2h_g_hexcl(cosmo_hm *self, double a, double k, double r, error **err)
{
   cosmo_hmANDhjmcc3 intpar;
   double Ngal, *ngal, *logMaxMass,
     val, logkmin, logkmax, kk, logk, dlogk,
     logR, dlogR, logRmin, logRmax, RR, prev, PNL;
   int j, i;


   testErrorRetVA(self->hod!=berwein02_hexcl, hm_hodtype,
		  "Invalid hod type (%d), has to be berwein02_hexcl(%d) at this point",
		  *err, __LINE__, 0.0, self->hod, berwein02);

   if (fabs(self->a_hexcl-a)>EPS*EPS) {

      if (self->P2hg_hexcl!=NULL) del_interTable2D(&self->P2hg_hexcl);

      //      fprintf(stderr, "Tabulating P2h_g_hexcl for a=%f\n", a);

      logkmin = log(k_min);
      logkmax = log(k_max);
      dlogk   = (logkmax - logkmin)/((double)NK2h-1.0);

      logRmin = log(RMIN2);
      logRmax = log(RMAX);
      dlogR   = (logRmax-logRmin)/((double)NR2h-1.0);

      self->P2hg_hexcl = init_interTable2D(NK2h, logkmin, logkmax, dlogk, NR2h, logRmin, logRmax, dlogR,
					   0.0, 0.0, err);
      forwardError(*err, __LINE__, 0.0);
      self->a_hexcl = a;

      /* Tabulate the minimum mass and Ngal. */
      logMaxMass = malloc_err(sizeof(double)*(NR2h+1), err);
      forwardError(*err, __LINE__, 0.0);
      ngal       = malloc_err(sizeof(double)*NR2h, err);
      forwardError(*err, __LINE__, 0.0);
      logMaxMass[0] = logMmin;
      for (i=0,logR=logRmin; i<NR2h; i++,logR+=dlogR) {
	 RR   = exp(logR);
	 logMaxMass[i+1] = log(M_lim(self, RR, a, err));
	 forwardError(*err, __LINE__, 0.0);
	 ngal[i] = n_gal_hexcl_triaxial(self, RR, a, err);
      }

      intpar.self = self;
      intpar.a    = a;

      for (j=0,logk=logkmin; j<NK2h; j++,logk+=dlogk) {
	 kk       = exp(logk);
	 intpar.k = kk;
	 //PL       = P_L(self->cosmo, a, kk, err);
	 PNL       = P_NL(self->cosmo, a, kk, err); 

	 forwardError(*err, __LINE__, 0.0);

	 //fprintf(stderr, "%d %g\n", j, kk);

	 for (i=0,logR=logRmin; i<NR2h; i++,logR+=dlogR) {
	    RR   = exp(logR);
	    intpar.r = RR;

	    Ngal = ngal[i];

	    /* Integrate from previous upper limit to new upper limit */
	    if (Ngal>0 && logMaxMass[i]<logMaxMass[i+1]) {
	       val = dsqr(1.0/Ngal*sm2_qromberg(int_for_P2h_g, (void*)&intpar, logMaxMass[i],
						logMaxMass[i+1], EPS, err));
	       forwardError(*err, __LINE__, 0.0);
	       //val *= PL;
	       val *= PNL;
	    } else {
	       val = 0.0;
	    }

	    if (i>0) prev = self->P2hg_hexcl->table[j][i-1];
	    else prev = 0.0;

	    /* Add previous to current value */
	    self->P2hg_hexcl->table[j][i] = prev+val;

	    testErrorRet(!finite(self->P2hg_hexcl->table[j][i]), math_infnan,
			 "Inf or Nan encountered", *err, __LINE__, 0.0);
	 }

      }

      free(logMaxMass);
      free(ngal);

      //fprintf(stderr, "done\n");

   }

   val = interpol2D(self->P2hg_hexcl, log(k), log(r), err);
   forwardError(*err, __LINE__, 0.0);

   return val;
}
#undef EPS
#undef NR2h
#undef NK2h

wt_t *read_wtheta_mk(const char *name, double delta, double intconst, error **err)
{
  /* read in a hjmcc-format w-theta file into an array. */
  FILE *inF; 
  static char str[MAXCHAR];
  double th,w;
  char vectok[] = {",() \t\n\r"};
  char *str2_new;
  wt_t *wtr; 
  int i;

  wtr       = malloc_err(sizeof(nz_t), err);              forwardError(*err, __LINE__, NULL);
  wtr->th   = malloc_err(sizeof(nz_t)*NLINES, err);       forwardError(*err, __LINE__, NULL);
  wtr->w    = malloc_err(sizeof(nz_t)*NLINES, err);       forwardError(*err, __LINE__, NULL);
  wtr->werr = malloc_err(sizeof(nz_t)*NLINES, err);       forwardError(*err, __LINE__, NULL);

  i = 0;
  inF = fopen_err(name, "r", err);                        forwardError(*err, __LINE__, NULL);

  while (fgets(str, MAXCHAR, inF)) {
    if (!(str2_new = strtok(str, vectok)))
      break;
    if (*str2_new == (char)'#')
      continue;

    th=atof(str2_new);
    wtr->th[i]=th;

     /* skip quantities we don't need */
     str2_new = strtok(NULL, vectok);
     str2_new = strtok(NULL, vectok);
     str2_new = strtok(NULL, vectok);
     str2_new = strtok(NULL, vectok);
     str2_new = strtok(NULL, vectok);
     str2_new = strtok(NULL, vectok);
     str2_new = strtok(NULL, vectok);
     w        = (double)atof(str2_new);
     str2_new = strtok(NULL, vectok);
     str2_new = strtok(NULL, vectok);
     wtr->werr[i] = (double)atof(str2_new);

     /* Apply the correction for the integral constraint*/
     wtr->w[i]=w*pow(pow(10.0,th),(-1.0*delta))/
		  (pow(pow(10.0,th),(-1.0*delta))
		   -intconst);


     //     printf ("%5.2f %8.2e %8.2e\n", th, w, wtr->w[i]);

     i++;

     testErrorRetVA(i>=NLINES, hm_overflow, "Too many lines (>=%d), increase NLINES in hod.h",
		    *err, __LINE__, NULL, NLINES);
  }
  wtr->nbins = i;

  realloc_err(wtr->th, wtr->nbins*sizeof(double), err);
  forwardError(*err, __LINE__, NULL);

  realloc_err(wtr->w, wtr->nbins*sizeof(double), err);
  forwardError(*err, __LINE__, NULL);

  realloc_err(wtr->werr, wtr->nbins*sizeof(double), err);
  forwardError(*err, __LINE__, NULL);


  fclose(inF);
  return(wtr);
}

wt_t *read_wtheta(config_hm_info *config, error **err)
{
   wt_t *res;
   res = read_wtheta_mk(config->WTHETA, config->delta, config->intconst, err);
   forwardError(*err, __LINE__, NULL);
   return res;
}

nz_t *read_dndz(char *infile,error **err)
{
  FILE *inF; 
  static char str[MAXCHAR];
  char vectok[] = {",() \t\n\r"};
  int i, nbin;
  double nzr1, nzr2;
  char *str2_new;
  double sum;
  double zm;
  nz_t *nzr;

  nzr      = malloc_err(sizeof(nz_t), err);          forwardError(*err, __LINE__, NULL);
  nzr->z   = malloc_err(sizeof(double)*NLINES, err); forwardError(*err, __LINE__, NULL);
  nzr->fac = malloc_err(sizeof(double)*NLINES, err); forwardError(*err, __LINE__, NULL);

  inF = fopen_err(infile, "r", err);                 forwardError(*err, __LINE__, NULL);
  
  i = zm = sum = 0;
   while (fgets(str, MAXCHAR, inF)) {
     
     if (!(str2_new = strtok(str, vectok)))
       break;

     if (*str == (char)'#')
       continue;

     //theta = atof(str2_new); // not used
     nzr1  = (double)atof(str2_new);

     str2_new = strtok(NULL, vectok);
     testErrorRet(!str2_new, hm_io, "Error reading the lines", *err, __LINE__, NULL);

     nzr2 = (double)atof(str2_new);

     nzr->z[i] = fabs(nzr2-nzr1)/2.0 + nzr1;

     str2_new = strtok(NULL, vectok);
     testErrorRet(!str2_new, hm_io, "Error reading the lines", *err, __LINE__, NULL);

     nzr->fac[i] = (double)atof(str2_new);

     sum += (nzr2-nzr1)*nzr->fac[i];

     zm += (nzr2-nzr1)*nzr->fac[i]*nzr->z[i];

     i++;

     testErrorRetVA(i>=NLINES, hm_overflow, "Too many lines (>=%d), increase NLINES in hod.h",
		    *err, __LINE__, NULL, NLINES);
   }

   fclose (inF);
   nbin = i;
   /* normalise */
   for (i=0;i<nbin;i++) {
     nzr->fac[i]=nzr->fac[i]/sum; 
     /*       printf ("%f %f\n", nzr->z[i], nzr->fac[i]); */
   }
   nzr->nbins=nbin;
   zm/=sum;
   nzr->zm=zm;
   printf ("zm=%f\n", zm);
   /* end */

   /* also initialize spline function here */ 
   nzr->ypn = sm2_vector(1,nzr->nbins,err); 
   forwardError(*err, __LINE__,0);

   sm2_spline(nzr->z-1,nzr->fac-1,nzr->nbins,1.0e30,1.0e30,nzr->ypn,err);

   return nzr;
}

void read_config_hm_file(config_hm_info *config, char cname[], error **err)
{
   FILE *F;
   config_element c = {0, 0.0, ""};

   F = fopen_err(cname, "r", err);                   forwardError(*err, __LINE__,);

   CONFIG_READ_S(config, WTHETA, s, F, c, err);
   CONFIG_READ_S(config, COVNAME, s, F, c, err);
   CONFIG_READ_S(config, DNDZ, s, F, c, err);
   CONFIG_READ_S(config, OUTFILE, s, F, c, err);

   CONFIG_READ(config, ngal, d, F, c, err);
   CONFIG_READ(config, ngalerr, d, F, c, err);
   CONFIG_READ(config, area_deg, d, F, c, err);
   CONFIG_READ(config, intconst, d, F, c, err);
   CONFIG_READ(config, delta, d, F, c, err);
   CONFIG_READ(config, nbins, d, F, c, err);

   CONFIG_READ(config, alpha_min, d, F, c, err);
   CONFIG_READ(config, alpha_max, d, F, c, err);
   CONFIG_READ(config, dalpha, d, F, c, err);


   CONFIG_READ(config, M1_min, d, F, c, err);
   CONFIG_READ(config, M1_max, d, F, c, err);
   CONFIG_READ(config, dM1, d, F, c, err);


   CONFIG_READ(config, Mmin_min, d, F, c, err);
   CONFIG_READ(config, Mmin_max, d, F, c, err);
   CONFIG_READ(config, dMmin, d, F, c, err);

   fclose(F);
}

/* Limber equation: modification, allow input pofk.   *
 * Obsolete function for testing only, use w_of_theta *
 * instead.					      */
double w_lim(cosmo_hm *self, nz_t *nzr, double theta, pofk_t pofk,
	     error **err)
{
  cosmohmANDstuff_pofk inpar;  double ans; 

  double z1,z2;
  double EPS;
  EPS = 1e-3;
  ans=0;

  inpar.theta = theta;
  inpar.self = self;
  inpar.nz = nzr;
  inpar.pofk = pofk;
  

  z1=nzr->z[0];
  z2=nzr->z[nzr->nbins-1];

  ans=sm2_qromberg(w_zed,(void *)&inpar,z1,z2,EPS, err);
  ans=ans*1.0/(2.0*pi)*1.0/R_HUBBLE;
  forwardError(*err,__LINE__,0);
  return ans;
}

/* Limber equation: modification, allow input pofk.          *
 * Other modification, do direct summation over each bin.    *
 * Obsolete function for testing only, use w_of_theta        *
 * instead.						     */
double w_lim_fast(cosmo_hm *self, nz_t *nzr, double theta, pofk_t pofk,
	     error **err)
{
  double ans; 
  cosmohmAND2double_pofk inpar_limber;
  cosmo *model;
  double z, dz, k1, k2;
  double EPS;
  int i, nbins;

  EPS = 1e-3;
  k1=1e-3;
  k2=100;
  ans=0.0;

  inpar_limber.self=self;
  inpar_limber.a=theta;
  inpar_limber.pofk = pofk;
  model = self->cosmo;


  dz    = nzr->z[1]-nzr->z[0];
  nbins = nzr->nbins;

  for (i=0;i<nbins;i++) {
    z = nzr->z[i];
    inpar_limber.r=z;
    /* integrate over all values of k */
    ans += sqrt(model->Omega_m*pow((1.0+z),3.0)+model->Omega_de)*nzr->fac[i]*nzr->fac[i]*
      sm2_qromberg(limber,(void *)&inpar_limber,k1,k2,EPS, err)*dz;
    forwardError(*err,__LINE__,0);
  }

  ans = ans*1.0/(2.0*pi)*1.0/R_HUBBLE;

  return ans;
}

/* ============================================================ *
 * Inner u-integral for w(theta), e.g. Blake, Collister &       *
 * Lahav et al. (2008), eq. 33., Bartelmann & Schneider (2001), *
 * eq. 2.79.							*
 * ============================================================ */
double int_over_xir_u(cosmo_hm *self, double theta, double z, pofk_t pofk, error **err)
{
   double res, r, x, u, logu, dlogu, ww;
   double zm,azm;
   int wOmegar = 0;

   //a = 1.0/(1.0+z);
   zm = zmean(self->redshift, 0, err);         forwardError(*err, __LINE__, 0.0);
   azm = 1.0/(1.0+zm);
   // note that we calculate xi(r) at the MEAN redshift not at "a"!
   dlogu = (log(RMAX)-log(RMIN))/((double)NR-1.0);
   for (logu=log(RMIN),res=0.0; logu<RMAX; logu+=dlogu) {
      u   = exp(logu);
      ww  = w(self->cosmo, azm, wOmegar, err);   forwardError(*err, __LINE__, 0.0);
      x   = f_K(self->cosmo, ww, err);         forwardError(*err, __LINE__, 0.0);
      r   = sqrt(u*u + x*x*theta*theta);
      /* du = u*dlogu */
      res += u*xir_g(self, azm, r, pofk, err);
   }
   res *= dlogu;

   return res;
}

/* ============================================================ *
 * Angular correlation function, projecting from xi(r). This is *
 * needed for berwein02-model where a fast Hankel transform is  *
 * not possible for the 1h-cs term.				*
 * Low-level function, use w_of_theta as master function.	*
 * ============================================================ */
#define NTHETA 50
#define NZ 20
#define THETA_MIN (0.002*arcmin)
#define THETA_MAX (200.0*arcmin)
#define EPS 1.0e-7
double w_from_xir(cosmo_hm *self, double theta, int i_bin, int j_bin, pofk_t pofk_w_from_xir, error **err)
{
   double dlogtheta, tt, logtheta, res, z, dz, integrand_z;
   double zmin, zmax, zmini, zminj, zmaxi, zmaxj;
   int Nzbin, Nzcorr, ii, jj, index, k;

   Nzbin  = self->redshift->Nzbin;
   Nzcorr = Nzbin*(Nzbin+1)/2;

   if (pofk_w_from_xir!=self->pofk_w_from_xir) {
      if (self->wfromxi!=NULL) {
	 del_interTable_arr(&(self->wfromxi), Nzcorr);
	 self->pofk_w_from_xir = pofk_undef;
      }
   }

   if (self->wfromxi==NULL) {

      /* 0's, 1's: dummy arguments, filled later */
      dlogtheta = (log(THETA_MAX)-log(THETA_MIN))/((double)NTHETA-1.0);
      self->wfromxi = init_interTable_arr(Nzcorr, NTHETA, log(THETA_MIN), log(THETA_MAX), 
					  dlogtheta, 0.0, 0.0, err);
      forwardError(*err, __LINE__, 0);
      self->pofk_w_from_xir = pofk_w_from_xir;

      for (ii=0; ii<Nzbin; ii++) {
	 zmini = get_zmin(self->redshift, i_bin);
	 zmaxi = get_zmax(self->redshift, i_bin);
	 for (jj=ii; jj<Nzbin; jj++) {
	    zminj = get_zmin(self->redshift, j_bin);
	    zmaxj = get_zmax(self->redshift, j_bin);

	    zmin  = MIN(zmini, zminj);
	    zmax  = MAX(zmaxi, zmaxj);
	    dz    = (zmax-zmin)/((double)NZ-1.0);

	    index = idx_zz(ii, jj, Nzbin);

	    for (k=0; k<NTHETA; k++) self->wfromxi[index]->table[k] = 0.0;

	    /* Outer z-integration */
	    for (z=zmin; z<zmax; z+=dz) {

	       /* Tabulate w on theta-grid */
	       for (k=0,logtheta=log(THETA_MIN); k<NTHETA; k++,logtheta+=dlogtheta) {
		  tt    = exp(logtheta);

		  integrand_z  = prob(self->redshift, z, i_bin, err);
		  forwardError(*err, __LINE__, 0.0);
		  integrand_z *= prob(self->redshift, z, j_bin, err);
		  forwardError(*err, __LINE__, 0.0);

		  if (fabs(integrand_z)>EPS) {
		     integrand_z *= int_over_xir_u(self, tt, z, pofk_w_from_xir, err);
		     forwardError(*err, __LINE__, 0.0);
		     integrand_z /= drdz(self->cosmo, 1.0/(1.0+z), err);
		     forwardError(*err, __LINE__, 0.0);
		  }
		  self->wfromxi[index]->table[k] += 2.0*integrand_z*dz;

	       }

	    }

	 }
      }

   }

   if (theta<THETA_MIN || theta>THETA_MAX) return 0.0;

   index = idx_zz(i_bin, j_bin, Nzbin);
   res = interpol_wr(self->wfromxi[index], log(theta), err);
   forwardError(*err, __LINE__, 0);

   return res;
}
#undef NTHETA
#undef NZ
#undef THETA_MIN
#undef THETA_MAX
#undef EPS

/* ============================================================ *
 * Angular correlation function w(theta).			*
 * Low-level function, use w_of_theta as master function.	*
 * ============================================================ */
double w_lim_hankel(cosmo_hm *self, double theta, int i_bin, int j_bin, pofk_t pofk, error **err)
{
   cosmohmANDstuff2 cs2;
   double dlogtheta, logthetamin, logthetamax;
   double res;
   int Nzbin, Nzcorr, ii, jj, index;

   Nzbin  = self->redshift->Nzbin;
   Nzcorr = Nzbin*(Nzbin+1)/2;
   if (pofk!=self->pofk) {
      if (self->wlim!=NULL) {
	 del_interTable_arr(&(self->wlim), Nzcorr);
	 self->pofk = pofk_undef;
      }
   }
   if (self->wlim==NULL) {
      /* 0's, 1's: dummy arguments, filled later */
      self->wlim = init_interTable_arr(Nzcorr, N_thetaH, 0, 1, 1, 0.0, 0.0, err);
      forwardError(*err, __LINE__, 0);
      self->pofk = pofk;

      cs2.self = self;
      cs2.pofk = pofk;
      for (ii=0; ii<Nzbin; ii++) {
	 for (jj=ii; jj<Nzbin; jj++) {

	    cs2.i_bin = ii;
	    cs2.j_bin = jj;

	    index = idx_zz(ii,jj,Nzbin);
	    /* cs2.ell not used */
	    tpstat_via_hankel((void*)&cs2, &(self->wlim[index]->table), &logthetamin, &logthetamax,
			      tp_w, P_projected_hm, ii, jj, err);
	    forwardError(*err, __LINE__, 0);
	    dlogtheta = (logthetamax-logthetamin)/((double)N_thetaH-1.0);
	    self->wlim[index]->a  = logthetamin;
	    self->wlim[index]->b  = logthetamax;
	    self->wlim[index]->dx = dlogtheta;

	 }
      }
   }

   index = idx_zz(i_bin, j_bin, Nzbin);
   res = interpol_wr(self->wlim[index], log(theta), err);
   forwardError(*err, __LINE__, 0);

   return res;
}

/* ============================================================ *
 * Master function for the angular correlation function         *
 * w(theta). Calls w_lim_hankel (Fast Hankel Transform) and/or  *
 * w_from_xir (direct Riemann-summation) according to		*
 * the HOD type.						*
 * ============================================================ */
double w_of_theta(cosmo_hm *self, double theta, int i_bin, int j_bin, error **err)
{
   double res;

   switch (self->hod) {
      case hamana04 :
	 res = w_lim_hankel(self, theta, i_bin, j_bin, pthg, err);
	 forwardError(*err, __LINE__, 0.0);
	 break;

      case berwein02 :
	 res  = w_from_xir(self, theta, i_bin, j_bin, p1hgcs, err);
	 forwardError(*err, __LINE__, 0.0);
	 res += w_lim_hankel(self, theta, i_bin, j_bin, (pofk_t)((int)(p1hgss+p2hg)), err);
	 forwardError(*err, __LINE__, 0.0);
	 break;

      case berwein02_hexcl :
	 res  = w_from_xir(self, theta, i_bin, j_bin, (pofk_t)((int)(p1hgcs+p2hg)), err);
	 forwardError(*err, __LINE__, 0.0);
	 // MKDEBUG!
	 //if (theta<0.00055) {
	    res += w_lim_hankel(self, theta, i_bin, j_bin, p1hgss, err);
	    forwardError(*err, __LINE__, 0.0);
	    //}
	 break;

      default :
	*err = addErrorVA(ce_unknown, "Wrong HOD type %d", *err, __LINE__, self->hod);
	return 0.0;
   }

   return res;
}

/* This is the equation to be integrated over in z */
double w_zed(double z, void *inpar, error **err) 
{
  cosmohmANDstuff_pofk *cosmo_in;  double ans; 
  cosmohmAND2double_pofk inpar_limber;
  cosmo *model;
  int nk; /* number of steps in k */ 
  nz_t *nzr;
  double k1,k2,k,dk,sum;
  double EPS;
  double zfac;
  int nbins;
  EPS = 1e-2;
  k1=1e-2;
  k2=10;
  nk=10;
  ans=0;
  dk = (k2-k1)/nk;
  cosmo_in = (cosmohmANDstuff_pofk *)inpar;

  nzr=cosmo_in->nz;
  nbins=cosmo_in->nz->nbins;
  model=cosmo_in->self->cosmo;


  inpar_limber.self=cosmo_in->self;
  inpar_limber.r=z;
  inpar_limber.a=cosmo_in->theta;
  inpar_limber.pofk = cosmo_in->pofk;

  sm2_splint(nzr->z-1,nzr->fac-1,nzr->ypn,nbins,z,&zfac,err);
  forwardError(*err,__LINE__,0);

  // The following is z-integration without splines */
  /*
  zfac = -1;
  for (i=0; i<nbins-1; i++) {
     if (nzr->z[i]<=z && z<nzr->z[i+1]) {
	zfac = nzr->fac[i];
	break;
     }
  }
  if (z>=nzr->z[nbins-1]) zfac = nzr->fac[nbins-1];
  testErrorRet(zfac==-1, ce_negative, "z bin not found", *err, __LINE__, 0);
  */

  if (z < nzr->z[0] || z > nzr->z[nbins-1] || zfac < 0) return 0;


  /* integrate over all values of k */
  sum=0.0;
  for (k=k1; k < k2; k+=dk) { 
    sum+=
      sm2_qromberg(limber,(void *)&inpar_limber,k,k+dk,EPS, err);
    forwardError(*err,__LINE__,0);
  }
  ans = sum*sqrt(model->Omega_m*pow((1.0+z),3.0)+model->Omega_de)*zfac*zfac;
  return ans;
}

double limber(double k, void *intpar, error **err)
{
  /* this is the integral over k */
  double ans, a, z, theta;
  double r, pofk;
  cosmo *model;
  cosmohmAND2double_pofk *cosmo_in;
  int wOmegar;

  cosmo_in = (cosmohmAND2double_pofk *)intpar;
  z        = cosmo_in->r;
  theta    = cosmo_in->a;
  model    = cosmo_in->self->cosmo;
  a        = 1.0/(1.0+z);
  wOmegar  = 0;
  r        = w(model, a, wOmegar, err);
  forwardError(*err,__LINE__,0);

  switch (cosmo_in->pofk) {
     case pnl :
	pofk = P_NL(model,a, k,err);
	break;
     case p1hg:
	pofk = P1h_g(cosmo_in->self,a,k,err);
	break;
     case p2hg:
	pofk = P2h_g(cosmo_in->self,a,k,err);
	break;
     case pthg:
	pofk = Pth_g(cosmo_in->self,a,k,err);
	break;
     case pl:
	pofk = P_L(model,a,k,err);
	break;
     default :
	addError(ce_unknown, "wrong or not supported pofk", *err, __LINE__);
	return 0.0;
  }
  forwardError(*err,__LINE__,0.0);

  ans = pofk*k*bessj0(f_K(model,r,err)*k*theta);
  forwardError(*err,__LINE__,0.0);

  return ans;
}


/* Returns the number density of galaxies per volume corresponding *
 * to the observed number of galaxies ngal.			   */
double ngd_volume(cosmo_hm *model, double ngal, double area, error **err)
{
   double vc, rad2degsqr, ngd_obs;

   /* Comoving volume */
   vc       = Vc(model, err);         forwardError(*err, __LINE__, -1.0);

   /* Observed 2d-number density of galaxies */
   rad2degsqr = (pi/180.0)*(pi/180.0);
   ngd_obs    = ngal/area/rad2degsqr/vc;

   return ngd_obs;
}

/* ============================================================ *
 * HOD halomodel log-likelihood function.			*
 * See Hamana et al. 2004, MNRAS, 347, 813			*
 * ============================================================ */
double chi2_hm(cosmo_hm *model, halomode_t halomode, const wt_t* data, ngal_fit_t ngal_fit_type,
	       double ngal, double ngalerr, double area, error **err)
{
   double chi2, ngd, ngd_obs, ngd_err;

   /* TODO: combine several redshift bins! */

   ngd_obs = ngd_volume(model, ngal, area, err);   forwardError(*err, __LINE__, -1.0);
   ngd_err = ngd_obs/ngal*ngalerr;

   //double ngd_obs_w;
   //ngd_obs_w = ngd_obs_weighted(model,area,ngal,err);
   //printf ("ngd=%5.2e ngd_err=%5.2e ngd_w=%5.2e\n",ngd_obs, ngd_err, ngd_obs_w);


   if (halomode==hjmcc || halomode==hjmcc_cov || halomode==logw) {
      ngd  = 0.0;
      chi2 = compute_chisq(model, data, ngd_obs, ngd_err, ngal_fit_type, &ngd, halomode==logw, err);
      forwardError(*err, __LINE__, -1.0);
   } else {
      chi2 = 0.0;
      ngd  = 0.0;
   }

   //   fprintf(stderr, "chi2_hm: ngden(mod,obs,err) = (%5.2e,%5.2e,%5.2e) ln(ngden)(mod,obs,err) = (%g,%g,%g, %g)\n",
   //      	   ngd, ngd_obs, ngd_err, log(ngd), log(ngd_obs), ngd_err/ngd_obs, log(ngd_err+ngd_obs) - log(ngd_obs-ngd_err));
	   

   /* det C ... */
   return -0.5*chi2;
}

/* ============================================================= *
 * Returns chi^2 using the angular correlation function w(theta) *
 * and the galaxy number density.				 *
 * ============================================================= */
double compute_chisq(cosmo_hm *model, const wt_t *wth, double ngd_obs, double ngd_err,
		     ngal_fit_t ngal_fit_type, double *ngd, int dologw, error **err)
{
   int i, j;
   double chisq, *wh, th;
   double dchisq1,dchisq2;
   double lngd, lngd_obs, lngd_err;


  testErrorRet(model->redshift->Nzbin!=1, ce_overflow,
	       "More than one redshift bin not yet supported in likelihood",
	       *err, __LINE__, -1.0);


  if (ngal_fit_type!=ngal_lin_fit_only) {

     wh = malloc_err(sizeof(double)*wth->nbins, err);  forwardError(*err, __LINE__, 0.0);

     for (i=0; i<wth->nbins; i++) {
	th    = pow(10.0,wth->th[i])/180.0*pi;
	wh[i] = w_of_theta(model, th, 0, 0, err);
	forwardError(*err, __LINE__, -1.0);
	if (dologw==1) wh[i] = log(wh[i]);
     }

     if (wth->wcov==NULL) {
	for (i=0,chisq=0.0; i<wth->nbins; i++) {
	   dchisq1  = dsqr((wth->w[i] - wh[i])/(wth->werr[i]));
	   chisq   += dchisq1;
	   //fprintf (stderr, "%10d %5.2e %5.2e %5.2e %g\n", i, wh[i],wth->w[i],dchisq1, chisq);
	}
     } else {
	for (i=0,chisq=0.0; i<wth->nbins; i++) {
	   for (j=0; j<wth->nbins; j++) {
	      dchisq1  = (wth->w[i] - wh[i])*wth->wcov[i*wth->nbins+j]*(wth->w[j] - wh[j]);
	      chisq   += dchisq1;
	      //if (i==j) fprintf (stderr, "%10d %10d %5.2e  %5.2e %5.2e   %5.2e %g\n",
	      //		 i, j, wh[i], wth->w[i], wth->wcov[i*wth->nbins+j], dchisq1, chisq);
	   }
	}
     }

     free(wh);

  } else {

     /* Only fit number of galaxies (below) */
     chisq = 0.0;

  }
  
  if (ngal_fit_type!=ngal_no_fit) {
     *ngd = Ngal_den(model, err);
  } else {
     *ngd = -1.0;
  }

  /* Number density of galaxies */
  switch (ngal_fit_type) {
     case ngal_log_fit :
	/* "Number density varied logarithmically with M1" [Hamana 2004] */
	lngd_obs = log(ngd_obs);

	/* Delta ln x = Delta x / x */
	lngd_err = ngd_err/ngd_obs;

	lngd = log(*ngd);

	dchisq2 = dsqr((lngd_obs - lngd)/lngd_err);
	break;

     case ngal_lin_fit : case ngal_lin_fit_only :
	dchisq2 = dsqr((ngd_obs - *ngd)/ngd_err);
	break;

     case ngal_no_fit :
	dchisq2 = 0.0;
	break;

     case ngal_match_M1 :
	/* MKDEBUG: TODO... */
     default :
	*err = addErrorVA(ce_unknown, "Wrong ngal_fit_type %d", *err, __LINE__, (int)ngal_fit_type);
	return -1.0;
  }

  chisq += dchisq2;

  fprintf(stderr, "\nCS2 chi^2: %g %g\n", chisq, dchisq2);
    //fprintf(stderr, "ngal(obs) ngal(pred): %5.2e %5.2e\n", exp(lngd_obs), exp(lngd));

  testErrorRetVA(chisq<0.0, math_negative, "Negative chi^2 %g. Maybe the covariance matrix is not positive",
		 *err, __LINE__, -1.0, chisq);

  return chisq;
}

double int_for_P_projected_hm(double z, void *intpar, error **err)
{
   cosmohmANDstuff2 *cs2;
   pofk_t pofk;
   cosmo_hm *self;
   double res, a, ps, ell, k, ww, fK;
   int wOmegar;
   int i_bin, j_bin;

   cs2   = (cosmohmANDstuff2*)intpar;
   self  = cs2->self;
   pofk  = cs2->pofk;
   ell   = cs2->ell;
   i_bin = cs2->i_bin;
   j_bin = cs2->j_bin;

   a       = 1.0/(1.0+z);
   wOmegar = 0;

   ww = w(self->cosmo, a, wOmegar, err);     forwardError(*err, __LINE__, 0);
   fK = f_K(self->cosmo, ww, err);           forwardError(*err, __LINE__, 0);
   k  = ell/fK;

   if (k<=k_min || k>=k_max) {
      //fprintf(stderr, "k = %g out of range [%g:%g]\n", k, k_min, k_max);
      return 0;
   }

   testErrorRetVA(pofk&p1hgcs, hm_pofk, "pofk=p1hgcs(%d) not supported in Hankel transform",
		  *err, __LINE__, 0.0, p1hgcs);

   ps = 0.0;
   if (pofk&pl)     { ps += P_L(self->cosmo, a, k, err);    forwardError(*err, __LINE__, 0); }
   if (pofk&pnl)    { ps += P_NL(self->cosmo, a, k, err);   forwardError(*err, __LINE__, 0); }
   if (pofk&p1hdm)  { ps += P1h_dm(self, a, k, err);        forwardError(*err, __LINE__, 0); }
   if (pofk&p2hdm)  { ps += P2h_dm(self, a, k, err);        forwardError(*err, __LINE__, 0); }
   if (pofk&p1hg)   { ps += P1h_g(self, a, k, err);         forwardError(*err, __LINE__, 0); }
   if (pofk&p2hg)   { ps += P2h_g(self, a, k, err);         forwardError(*err, __LINE__, 0); }
   if (pofk&pthg)   { ps += Pth_g(self, a, k, err);         forwardError(*err, __LINE__, 0); }
   if (pofk&p1hgss) { ps += P1h_g_ss(self, a, k, err);      forwardError(*err, __LINE__, 0); }

   res  = sqrt(Esqr(self->cosmo, a, wOmegar, err));        forwardError(*err, __LINE__, 0);
   res *= prob(self->redshift, z, i_bin, err);             forwardError(*err, __LINE__, 0);
   res *= prob(self->redshift, z, j_bin, err);             forwardError(*err, __LINE__, 0);
   res /= fK*fK;

   return res*ps;
}

#define EPS 1.0e-3
double P_projected_hm(void *cs2v, double l, int i_bin, int j_bin, error **err)
{
   cosmohmANDstuff2 *cs2;
   double ans; 
   cosmo_hm *self;
   double z1, z2, zi, zj;

   cs2        = (cosmohmANDstuff2*)cs2v;
   cs2->ell   = l;
   /* cs2->i_bin, j_bin already set */
   testErrorRetVA(cs2->i_bin!=i_bin, hm_zbin, "Inconsistent z-bins, %d!=%d", *err, __LINE__, 0.0, cs2->i_bin, i_bin);
   testErrorRetVA(cs2->j_bin!=j_bin, hm_zbin, "Inconsistent z-bins, %d!=%d", *err, __LINE__, 0.0, cs2->i_bin, j_bin);
   self       = (cosmo_hm*)cs2->self;

   zi = get_zmin(self->redshift, i_bin); zj = get_zmin(self->redshift, j_bin);
   z1 = fmin(zi, zj);

   zi = get_zmax(self->redshift, i_bin); zj = get_zmax(self->redshift, j_bin);
   z2 = fmax(zi, zj);

   ans = sm2_qromberg(int_for_P_projected_hm, (void *)cs2, z1, z2, EPS, err);
   forwardError(*err,__LINE__,0);
   ans = ans/R_HUBBLE;

   return ans;

}
#undef EPS
