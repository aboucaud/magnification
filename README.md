MAGNIFICATION FISHER MATRIX CODE
================================

* _Package name:_ `magnification`
* _Author:_ Alexandre Boucaud
* _Email:_ <boucaud.alexandre@gmail.com>
* _Created:_ 06/06/2013
* _Last modified:_ 27/10/2015
* _Version:_ 0.9.1

***

## Installation instructions

#### Compile the needed shared libraries (`Cython` library required)

    $ make

For details, this
 * compiles the version 2.3 of `NICAEA` (cosmology library from Martin Kilbinger, see [webpage](http://www.cosmostat.org/software/nicaea/))
 * bundles all necessary methods into a shared library called `libnicaea.so`
 * creates the Python wrapper for the matter power spectrum using `Cython`


#### Test the code

    $ make test

At this point a Python window should appear with a figure of a linear and a non linear matter powerspectrum.
You can then close the window to proceed.


## The `magnification` package

TBC
