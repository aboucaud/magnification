# -*- coding: utf-8 -*-
"""
Created on Fri Feb  1 19:10:34 2013

@author: alexandreboucaud
"""
import time
#import numpy as np

import magnification.survey as Survey
import magnification.fisher as Fisher
#import magnification.fisherpp as FisherPP

# REDSHIFT RANGE
ZTAB = [0.3, 0.7]  # , 1.1]
# Ell RANGE
ELLMIN, ELLMAX, NELL = 10, 3000, 30

print "\n########################"
print "# Starting Fisher test #"
print '########################\n'
ZSTRING = '\t'.join('%1.2f' % z for z in ZTAB)
print 'Redshift range:\n\t%s' % ZSTRING
print 'ELL range:\n\t%d\t=>\t%d\n' % (ELLMIN, ELLMAX)

# REDSHIFT BINS
#LsstRedshiftBins = Survey.RedshiftBins()
EuclidZBins = Survey.RedshiftBins(ZTAB, survey='Euclid', dictype='ivan')

T0 = time.time()
# FISHER
F = Fisher.MagnificationFisher(EuclidZBins, 'code')
s8dict = F.get_derivatives('norm')
# omdict = F.get_derivatives('omega_m')
# oldict = F.get_derivatives('omega_l')
# w0ict = F.get_derivatives('w0')

# F.run_fisher(ELLMIN, ELLMAX, NELL)

T1 = time.time()
secs = T1 - T0
mins = secs / 60.
print 'Time:\t%1.fsecs = %0.2fmins' % (secs, mins)

# F.write(secs)
