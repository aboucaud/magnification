#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import sys
import os
import pickle
import numpy as np
from matplotlib import rc
import matplotlib.pyplot as plt

import magnification.survey as Survey
import magnification.correlation as Corr
import magnification.covariance as Cov
import magnification.cosmology as Cosmo

blue = "#348ABD"
red = "#A60628"
violet = "#7A68A6"
green = "#467821"
orange = "#E24A33"
pink = '#F53374'
colors = [blue, red, green, orange, pink]
linesty = ['--', '-', ':', '-.']
linewei = [2.0, 2.0, 2.5, 2.5]

plotpath = os.getenv('THPLOTS')

# DATA FILES
pfolder = 'picklefiles/'
ext = '.pck'
snrfile = 'snr5bins'
psfile = 'ps5bins'
psmagfile = 'psm5bins'
psautofile = 'psa5bins'
pslensfile = 'psl5bins'
snfile = 'shotnoise5bins'

str_spectra = [psmagfile, psautofile, pslensfile]

rc('text', usetex=True)
rc('axes', labelsize='x-large')


################################################

# REDSHIFT RANGE
ZTAB = [0.3, 0.7, 1.1, 1.5, 1.9]
# Ell RANGE
# ELLMIN, ELLMAX, NELL = 10, 3000, 30
ELLMIN, ELLMAX, NELL = 10, 3000, 50

cosmofid = Cosmo.CosmoStructure(dictype="ivan")

# LsstRedshiftBins = Survey.RedshiftBins(ZTAB, survey="LSST", dictype="ivan")


class MagnificationData():
    def __init__(self, ztab, ellmin, ellmax,
                 nell=30, survey='LSST', dictype='ivan', linear=None):
        self.zbins = Survey.RedshiftBinsBasic(ztab, survey=survey, dictype=dictype)
        self.dictype = dictype
        self.linear = linear
        self.ell = []
        self.delta_ell = []
        self.cov = []

        self.make_ell()

    def make_ell(self):
        bigell = np.logspace(
            np.log10(ELLMIN), np.log10(ELLMAX), num=NELL)
        self.ell = 0.5 * (bigell[:-1] + bigell[1:])
        self.delta_ell = np.diff(bigell)

    def init_covmatrix(self):
        cov = Cov.MagnificationCovariance(self.zbins, linear=self.linear)
        cov._init_matrix()
        cov._init_spectra()
        self.cov = cov

    def compute_covmatrix(self, ell, delta_ell):
        return self.cov.get_matrix(ell, delta_ell)

    def compute_spectrum(self, ia, ib, typ='cross'):
        corr = Corr.CorrelationFull(self.zbins, ia, ib, dictype=self.dictype,
                                    corrtype=typ, linear=self.linear)
        return corr.kernel(np.log(self.ell))


def find_index(nz, ia, ib):
    idlis = []
    for i in xrange(nz):
        for j in xrange(i, nz):
            idlis.append((i, j))
    return idlis.index((ia, ib))


def compute_signal2noise(mag):
    mag.init_covmatrix()
    mat_list = []
    for l, dl in zip(mag.ell, mag.delta_ell):
        mat_list.append(np.sqrt(np.diag(mag.compute_covmatrix(l, dl))))
    # noise = np.diag(mag.covmatrix)
    nz = len(ZTAB)
    ncross = int(0.5 * nz * (nz - 1))
    SNR = {}
    print "Computing signal to noise for {} power spectra".format(ncross)
    for i in xrange(nz-1):
        SNR[str(i)] = {}
        for j in xrange(i+1, nz):
            id = find_index(nz, i, j)
            s = mag.compute_spectrum(i, j, 'cross')
            n = np.array([mat[id] for mat in mat_list])
            SNR[str(i)][str(j)] = s / n
            print "Foreground bin {}\tBackground bin {}".format(i, j)
    return SNR


def compute_spectra(mag):
    nz = len(ZTAB)
    PSM = {}
    PSA = {}
    PSL = {}
    for i in xrange(nz):
        PSM[str(i)] = {}
        PSA[str(i)] = {}
        PSL[str(i)] = {}
        for j in xrange(i, nz):
            s = mag.compute_spectrum(i, j, 'cross')
            a = mag.compute_spectrum(i, j, 'auto')
            m = mag.compute_spectrum(i, j, 'lens')
            PSM[str(i)][str(j)] = s
            PSA[str(i)][str(j)] = a
            PSL[str(i)][str(j)] = m
            print "Foreground bin {}\tBackground bin {}".format(i, j)
    return PSM, PSA, PSL


def compute_spectr(mag, typ):
    nz = len(ZTAB)
    PS = {}
    for i in xrange(nz):
        PS[str(i)] = {}
        for j in xrange(i, nz):
            print "Foreground bin {}\tBackground bin {}".format(i, j)
            PS[str(i)][str(j)] = mag.compute_spectrum(i, j, typ)
    return PS


def save_shotnoise(mag):
    sn = mag.zbins.shot_noise()
    with open(snfile, 'w') as pfile:
        pickle.dump(sn, pfile)
    print "Shot noise file written"


def save_pickle(data, fil, extra=''):
    if extra:
        fil += '_{}'.format(extra)
    with open('{}{}{}'.format(pfolder, fil, ext), 'w') as tempf:
        pickle.dump(data, tempf)


def load_pickle(fil, extra=''):
    if extra:
        fil += '_{}'.format(extra)
    with open('{}{}{}'.format(pfolder, fil, ext), 'r') as tempf:
        data = pickle.load(tempf)
    return data


def make_ell():
    bigell = np.logspace(np.log10(ELLMIN), np.log10(ELLMAX), num=NELL)
    return 0.5 * (bigell[:-1] + bigell[1:])


def ell_conv(ell, fg):
    chi = cosmofid.comoving_distance(0, ZTAB[fg])
    return 2 * np.pi * chi / ell


#########
# PLOTS #
#########

#labels
lell = r'$\ell$'
lsnr = r'$\frac{P_{gm}(\ell)}{\sqrt{\sigma(\ell, \Delta\ell)}}$'
lsize = r'$\lambda\ [{\rm Mpc}]$'
lpmag = r'$\frac{\ell^2\,P_{\mu\delta}(\ell)}{2\pi}$'
# lp = r'$\ell^2\,P(\ell)$'
lp = r'$\ell\,(\ell+1)\,P(\ell)/2\pi$'
lamb = r'$\lambda\ [{\rm Mpc}]$'
snrlabel = r'${\rm signal\ to\ noise\ ratio}$'
redshif = r'$z_{\rm fg} = %1.1f\ -\ z_{\rm bg} = %1.1f$'
xlmin, xlmax = 1.0e1, 3.0e3


def plot_snr(bins, outputfile='snr'):
    snr_dict = load_pickle(snrfile)
    ell = make_ell()
    fig = plt.figure()
    ax = fig.add_subplot(111)
    for i, vals in enumerate(bins):
        a, b = map(str, vals)
        ax.semilogx(ell, snr_dict[a][b], '.',
                    color=colors[i], label='fg {} - bg {}'.format(*vals))
    # ax.set_title('')
    ax.set_xlabel(lell)
    ax.set_ylabel(lsnr)
    # ax.set_axis()
    ax.set_ylim(0, 0.7)
    plt.legend(loc='best', frameon=False)
    fig.savefig(os.path.join(plotpath, 'tests/'+outputfile+'.png'), dpi=144)
    plt.close()
    # fig.show()
    return


def plot_snr_fg(fg, extra=''):
    bins = [(fg, x) for x in range(fg+1, len(ZTAB))]
    if not extra:
        extra2 = 'linear'
    else:
        extra2 = extra + '_linear'
    snr_dict = load_pickle(snrfile, extra=extra)
    snr_dict_lin = load_pickle(snrfile, extra=extra2)
    ell = make_ell()
    fig = plt.figure()
    ax = fig.add_subplot(111)
    for i, vals in enumerate(bins):
        a, b = map(str, vals)
        ax.semilogx(ell, snr_dict[a][b], '-',
                    color=colors[i], lw=2.0, label='fg {} - bg {}'.format(*vals))
        ax.semilogx(ell, snr_dict_lin[a][b], '--',
                    color=colors[i], lw=1.5)
    # ax.set_title('')
    ax.set_xlabel(lell)
    ax.set_ylabel(lsnr)
    # ax.set_axis()
    ymin, ymax = 0.0, 6.0
    ax.set_ylim(ymin, ymax)
    plt.legend(loc=2, frameon=False)
    chi = cosmofid.comoving_distance(0, ZTAB[fg])
    xmin = ell_conv(1e1, chi)
    xmax = ell_conv(1e4, chi)
    ay2 = ax.twiny()
    ay2.set_xscale('log')
    ay2.xaxis.tick_top()
    ay2.set_xlabel(lsize)
    plt.axis([xmin, xmax, ymin, ymax])
    fig.savefig(os.path.join(plotpath, 'tests/snr_{}{}_vs_lin.png'.format(extra, fg)), dpi=144)
    plt.close()
    # fig.show()
    return


def plot_spectra(bins, outputfile='pmag'):
    ps_dict = load_pickle(psmagfile)
    ell = make_ell()
    fig = plt.figure()
    ax = fig.add_subplot(111)
    for i, vals in enumerate(bins):
        a, b = map(str, vals)
        ax.loglog(ell, ell**2 * ps_dict[a][b], '.',
                  color=colors[i], label='fg {} - bg {}'.format(*vals))
    # ax.set_title('')
    ax.set_xlabel(lell)
    ax.set_ylabel(lpmag)
    ax.set_ylim(1e-10, 1e-4)
    # ax.set_axes([5, 5e3, 1e-10, 1e-4])
    plt.legend(loc='best', frameon=False)
    plt.savefig(os.path.join(plotpath, 'tests/'+outputfile+'.png'), dpi=144)
    plt.close()
    # fig.show()
    return


def plot_spectra_fg(fg):
    bins = [(fg, x) for x in range(fg+1, len(ZTAB))]
    ps_dict = load_pickle(psmagfile)
    ps_dict_lin = load_pickle(psmagfile, 'linear')
    ell = make_ell()
    fig = plt.figure()
    ax = fig.add_subplot(111)
    for i, vals in enumerate(bins):
        a, b = map(str, vals)
        ax.loglog(ell, ell**2 * ps_dict[a][b], '-',
                  color=colors[i], lw=2.0, label='fg {} - bg {}'.format(*vals))
        ax.loglog(ell, ell**2 * ps_dict_lin[a][b], '--',
                  color=colors[i], lw=1.5)
    ax.set_xlabel(lell)
    ax.set_ylabel(lpmag)
    ax.set_ylim(1e-10, 1e-4)
    # ax.set_axes([5, 5e3, 1e-10, 1e-4])
    ax.legend(loc=4, frameon=False)
    xmin = ell_conv(xlmin, fg)
    xmax = ell_conv(xlmax, fg)
    ay2 = ax.twiny()
    ay2.set_xscale('log')
    ay2.xaxis.tick_top()
    ay2.set_xlabel(lamb)
    plt.axis([xmin, xmax, 1e-10, 1e-4])
    plt.savefig(os.path.join(plotpath, 'tests/pmag{}_vs_lin.png'.format(fg)), dpi=144)
    plt.close()
    # fig.show()
    return


def plot4in1(i, j, extra=''):
    si, sj = map(str, (i, j))
    ell = make_ell()
    ximin, ximax = ell_conv([xlmin, xlmax], i)
    xjmin, xjmax = ell_conv([xlmin, xlmax], j)
    ymin, ymax = 1e-8, 1e0
    ticks = [1e-8, 1e-6, 1e-4, 1e-2, 1e0]
    # LOAD FILES
    psmag = load_pickle(psmagfile, extra=extra)
    psauto = load_pickle(psautofile, extra=extra)
    pslens = load_pickle(pslensfile, extra=extra)
    shot = load_pickle(snfile)
    snr = load_pickle(snrfile, extra=extra)
    # PREPARE FOR PLOTTING
    plotlist = [psauto, psmag, pslens, shot]
    labels = [r'$gg$', r'$gm$',
              r'$mm$', r'${\rm sn}$']
    # labels = [r'$\delta\delta$', r'$\delta\mu$',
    #           r'$\mu\mu$', r'${\rm sn}$']
    bbr = dict(facecolor='red', alpha=0.2)
    bbb = dict(facecolor='blue', alpha=0.1)
    fig = plt.figure(figsize=(12, 8))
    # fig.subplots_adjust(hspace=0.2)
    # bin i
    ax1 = fig.add_subplot(2, 2, 1)
    for m, ps in enumerate(plotlist):
        ax1.loglog(ell, ell**2 * ps[si][si] / (2 * np.pi),
                   color=colors[m], lw=linewei[m],
                   ls=linesty[m], label=labels[m])
    # ax1.set_xlabel(lell)
    ax1.set_ylabel(lp)
    ax1.set_xlim(xlmin, xlmax)
    ax1.set_ylim(ymin, ymax)
    ax1.set_yticks(ticks)
    # ax1.legend(loc=4, frameon=False)
    ax1.text(0.05, 0.90, 'fg - fg', fontsize=13, transform=ax1.transAxes, bbox=bbb)
    ay1 = ax1.twiny()
    ay1.set_xscale('log')
    ay1.xaxis.tick_top()
    ay1.set_xlabel(lamb, fontsize='large')
    ay1.axis([ximin, ximax, ymin, ymax])
    ax2 = fig.add_subplot(2, 2, 2)
    for m, ps in enumerate(plotlist):
        ax2.loglog(ell, ell**2 * ps[sj][sj] / (2 * np.pi),
                   color=colors[m], lw=linewei[m],
                   ls=linesty[m], label=labels[m])
    # ax2.set_xlabel(lell)
    # ax2.set_ylabel(lp)
    ax2.set_xlim(xlmin, xlmax)
    ax2.set_ylim(ymin, ymax)
    ax2.set_yticks(ticks)
    ax2.legend(loc=4, frameon=False)
    ax2.text(0.05, 0.90, 'bg - bg', fontsize=13, transform=ax2.transAxes, bbox=bbb)
    ay2 = ax2.twiny()
    ay2.set_xscale('log')
    ay2.xaxis.tick_top()
    ay2.set_xlabel(lamb, fontsize='large')
    ay2.axis([xjmin, xjmax, ymin, ymax])
    ax3 = fig.add_subplot(2, 2, 3)
    for m, ps in enumerate(plotlist[:-1]):
        ax3.loglog(ell, ell**2 * ps[si][sj] / (2 * np.pi),
                   color=colors[m], lw=linewei[m],
                   ls=linesty[m], label=labels[m])
    ax3.set_xlabel(lell)
    ax3.set_ylabel(lp)
    ax3.set_xlim(xlmin, xlmax)
    ax3.set_ylim(ymin, ymax)
    ax3.set_yticks(ticks)
    # ax3.legend(loc=4, frameon=False)
    ax3.text(0.05, 0.90, 'fg - bg', fontsize=13, transform=ax3.transAxes, bbox=bbr)
    ay3 = ax3.twiny()
    ay3.set_xscale('log')
    ay3.xaxis.tick_top()
    # ay3.set_xlabel(lamb)
    ay3.axis([ximin, ximax, ymin, ymax])
    ax4 = fig.add_subplot(2, 2, 4)
    ax4.semilogx(ell, snr[si][sj], '-', lw=2.0,
                 color=pink,
                 label=r'$\frac{S}{N}$')
    ax4.set_xlabel(lell)
    ax4.set_ylabel(lsnr)
    ax4.set_xlim(xlmin, xlmax)
    ax4.set_ylim(0, 50)
    ax4.legend(loc=0, frameon=False)
    ax4.text(0.05, 0.88, redshif % (ZTAB[i], ZTAB[j]),
             # verticalaligment='center',
             fontsize=14, transform=ax4.transAxes) #, bbox=bbp)
    ay4 = ax4.twiny()
    ay4.set_xscale('log')
    ay4.xaxis.tick_top()
    # ay4.set_xlabel(lamb)
    ay4.axis([ximin, ximax, 0, 50])
    plt.savefig(os.path.join(plotpath,
                'all_spectra_snr_{}-{}{}.png'.format(i, j, extra)), dpi=144)
    plt.close()
    # return


def plot_covcontrib(i, j, extra=''):
    si, sj = map(str, (i, j))
    psmag = load_pickle(psmagfile, extra=extra)
    psauto = load_pickle(psautofile, extra=extra)
    pslens = load_pickle(pslensfile, extra=extra)
    shot = load_pickle(snfile)
    ell = make_ell()
    ximin, ximax = ell_conv([xlmin, xlmax], i)
    xjmin, xjmax = ell_conv([xlmin, xlmax], j)
    ymin, ymax = 1e-8, 1e1
    # ymin, ymax = 1e-12, 1e0
    ticks = [1e-8, 1e-6, 1e-4, 1e-2, 1e0]
    plotlist = [psauto, psmag, pslens, shot]
    labels = [r'$gg$', r'$gm$',
              r'$mm$', r'${\rm sn}$']
    # labels = [r'$\delta\delta$', r'$\delta\mu$',
    #           r'$\mu\mu$', r'${\rm sn}$']
    bbr = dict(facecolor='red', alpha=0.2)
    bbb = dict(facecolor='blue', alpha=0.1)
    fig = plt.figure(figsize=(6, 5))
    # fig.subplots_adjust(hspace=0.2)
    # bin i
    ax = fig.add_subplot(111)
    for m, ps in enumerate(plotlist):
        ax.loglog(ell, ell**2 * ps[si][sj] / (2.*np.pi),
                   color=colors[m], lw=linewei[m],
                   ls=linesty[m], label=labels[m])
    ax.set_xlabel(lell)
    ax.set_ylabel(lp)
    ax.set_xlim(xlmin, xlmax)
    ax.set_ylim(ymin, ymax)
    ax.set_yticks(ticks)
    # ax.legend(loc=4, frameon=False)
    # ax.text(0.05, 0.90, 'foreground - background', fontsize=14, transform=ax.transAxes, bbox=bbr)
    # ax.text(0.05, 0.90, 'foreground - foreground', fontsize=14, transform=ax.transAxes, bbox=bbb)
    ay = ax.twiny()
    ay.set_xscale('log')
    ay.xaxis.tick_top()
    ay.set_xlabel(lamb, fontsize='large')
    ay.axis([ximin, ximax, ymin, ymax])
    plt.savefig(os.path.join(plotpath, 'correct/covspectra_{}-{}_{}.png'.format(i, j, extra)), dpi=144)
    plt.close()


def plot_shotnoise(i, j, extra=''):
    si, sj = map(str, (i, j))
    ell = make_ell()
    ximin, ximax = ell_conv([xlmin, xlmax], i)
    snr = load_pickle(snrfile, extra=extra)
    fig = plt.figure(figsize=(6, 5))
    ax = fig.add_subplot(111)
    ax.semilogx(ell, snr[si][sj], '-', lw=2.0,
                 color=pink,
                 label=r'$\frac{S}{N}$')
    ax.set_xlabel(lell)
    ax.set_ylabel(lsnr)
    ax.set_xlim(xlmin, xlmax)
    ax.set_ylim(0, 6)
    # ax.legend(loc=0, frameon=False)
    ax.text(0.05, 0.47, redshif % (ZTAB[i], ZTAB[j]),
             # verticalaligment='center',
             fontsize=14, transform=ax.transAxes) #, bbox=bbp)
    ay = ax.twiny()
    ay.set_xscale('log')
    ay.xaxis.tick_top()
    ay.set_xlabel(lamb, fontsize='large')
    ay.axis([ximin, ximax, 0, 50])
    plt.savefig(os.path.join(plotpath, 'correct/snr_{}-{}_{}.png'.format(i, j, extra)), dpi=144)
    plt.close()

def plot_duncan():
    ell = make_ell()
    psa = load_pickle(psautofile, 'new')
    psm = load_pickle(psmagfile, 'new')
    psl = load_pickle(pslensfile, 'new')
    fig, (ax1, ax2, ax3) = plt.subplots(1, 3, sharey=True, figsize=(9, 3))
    #ax = fig.add_subplot(111)
    ax1.loglog(ell, psa['0']['0'], label="gg")
    ax1.loglog(ell, psl['0']['0'], label="mm")
    ax1.loglog(ell, 2 * psm['0']['0'], label="gm + mg")
    ax1.set_xlim(5.0e1, 3.0e3)
    ax1.set_ylim(1.0e-19, 1.0e-3)
    ax1.set_ylabel(r"$P(\ell)$")
    ax1.legend(loc='best', frameon=False)
    #ax1.title("zf = 0.3 - zb = 0.3")
    ax2.loglog(ell, psa['0']['1'])
    ax2.loglog(ell, psl['0']['1'])
    ax2.loglog(ell, 2 * psm['0']['1'])
    ax2.set_xlim(5.0e1, 3.0e3)
    ax2.set_ylim(1.0e-19, 1.0e-3)
    ax2.set_xlabel(r'$\ell$')
    #ax2.title("zf = 0.3 - zb = 0.7")
    ax3.loglog(ell, psa['0']['2'])
    ax3.loglog(ell, psl['0']['2'])
    ax3.loglog(ell, 2 * psm['0']['2'])
    ax3.set_xlim(5.0e1, 3.0e3)
    ax3.set_ylim(1.0e-19, 1.0e-3)
    #ax3.title("zf = 0.3 - zb = 1.1")
    # plt.show()


def main():
    print "\n#################"
    print "# Starting test #"
    print '#################\n'

    import time
    start = time.time()

    # mag = MagnificationData(ZTAB, ELLMIN, ELLMAX, NELL)
    mag = MagnificationData(ZTAB, ELLMIN, ELLMAX, NELL, linear=True)

    spectra = compute_spectra(mag)
    for ps, fil in zip(spectra, str_spectra):
        save_pickle(ps, fil, 'correct_linear')

    # spectrum = compute_spectr(mag, 'all')
    snr = compute_signal2noise(mag)
    save_pickle(snr, snrfile, 'correct_linear')
    # save_pickle(spectrum, 'crosspowerspectrum')

    # save_shotnoise(mag)

    end = time.time()
    print 'The code took {:.2f}s'.format(end-start)

    print "Data saved !"


if __name__ == '__main__':
    # main()
    # for i in xrange(4):
    #     for j in xrange(i+1, 5):
    #         plot_shotnoise(i, j, 'correct_linear')
    # for i in xrange(5):
    #     for j in xrange(i, 5):
    #         plot_covcontrib(i, j, 'correct_linear')
    plot4in1(0,2,'correct')
