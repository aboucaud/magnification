# -*- coding: utf-8 -*-
"""
Created on Tue Feb  5 18:12:19 2013

@author: alexandreboucaud
"""
import numpy as np
import os

OUTPUTDIR = '/Users/alexandreboucaud/work/LSST/magnif/outputs'
fisherfile = os.path.join(OUTPUTDIR, 'fisher_matrix.txt')
covfile = os.path.join(OUTPUTDIR, 'covariance_matrix.txt')
f = np.loadtxt(fisherfile)
c = np.loadtxt(covfile)
print 'Fisher:', f
print 'Covariance:', c